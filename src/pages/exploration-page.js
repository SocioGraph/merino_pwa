import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import CancelIcon from "@material-ui/icons/Cancel";
import GetAppIcon from "@material-ui/icons/GetApp";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import SettingsBackupRestoreIcon from "@material-ui/icons/SettingsBackupRestore";
import ShareIcon from "@material-ui/icons/Share";
import React, { useEffect, useState } from "react";
import SlideInUp from "react-animations/lib/slide-in-up";
import Carousel from "react-bootstrap/Carousel";
import { withRouter } from "react-router-dom";
import { toast } from "react-toastify";
import { RWebShare } from "react-web-share";
import { TransformComponent, TransformWrapper } from "react-zoom-pan-pinch";
import styled, { keyframes } from "styled-components";
import ViewDetailsIcon from "../../public/assets/Asset 1.svg";
import ZoomInIcon from "../../public/assets/Asset 2.svg";
import LikeIcon from "../../public/assets/Asset 4.svg";
import Loader from "../../public/assets/loader1.gif";
import LikedIcon from "../../public/assets/test.jpeg";
import WishlistedIcon from "../../public/assets/test2.jpeg";
import WishlistIcon from "../../public/assets/test3.jpeg";
import { apiActions } from "../../server";
import { useThemeSelection } from "../hooks/theme-hook";
import "../styles/design-page.css";
import "../styles/search-page.css";
import "../styles/space.css";
import { filterCategories, spaceFilterCategories } from "../util/filters";

const _ = require("lodash");
const SlideInUpAnimation = keyframes`${SlideInUp}`;

const SlideInUpDiv = styled.div`
  animation: 0.2s ${SlideInUpAnimation};
`;

const ExplorationPage = (props) => {
  const { currentThemeSelection } = useThemeSelection();
  const [selectedFilters, setSelectedFilters] = useState([]);
  const [selectedFiltersObject, setSelectedFiltersObject] = useState({
    structure: [],
    species: [],
    finish_type: [],
    design_category: [],
    product_names: [],
    surfaces: [],
    colour: [],
  });
  const [selectedFilterCategory, setSelectedFilterCategory] = useState("");
  const [showViewDetailsModal, setShowViewDetailsModal] = useState(false);
  const [showZoomInModal, setShowZoomInModal] = useState(false);
  const [fullImage, setFullImage] = useState(false);
  const [showSignUpModal, setShowSignUpModal] = useState(false);
  const [selectedDesignID, setSelectedDesginID] = useState(1);
  const [showWishlistModal, setShowWishlistModal] = useState(false);
  const [showAuthModal, setShowAuthModal] = useState(false);
  const [loadingRenderImages, setLoadingRenderImages] = useState(false);
  const [loadedRenderImages, setLoadedRenderImages] = useState(false);
  const [loadingDesignData, setLoadingDesignData] = useState(false);
  const [loadedDesignData, setLoadedDesignData] = useState(false);

  const [similarDesignData, setSimilarDesignData] = useState([]);
  const [loadingSimilarDesignData, setLoadingSimilarDesignData] = useState(
    false
  );
  const [totalSimilarDesignData, setTotalSimilarDesignData] = useState(0);

  const [similarOfDesignPageNo, setSimilarDesignDataPageNo] = useState(1);
  const [
    loadingMoreSimilarDesignData,
    setLoadingMoreSimilarDesignData,
  ] = useState(false);

  // ref hooks to use state in event handlers
  const similarDesignDataRef = React.useRef(similarDesignData);
  const setSimilarDesignDataRef = (data) => {
    similarDesignDataRef.current = data;
    setSimilarDesignData(data);
  };

  const similarOfDesignPageNoRef = React.useRef(similarOfDesignPageNo);
  const setSimilarDesignDataPageNoRef = (data) => {
    similarOfDesignPageNoRef.current = data;
    setSimilarDesignDataPageNo(data);
  };

  const loadingMoreSimilarDesignDataRef = React.useRef(
    loadingMoreSimilarDesignData
  );
  const setLoadingMoreSimilarDesignDataRef = (data) => {
    loadingMoreSimilarDesignDataRef.current = data;
    setLoadingMoreSimilarDesignData(data);
  };

  const [availableDesignData, setAvailableDesignData] = useState([]);
  const [loadingAvailableDesignData, setLoadingAvailableDesignData] = useState(
    false
  );
  const [totalAvailableDesignData, setTotalAvailableDesignData] = useState(0);

  const [availableOfDesignPageNo, setAvailableDesignDataPageNo] = useState(1);
  const [
    loadingMoreAvailableDesignData,
    setLoadingMoreAvailableDesignData,
  ] = useState(false);

  // ref hooks to use state in event handlers
  const availableDesignDataRef = React.useRef(availableDesignData);
  const setAvailableDesignDataRef = (data) => {
    availableDesignDataRef.current = data;
    setAvailableDesignData(data);
  };

  const availableOfDesignPageNoRef = React.useRef(availableOfDesignPageNo);
  const setAvailableDesignDataPageNoRef = (data) => {
    availableOfDesignPageNoRef.current = data;
    setAvailableDesignDataPageNo(data);
  };

  const loadingMoreAvailableDesignDataRef = React.useRef(
    loadingMoreAvailableDesignData
  );
  const setLoadingMoreAvailableDesignDataRef = (data) => {
    loadingMoreAvailableDesignDataRef.current = data;
    setLoadingMoreAvailableDesignData(data);
  };

  const [loadedSimilarDesignData, setLoadedSimilarDesignData] = useState(false);

  const [familyDesignData, setFamilyDesignData] = useState([]);
  const [loadingFamilyDesignData, setLoadingFamilyDesignData] = useState(false);
  const [loadedFamilyDesignData, setLoadedFamilyDesignData] = useState(false);
  const [totalFamilyDesignData, setTotalFamilyDesignData] = useState(0);

  const [familyOfDesignPageNo, setFamilyDesignDataPageNo] = useState(1);
  const [
    loadingMoreFamilyDesignData,
    setLoadingMoreFamilyDesignData,
  ] = useState(false);

  // ref hooks to use state in event handlers
  const familyDesignDataRef = React.useRef(familyDesignData);
  const setFamilyDesignDataRef = (data) => {
    familyDesignDataRef.current = data;
    setFamilyDesignData(data);
  };

  const familyOfDesignPageNoRef = React.useRef(familyOfDesignPageNo);
  const setFamilyDesignDataPageNoRef = (data) => {
    familyOfDesignPageNoRef.current = data;
    setFamilyDesignDataPageNo(data);
  };

  const loadingMoreFamilyDesignDataRef = React.useRef(
    loadingMoreFamilyDesignData
  );
  const setLoadingMoreFamilyDesignDataRef = (data) => {
    loadingMoreFamilyDesignDataRef.current = data;
    setLoadingMoreFamilyDesignData(data);
  };

  const [imagesArr, setImagesArr] = useState([]);
  const [imagesNamesArr, setImagesNamesArr] = useState([]);

  const [renderCount, setRenderCount] = useState(0);
  const [selectedAuth, setSelectedAuth] = useState("signup");
  const [loginFields, handleLoginFieldChange] = useState({
    email: "",
    password: "",
  });

  const [signupFields, handleSignupFieldChange] = useState({
    username: "",
    name: "",
    contact: "",
    email: "",
    password: "",
    repassword: "",
  });

  const [designID, setDesignID] = useState("");
  const [currentDesign, setCurrentDesign] = useState({});

  const [imgNumber, setImgNumber] = useState(1);

  const [liked, setLiked] = useState(false);
  const [wishlisted, setWishlisted] = useState(false);

  const [selectedCatIdx, setSelectedCatIdx] = useState(-1);

  const [productSubCategory, setProductSubCategory] = useState("");
  const [roomSubCategory, setRoomSubCategory] = useState("");
  const [changeRoom, setChangeRoom] = useState("");

  let cachedFilterCategories = filterCategories;
  const filterCategoriesKeys = _.keys(cachedFilterCategories);

  let cachedFilterCategoriesRoom = spaceFilterCategories;
  const filterCategoriesRoomKeys = _.keys(cachedFilterCategoriesRoom);

  const [signingUp, setSigningUp] = useState(false);
  const [loggingIn, setLogginIn] = useState(false);

  const [filterInspirationsArr, setFilterInspirationsArr] = useState(["Suede"]);

  const [rotatedImage, setRotatedImage] = useState(false);
  const [currentSelectedScene, setCurrentSelectedScene] = useState("");
  const [inspirationsObj, setInspirationsObj] = useState({});
  const [inpirationsSceneIDsArr, setInpirationsSceneIDsArr] = useState([]);
  const [selectedFinishForScenes, setSelectedFinishForScenes] = useState({});
  const [loadOnlyInspirations, setLoadOnlyInspirations] = useState(false);

  const [selectedProductName, setSelectedProductName] = useState("");
  const [showProductNamesList, setShowProductNamesList] = useState(false);

  const loadAvailableDesignsForPage = () => {
    if (document.getElementById("available"))
      document
        .getElementById("available")
        .removeEventListener("scroll", trackScrollingAvailable);

    const selectedSceneIDSpace = JSON.parse(
      localStorage.getItem("selected-scene-id-space")
    );
    let queryFilter = {};

    const selectedSurfaces = JSON.parse(
      localStorage.getItem("selected-surfaces")
    );
    if (selectedFiltersObject["surfaces"].length) {
      queryFilter = { ...selectedFiltersObject };
    } else {
      queryFilter = {
        ...selectedFiltersObject,
        surfaces: selectedSurfaces,
      };
    }

    queryFilter._page_number = availableOfDesignPageNoRef.current + 1;
    queryFilter.scene_ids = !selectedFilters.length
      ? selectedSceneIDSpace
      : undefined;

    if (queryFilter["finish_type"].length) {
      queryFilter["finishes"] = queryFilter["finish_type"];
      delete queryFilter["finish_type"];
    }

    setLoadingMoreAvailableDesignDataRef(true);

    setTimeout(() => {
      document.getElementById("available").scrollTo({
        right: 0,
        behavior: "smooth",
      });
    }, 100);

    apiActions.get_designs(
      queryFilter,
      (res) => {
        if (res.is_last) {
          if (document.getElementById("available"))
            document
              .getElementById("available")
              .removeEventListener("scroll", trackScrollingAvailable);
        } else {
          setAvailableDesignDataPageNoRef(queryFilter._page_number);
          if (document.getElementById("available"))
            document
              .getElementById("available")
              .addEventListener("scroll", trackScrollingAvailable);
        }

        let updatedAvailableDesignData = [
          ...availableDesignDataRef.current,
          ...res.data,
        ];
        setAvailableDesignDataRef(updatedAvailableDesignData);
        setLoadingMoreAvailableDesignDataRef(false);
      },
      () => {
        alert("failed to fetch desgin family data");
        setLoadingAvailableDesignData(false);
      }
    );
  };

  const trackScrollingAvailable = (e) => {
    if (
      e.currentTarget.scrollWidth -
        e.currentTarget.clientWidth -
        e.currentTarget.scrollLeft <=
      2
    ) {
      loadAvailableDesignsForPage();
    }
  };

  const loadFamilyOfDesignsForPage = () => {
    if (document.getElementById("family"))
      document
        .getElementById("family")
        .removeEventListener("scroll", trackScrollingFamily);

    let params = { ...JSON.parse(localStorage.getItem("design")) };
    params.pageNumber = familyOfDesignPageNoRef.current + 1;

    setLoadingMoreFamilyDesignDataRef(true);

    setTimeout(() => {
      if (document.getElementById("family"))
        document.getElementById("family").scrollTo({
          right: 0,
          behavior: "smooth",
        });
    }, 100);

    apiActions.get_design_family(
      JSON.stringify(params),
      (res) => {
        if (res.is_last) {
          if (document.getElementById("family"))
            document
              .getElementById("family")
              .removeEventListener("scroll", trackScrollingFamily);
        } else {
          setFamilyDesignDataPageNoRef(params.pageNumber);
          if (document.getElementById("family"))
            document
              .getElementById("family")
              .addEventListener("scroll", trackScrollingFamily);
        }

        let updatedFamilyDesignData = [
          ...familyDesignDataRef.current,
          ...res.data,
        ];
        setFamilyDesignDataRef(updatedFamilyDesignData);
        setLoadingMoreFamilyDesignDataRef(false);
      },
      () => {
        alert("failed to fetch desgin family data");
        setLoadingFamilyDesignData(false);
      }
    );
  };

  const trackScrollingFamily = (e) => {
    if (
      e.currentTarget.scrollWidth -
        e.currentTarget.clientWidth -
        e.currentTarget.scrollLeft <=
      2
    ) {
      loadFamilyOfDesignsForPage();
    }
  };

  const loadSimilarDesignsForPage = () => {
    if (document.getElementById("similar"))
      document
        .getElementById("similar")
        .removeEventListener("scroll", trackScrollingSimilar);

    let params = { ...JSON.parse(localStorage.getItem("design")) };
    params.pageNumber = similarOfDesignPageNoRef.current + 1;
    setLoadingMoreSimilarDesignDataRef(true);

    setTimeout(() => {
      document.getElementById("similar").scrollTo({
        right: 0,
        behavior: "smooth",
      });
    }, 100);

    apiActions.get_similar_designs(
      JSON.stringify(params),
      (res) => {
        if (res.is_last) {
          if (document.getElementById("similar"))
            document
              .getElementById("similar")
              .removeEventListener("scroll", trackScrollingSimilar);
        } else {
          setSimilarDesignDataPageNoRef(params.pageNumber);
          if (document.getElementById("similar"))
            document
              .getElementById("similar")
              .addEventListener("scroll", trackScrollingSimilar);
        }

        let updatedSimilarDesignData = [
          ...similarDesignDataRef.current,
          ...res.data,
        ];
        setSimilarDesignDataRef(updatedSimilarDesignData);
        setLoadingMoreSimilarDesignDataRef(false);
      },
      () => {
        alert("failed to fetch desgin family data");
        setLoadingSimilarDesignData(false);
      }
    );
  };

  const trackScrollingSimilar = (e) => {
    if (
      e.currentTarget.scrollWidth -
        e.currentTarget.clientWidth -
        e.currentTarget.scrollLeft <=
      2
    ) {
      loadSimilarDesignsForPage();
    }
  };

  const downloadFile = (file) => {
    window.location.href = file;
  };

  const setFilter = (option) => {
    const filterObj = {};
    let selectedFilterCat = selectedFilterCategory;

    // allows to set filter for suede and gloss option
    if (selectedFilterCategory == "") {
      selectedFilterCat = "finish_type";
    }
    filterObj[selectedFilterCat] = option;

    if (!selectedFilters.some((filter) => _.isEqual(filter, filterObj))) {
      let copySelectedFilters = Object.assign([], selectedFilters);
      copySelectedFilters.push(filterObj);
      setSelectedFilters(copySelectedFilters);
      // update filters object as well
      let copySelectedFiltersObject = Object.assign({}, selectedFiltersObject);
      copySelectedFiltersObject[selectedFilterCat].push(option);
      setSelectedFiltersObject(copySelectedFiltersObject);
    } else {
      alert("Filter is already selected.");
    }
    setSelectedFilterCategory("");
  };

  const getList = () => {
    let filterCategoryOptions = cachedFilterCategories[selectedFilterCategory];
    let name = selectedFilterCategory;
    if (selectedCatIdx != -1) {
      if (selectedFilterCategory == "product_names") {
        filterCategoryOptions = cachedFilterCategories["product_names"];
        let filteredOptions = filterCategoryOptions[selectedCatIdx];
        filterCategoryOptions = Object.values(filteredOptions);
        filterCategoryOptions = filterCategoryOptions[0];
        name = "Product > " + productSubCategory;
      } else {
        let filteredOptions1 = Object.keys(cachedFilterCategoriesRoom)[
          selectedCatIdx
        ];
        filterCategoryOptions = cachedFilterCategoriesRoom[filteredOptions1];
        name = "ROOMS > " + roomSubCategory;
      }
    }
    return (
      <div className="popup-overlay">
        <div className="category-dropdown-wrapper">
          <SlideInUpDiv>
            <CancelIcon
              className="close-dropdown"
              onClick={() => {
                setSelectedFilterCategory("");
                setProductSubCategory("");
              }}
            />
            <div className="category-dropdown">
              <div
                className="category-name"
                onClick={() => {
                  setSelectedCatIdx(-1);
                  // setProductSubCategory("");
                }}
              >
                {cleanCategory(name)}
              </div>
              <div className="category-options">
                {filterCategoryOptions &&
                  filterCategoryOptions.map((option) => (
                    <div
                      className="category-option"
                      onClick={() => {
                        if (selectedCatIdx == -1) {
                          setFilter(option);
                        } else {
                          if (selectedFilterCategory == "product_names") {
                            const filterObj = {};
                            filterObj[selectedFilterCategory] = option;
                            if (
                              !selectedFilters.some((filter) =>
                                _.isEqual(filter, filterObj)
                              )
                            ) {
                              let copySelectedFilters = Object.assign(
                                [],
                                selectedFilters
                              );
                              copySelectedFilters.push(filterObj);
                              setSelectedFilters(copySelectedFilters);

                              // update filters object as well
                              let copySelectedFiltersObject = Object.assign(
                                {},
                                selectedFiltersObject
                              );
                              copySelectedFiltersObject[
                                selectedFilterCategory
                              ].push(option);

                              setSelectedFiltersObject(
                                copySelectedFiltersObject
                              );
                            } else {
                              alert("Filter is already selected.");
                            }

                            setSelectedFilterCategory("");
                            setSelectedFilterCategory("");
                          } else {
                            setChangeRoom("true");
                            const filterObj = {};
                            let formattedOption = option.toLowerCase();
                            formattedOption = formattedOption.replaceAll(
                              " ",
                              "_"
                            );
                            filterObj["surfaces"] = option;

                            if (
                              !selectedFilters.some((filter) =>
                                _.isEqual(filter, filterObj)
                              )
                            ) {
                              const updatedSelectedFilter = Object.assign(
                                [],
                                selectedFilters
                              );
                              updatedSelectedFilter.push({ surfaces: option });
                              setSelectedFilters(updatedSelectedFilter);
                              // update filters object as well
                              let copySelectedFiltersObject = Object.assign(
                                {},
                                selectedFiltersObject
                              );
                              copySelectedFiltersObject["surfaces"] = [
                                ...copySelectedFiltersObject["surfaces"],
                                formattedOption,
                              ];
                              setSelectedFiltersObject(
                                copySelectedFiltersObject
                              );
                            } else {
                              alert("Filter is already selected.");
                            }

                            setSelectedFilterCategory("");
                            setSelectedFilterCategory("");
                          }
                        }
                      }}
                    >
                      {option}
                    </div>
                  ))}
              </div>
              <div style={{ height: "15vh" }} />
            </div>
          </SlideInUpDiv>
        </div>
      </div>
    );
  };

  const getPrimaryListProduct = () => {
    const filterCategoryOptions = cachedFilterCategories["product_names"];

    const filteredOptions = filterCategoryOptions.map((obj) =>
      Object.keys(obj)
    );

    return (
      <div className="popup-overlay">
        <div className="category-dropdown-wrapper">
          <SlideInUpDiv>
            <CancelIcon
              className="close-dropdown"
              onClick={() => {
                setSelectedFilterCategory("");
                setSelectedCatIdx(-1);
              }}
            />
            <div className="category-dropdown">
              <div className="category-name">
                {cleanCategory(selectedFilterCategory)}
              </div>
              <div className="category-options">
                {filteredOptions &&
                  filteredOptions.map((option, idx) => (
                    <div
                      className="category-option"
                      onClick={() => {
                        setSelectedCatIdx(idx);
                        // alert(option);

                        setSelectedFilterCategory("product_names");
                        setProductSubCategory(option[0]);
                      }}
                    >
                      {option} <span className="arrow-sign">{" >"}</span>
                    </div>
                  ))}
              </div>
              <div style={{ height: "15vh" }} />
            </div>
          </SlideInUpDiv>
        </div>
      </div>
    );
  };

  const getPrimaryListSpace = () => {
    const filteredOptions = Object.keys(cachedFilterCategoriesRoom);
    return (
      <div className="popup-overlay">
        <div className="category-dropdown-wrapper">
          <SlideInUpDiv>
            <CancelIcon
              className="close-dropdown"
              onClick={() => {
                setSelectedFilterCategory("");
                setSelectedCatIdx(-1);
              }}
            />
            <div className="category-dropdown">
              <div className="category-name">
                {cleanCategory(selectedFilterCategory)}
              </div>
              <div className="category-options">
                {filteredOptions &&
                  filteredOptions.map((option, idx) => (
                    <div
                      className="category-option"
                      onClick={() => {
                        setSelectedCatIdx(idx);
                        setSelectedFilterCategory("rooms");
                        setRoomSubCategory(option);
                      }}
                    >
                      {option} <span className="arrow-sign">{" >"}</span>
                    </div>
                  ))}
              </div>
              <div style={{ height: "15vh" }} />
            </div>
          </SlideInUpDiv>
        </div>
      </div>
    );
  };

  const getWishlistModal = () => {
    return (
      <div className="popup-overlay">
        <div className="sign-up-modal-wrapper">
          <CancelIcon
            className="close-modal-sign-up"
            onClick={() => setShowWishlistModal(false)}
          />
          <center>
            <div className="sign-up-modal">
              <h6 style={{ font: "normal normal bold 18px/28px Encode Sans" }}>
                <b>WISHLIST</b>
              </h6>
              <br />
              <center>
                <h5
                  style={{
                    backgroundColor: "#656565",
                    borderRadius: "40px",
                    color: "white",
                    font: "normal normal bold 15px/18px Encode Sans",
                    marginTop: "1vh",
                    paddingTop: "0.5vh",
                    paddingBottom: "0.5vh",
                    width: "20vw",
                    mouse: "cursor",
                  }}
                />
              </center>
            </div>
          </center>
        </div>
      </div>
    );
  };

  const validSignUp = () => {
    return (
      signupFields.username != "" &&
      signupFields.email != "" &&
      signupFields.name != "" &&
      signupFields.password != "" &&
      signupFields.password == signupFields.repassword
    );
  };

  const validLogIn = () => {
    return loginFields.password != "" && loginFields.email != "";
  };
  const signupHandler = () => {
    setSigningUp(true);
    const data = {
      email: signupFields.email,
      password: signupFields.password,
      name: signupFields.name,
      phone_number: signupFields.contact,
      username: signupFields.username,
    };

    apiActions.patch_user(
      data,
      (res) => {
        setSigningUp(false);
        setSelectedAuth("login");
      },
      (e) => {
        setSigningUp(false);
        alert("Failed to register, please try again");
      }
    );
    // setShowAuthModal(false);
  };

  const loginHandler = () => {
    setLogginIn(true);
    apiActions.login(
      loginFields.email,
      loginFields.password,
      (res) => {
        setLogginIn(false);
        setShowAuthModal(false);
      },
      (e) => {
        setLogginIn(false);
        alert("Username or password not correct!");
      }
    );
    // setShowAuthModal(false);
  };

  const getAuthModal = () => {
    return (
      <div className="popup-overlay">
        <div className="auth-modal-wrapper">
          <CancelIcon
            className="close-modal-auth"
            onClick={() => {
              setShowAuthModal(false);
              // setWishlisted(true);
            }}
          />
          <center>
            <div className="auth-modal">
              <div className="auth-modal-header">
                <div className="row">
                  <div
                    className={
                      selectedAuth == "login"
                        ? "col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 selected-auth"
                        : "col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 "
                    }
                    style={{
                      paddingBottom: "1vh",
                      paddingTop: "1vh",
                      cursor: "pointer",
                    }}
                    onClick={() =>
                      selectedAuth == "signup" ? setSelectedAuth("login") : null
                    }
                  >
                    Login
                  </div>

                  <div
                    className={
                      selectedAuth == "signup"
                        ? "col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 selected-auth"
                        : "col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 "
                    }
                    style={{
                      paddingBottom: "1vh",
                      paddingTop: "1vh",
                      cursor: "pointer",
                    }}
                    onClick={() =>
                      selectedAuth == "login" ? setSelectedAuth("signup") : null
                    }
                  >
                    Signup
                  </div>
                </div>
              </div>

              <div className="auth-modal-body">
                {selectedAuth == "login" ? (
                  <>
                    <div class="container">
                      <input
                        type="text"
                        placeholder="Username"
                        value={loginFields.email}
                        onChange={(e) => {
                          const email = e.target.value;
                          handleLoginFieldChange({ ...loginFields, email });
                        }}
                        // name="uname"
                        required
                      />

                      <input
                        type="password"
                        value={loginFields.password}
                        onChange={(e) => {
                          const password = e.target.value;
                          handleLoginFieldChange({ ...loginFields, password });
                        }}
                        placeholder="Password"
                        name="psw"
                        required
                      />

                      <div
                        onClick={
                          () =>
                            !loggingIn
                              ? validLogIn()
                                ? loginHandler()
                                : alert("Please fill the form correctly")
                              : null
                          // setShowAuthModal(false);
                          // setWishlisted(true);
                        }
                        className={
                          loggingIn
                            ? "progress-btn active "
                            : validLogIn()
                            ? "progress-btn"
                            : "disabled progress-btn"
                        }
                        data-progress-style="indefinite"
                      >
                        <div className="btn"> Login</div>
                        <div class="progress" />
                      </div>

                      <label>
                        <input
                          type="checkbox"
                          checked="checked"
                          name="remember"
                        />{" "}
                        Remember me
                      </label>
                    </div>

                    {/* <div class="container">
                  <span class="psw">
                    Forgot <a href="#">password?</a>
                  </span>
                </div> */}
                  </>
                ) : (
                  <>
                    {" "}
                    <input
                      type="text"
                      placeholder="Username*"
                      name="uname"
                      required
                      value={signupFields.username}
                      onChange={(e) => {
                        const username = e.target.value;
                        handleSignupFieldChange({ ...signupFields, username });
                      }}
                    />
                    <input
                      type="text"
                      placeholder="Name*"
                      name="uname"
                      required
                      value={signupFields.name}
                      onChange={(e) => {
                        const name = e.target.value;
                        handleSignupFieldChange({ ...signupFields, name });
                      }}
                    />
                    <input
                      type="email"
                      placeholder="Email*"
                      name="uname"
                      required
                      value={signupFields.email}
                      onChange={(e) => {
                        const email = e.target.value;
                        handleSignupFieldChange({ ...signupFields, email });
                      }}
                    />
                    <input
                      type="text"
                      placeholder="Mobile No*"
                      name="uname"
                      required
                      value={signupFields.contact}
                      onChange={(e) => {
                        const contact = e.target.value;
                        handleSignupFieldChange({ ...signupFields, contact });
                      }}
                    />
                    <input
                      type="password"
                      placeholder="Password*"
                      name="psw"
                      required
                      value={signupFields.password}
                      onChange={(e) => {
                        const password = e.target.value;
                        handleSignupFieldChange({ ...signupFields, password });
                      }}
                    />
                    <input
                      type="password"
                      placeholder="Confirm Password*"
                      name="psw"
                      required
                      value={signupFields.repassword}
                      onChange={(e) => {
                        const repassword = e.target.value;
                        handleSignupFieldChange({
                          ...signupFields,
                          repassword,
                        });
                      }}
                    />
                    <label>
                      <input
                        type="checkbox"
                        checked="checked"
                        name="remember"
                      />{" "}
                      I agree with terms of use.
                    </label>
                    <div
                      onClick={
                        () =>
                          !signingUp
                            ? validSignUp()
                              ? signupHandler()
                              : alert("Please fill the form correctly")
                            : null
                        // setShowAuthModal(false);
                        // setWishlisted(true);
                      }
                      className={
                        signingUp
                          ? "progress-btn active "
                          : validSignUp()
                          ? "progress-btn"
                          : "disabled progress-btn"
                      }
                      data-progress-style="indefinite"
                    >
                      <div className="btn"> Create Account</div>
                      <div class="progress" />
                    </div>
                    {/* <button
                      type="submit"
                     
                    >
                     
                    </button> */}
                  </>
                )}
              </div>
            </div>
          </center>
        </div>
      </div>
    );
  };

  const getSignInPopUp = () => {
    return (
      <div className="popup-overlay">
        <div className="sign-up-modal-wrapper">
          <CancelIcon
            className="close-modal-sign-up"
            onClick={() => setShowSignUpModal(false)}
          />
          <center>
            <div className="sign-up-modal">
              <h6 style={{ font: "normal normal bold 18px/28px Encode Sans" }}>
                <b>
                  You’ve liked this design! Sign up now to add it to your wish
                  list so that you can view your favorites later.
                </b>
              </h6>
              <br />
              <center>
                <h5
                  style={{
                    backgroundColor: "#656565",
                    borderRadius: "40px",
                    color: "white",
                    font: "normal normal bold 15px/18px Encode Sans",
                    marginTop: "1vh",
                    paddingTop: "0.5vh",
                    paddingBottom: "0.5vh",
                    width: "20vw",
                  }}
                  onClick={() => {
                    //setShowWishlistModal(true);
                    setShowAuthModal(true);
                    setShowSignUpModal(false);
                    //$.signup();
                  }}
                >
                  SIGN UP
                </h5>
              </center>
            </div>
          </center>
        </div>
      </div>
    );
  };

  const getShowZoomInModal = () => {
    return (
      <div className="popup-overlay">
        <div className="zoom-in-modal-wrapper">
          <CancelIcon
            className="close-dropdown-zoom-in"
            onClick={() => setShowZoomInModal(false)}
            style={{ zIndex: 2 }}
          />
          <div className="zoom-in-modal">
            <TransformWrapper>
              {({ zoomIn, zoomOut, resetTransform, ...rest }) => (
                <React.Fragment>
                  <TransformComponent>
                    <img
                      className={rotatedImage ? "pan-img-rotated" : "pan-img"}
                      src={
                        inspirationsObj[inpirationsSceneIDsArr[imgNumber - 1]][
                          selectedFinishForScenes[
                            inpirationsSceneIDsArr[imgNumber - 1]
                          ]
                        ].url
                      }
                      alt="First slide"
                      style={{
                        height: "75vh",
                        width: "unset",
                        maxWidth: "unset",
                      }}
                    />
                  </TransformComponent>
                </React.Fragment>
              )}
            </TransformWrapper>
          </div>

          <SettingsBackupRestoreIcon
            className="refresh-icon"
            onClick={() => setRotatedImage(!rotatedImage)}
          />

          <a
            href={
              inspirationsObj[inpirationsSceneIDsArr[imgNumber - 1]][
                selectedFinishForScenes[inpirationsSceneIDsArr[imgNumber - 1]]
              ].url
            }
            target="_blank"
          >
            <GetAppIcon className="download-icon" />
          </a>

          <RWebShare
            data={{
              // text: "Like humans, flamingos make friends for life",
              url:
                inspirationsObj[inpirationsSceneIDsArr[imgNumber - 1]][
                  selectedFinishForScenes[inpirationsSceneIDsArr[imgNumber - 1]]
                ].url,
              // title: "Merino",
            }}
            sites={["whatsapp", "facebook", "twitter"]}
          >
            <ShareIcon
              className="share-icon"
              // onClick={() => setShowZoomInModal(false)}
            />
          </RWebShare>
        </div>
      </div>
    );
  };

  const getViewDetailsModal = () => {
    const design = JSON.parse(localStorage.getItem("design"));
    const productsInPWA = {};
    const productNameMap = design.product_name_map;
    const keysProductNameMap = Object.keys(productNameMap);

    keysProductNameMap.map((key) => {
      if (design.show_in_pwa_map[key]) {
        productsInPWA[key] = productNameMap[key];
      }
    });

    // console.log("test", productsInPWA);

    const valuesNames = Object.values(productsInPWA);
    if (valuesNames.length && selectedProductName == "") {
      setSelectedProductName(valuesNames[0]);
    }
    let finish_str = "";
    let name = "";
    let valuesKeys = [];
    if (valuesNames.length) {
      valuesKeys = Object.keys(productsInPWA);
      const key = Object.keys(productsInPWA).filter(function(key) {
        return productsInPWA[key] === selectedProductName;
      })[0];
      if (key) {
        design.product_design_finish_map[key].map(
          (finish) => (finish_str += finish + ", ")
        );
        finish_str = finish_str.slice(0, -2);
      }
      name = "Design " + design.design_id + " " + design.design_name;
    }

    return (
      <div className="popup-overlay">
        <div className="view-details-dropdown-wrapper">
          <SlideInUpDiv>
            <CancelIcon
              className="close-dropdown"
              onClick={() => setShowViewDetailsModal(false)}
            />
            <div className="view-details-dropdown">
              <div className="design-image-wrapper">
                <div
                  className="design-image"
                  style={{ width: fullImage ? "100%" : "50%" }}
                >
                  <center>
                    <h5
                      style={{
                        "font-size": "1rem",
                        "margin-bottom": "15px",
                      }}
                    >
                      <b>{name}</b>
                    </h5>
                  </center>

                  {fullImage ? (
                    <div
                      style={{
                        background: `url(${design.design_image})`,
                        height: "680px",
                        "background-repeat": "no-repeat",
                        "background-size": "contain",
                        "background-position": "center",
                      }}
                    />
                  ) : (
                    <div
                      style={{
                        background: `url(${design.image_thumbnail})`,
                        height: "234px",
                      }}
                    />
                  )}

                  <center>
                    <h5
                      style={{
                        backgroundColor: "#656565",
                        borderRadius: "40px",
                        color: "white",
                        font: "normal normal bold 15px/18px Encode Sans",
                        marginTop: "1vh",
                        paddingTop: "0.5vh",
                        paddingBottom: "0.5vh",
                        cursor: "pointer",
                      }}
                      onClick={() => setFullImage(!fullImage)}
                    >
                      {!fullImage ? (
                        <>VIEW FULL SHEET</>
                      ) : (
                        <>VIEW SMALL SHEET</>
                      )}
                    </h5>
                  </center>
                </div>
              </div>

              {valuesNames.length && (
                <div className="subcategory-dropdown">
                  <h6>
                    <b>SELECT SUB CATEGORY</b>
                  </h6>

                  <div className="desgin-dropdown-table">
                    <div class="btn-group">
                      <button
                        type="button"
                        class="btn btn-secondary dropdown-toggle"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                        style={{ width: "90vw" }}
                        onClick={() =>
                          setShowProductNamesList(!showProductNamesList)
                        }
                      >
                        {selectedProductName}
                      </button>

                      {showProductNamesList && (
                        <div
                          class="dropdown-menu dropdown-menu-right"
                          style={{
                            width: "90vw",
                            display: "unset",
                            backgroundColor: "#c0c1c1",
                          }}
                        >
                          {valuesNames.map((name) => (
                            <button
                              class="dropdown-item"
                              type="button"
                              onClick={() => {
                                setSelectedProductName(name);
                                setShowProductNamesList(false);
                              }}
                            >
                              {name}
                            </button>
                          ))}
                        </div>
                      )}
                    </div>

                    <div className="subcategory-table">
                      <table class="table table-bordered">
                        <tbody>
                          <tr>
                            <td>Finishes</td>
                            <td>{finish_str}</td>
                          </tr>
                          <tr>
                            <td>Species</td>
                            <td>{design.species}</td>
                          </tr>
                          <tr>
                            <td>Structure</td>
                            <td>{design.structure}</td>
                          </tr>
                          <tr>
                            <td>Standard Size (mm)</td>
                            <td>1220 * 2440</td>
                          </tr>
                          <tr>
                            <td>Standard Size (ft)</td>
                            <td>{design.size_feet}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              )}

              <div className="view-details-footer">
                <p style={{ fontSize: "10px" }}>
                  * Physical laminate color may vary from its digital
                  interpretation.
                </p>
                <center>
                  <h6 style={{ fontSize: "14px" }}>
                    <b>For minimum order quantity, please enquire.</b>
                  </h6>
                </center>
                <br />
                <br />
              </div>
              <div style={{ height: "10vh" }} />
            </div>
          </SlideInUpDiv>
        </div>
      </div>
    );
  };

  const deleteFilterAtIndex = (index) => {
    let copySelectedFilters = Object.assign([], selectedFilters);
    let copySelectedFiltersObject = Object.assign({}, selectedFiltersObject);
    if (index >= 0 && copySelectedFilters.length) {
      const filterObj = copySelectedFilters[index];
      const filterKey = Object.keys(filterObj)[0];
      const filterValue = Object.values(filterObj)[0];

      const arr = copySelectedFiltersObject[filterKey];

      const idx = arr.indexOf(filterValue);

      copySelectedFiltersObject[filterKey].splice(idx, 1);
      copySelectedFilters.splice(index, 1);

      setSelectedFilters(copySelectedFilters);
      setSelectedFiltersObject(copySelectedFiltersObject);
    }
  };

  const wishlistHandler = () => {
    const loggedIn = localStorage.getItem("logged_in");
    const designId = localStorage.getItem("design-id");
    if (!wishlisted) {
      setWishlisted(true);
      if (loggedIn && designId)
        apiActions.create_interaction(
          designId,
          "shortlisted",
          {},
          (res) => {
            setTimeout(() => apiActions.get_wishlist(), 5000);
            toast.success("Design added to wishlist successfully!", {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          },
          () => setWishlisted(false)
        );
      else {
        setShowAuthModal(true);
      }
    } else {
      setWishlisted(false);
      if (loggedIn && designId)
        apiActions.remove_interaction(
          designId,
          "shortlisted",
          {},
          (res) => setTimeout(() => apiActions.get_wishlist(), 5000),
          () => setWishlisted(true)
        );
      else {
        setShowAuthModal(true);
      }
    }
  };

  useEffect(() => {
    let copySelectedFiltersObject = { ...selectedFiltersObject };
    setRenderCount(renderCount + 1);
    const filters = props.location.state;
    const categories = Object.keys(filters);
    let updatedSelectedFilter = Object.assign([], selectedFilters);
    categories.map((category) => {
      let obj = {};
      const option = filters[category];
      obj[category] = option;
      updatedSelectedFilter.push(obj);
      copySelectedFiltersObject[category].push(option);
    });
    setSelectedFilters(updatedSelectedFilter);
    setSelectedFiltersObject(copySelectedFiltersObject);
  }, []);

  useEffect(() => {
    if (renderCount >= 1) {
      setLoadingRenderImages(true);
      setImagesArr([]);
      setInpirationsSceneIDsArr([]);
      setLoadingDesignData(true);
      setLoadingFamilyDesignData(true);
      setFamilyDesignDataRef([]);
      setTotalFamilyDesignData(0);
      setLoadingAvailableDesignData(true);
      setLoadingSimilarDesignData(true);
      setSimilarDesignDataRef([]);
      setTotalSimilarDesignData(0);
      setImgNumber(1);
      setIndex(0);

      // set family refs
      setFamilyDesignDataPageNoRef(1);
      if (document.getElementById("family")) {
        document
          .getElementById("family")
          .removeEventListener("scroll", trackScrollingFamily);

        document.getElementById("family").scrollTo({
          left: 0,
          behavior: "smooth",
        });
      }

      // set similar refs
      setSimilarDesignDataPageNoRef(1);
      if (document.getElementById("similar")) {
        document
          .getElementById("similar")
          .removeEventListener("scroll", trackScrollingSimilar);

        document.getElementById("similar").scrollTo({
          left: 0,
          behavior: "smooth",
        });
      }

      let queryFilter = {};

      if (selectedFiltersObject["surfaces"].length) {
        queryFilter = { ...selectedFiltersObject };
      } else {
        queryFilter = {
          ...selectedFiltersObject,
          // surfaces: selectedSurfaces,
        };
      }

      if (queryFilter["finish_type"].length) {
        queryFilter["finishes"] = queryFilter["finish_type"];
        delete queryFilter["finish_type"];
      }
      apiActions.get_designs(
        queryFilter,
        (res) => {
          let design;
          setLoadingAvailableDesignData(false);
          setAvailableDesignDataRef(res.data);
          setTotalAvailableDesignData(res.total_number);

          // add horizontal scroll event listener
          if (document.getElementById("available") && !res.is_last) {
            document
              .getElementById("available")
              .addEventListener("scroll", trackScrollingAvailable);
          }
          design = res.data[0];
          setCurrentDesign(design);
          if (design) {
            localStorage.setItem("design-id", design.design_id);
            localStorage.setItem("design", JSON.stringify(design));
            const designId = design.design_id;
            if (designId) {
              let wishlist = JSON.parse(localStorage.getItem("wishlist"));
              let likesArr = JSON.parse(localStorage.getItem("likes"));
              let wishlistIdx = wishlist.indexOf(designId);
              let likesIdx = likesArr.indexOf(designId);

              if (wishlistIdx >= 0) {
                setWishlisted(true);
              }

              if (likesIdx >= 0) {
                setLiked(true);
              }

              setLoadingRenderImages(true);
              // setImagesArr([]);
              setLoadingDesignData(true);
              setLoadingFamilyDesignData(true);
              setFamilyDesignDataRef([]);
              setTotalFamilyDesignData(0);

              setLoadingSimilarDesignData(true);
              setSimilarDesignDataRef([]);
              setTotalSimilarDesignData(0);

              setRenderCount(renderCount + 1);

              const selectedSurfaces = JSON.parse(
                localStorage.getItem("selected-surfaces")
              );
              const selectedSpace = JSON.parse(
                localStorage.getItem("selected-furniture")
              );
              // fetch render images
              let queryFilter1 = {};
              if (selectedFiltersObject["surfaces"].length) {
                queryFilter1 = {
                  design_id: localStorage.getItem("design-id"),
                  // ...selectedFiltersObject,
                  surfaces: selectedFiltersObject["surfaces"],
                };
              } else {
                queryFilter1 = {
                  design_id: localStorage.getItem("design-id"),
                  // room_name: selectedSpace[0],
                  // ...selectedFiltersObject,
                  // surfaces: selectedSurfaces,
                };
              }
              apiActions.get_inspirations(
                queryFilter1,
                (res) => {
                  //
                  const imagesArr1 = res.data.map((insp) => {
                    let obj = {
                      url: insp.rendering_image,
                      name:
                        insp.design_name +
                        "-" +
                        insp.design_id +
                        "-" +
                        insp.room_name +
                        "-" +
                        insp.finish_type,
                      finish_type: insp.finish_type,
                      scene_id: insp.scene_id,
                    };
                    return obj;
                  });
                  const imagesNamesArr = res.data.map(
                    (insp) =>
                      "Design " + insp.design_id + " - " + insp.design_name
                  );
                  let copyInspiartionsObj = sortInspirations(imagesArr1);
                  let inpirationsSceneIDsArr = Object.keys(copyInspiartionsObj);
                  let selectedFinishForScenes = {};
                  inpirationsSceneIDsArr.map((sceneID) => {
                    if (copyInspiartionsObj[sceneID]["Suede"]) {
                      selectedFinishForScenes[sceneID] = "Suede";
                    } else if (copyInspiartionsObj[sceneID]["Hi-Gloss"]) {
                      selectedFinishForScenes[sceneID] = "Hi-Gloss";
                    } else if (copyInspiartionsObj[sceneID]["MR"]) {
                      selectedFinishForScenes[sceneID] = "MR";
                    } else if (copyInspiartionsObj[sceneID]["metallic"]) {
                      selectedFinishForScenes[sceneID] = "metallic";
                    }
                  });
                  inpirationsSceneIDsArr = retainSceneID(
                    inpirationsSceneIDsArr
                  );

                  setInspirationsObj(copyInspiartionsObj);
                  setSelectedFinishForScenes(selectedFinishForScenes);
                  setInpirationsSceneIDsArr(inpirationsSceneIDsArr);
                  setImagesArr(imagesArr1);
                  setImgNumber(1);
                  setIndex(0);
                  setLoadingRenderImages(false);
                  setLoadedRenderImages(true);
                },
                (error) => {
                  alert("api failed to fetch the data");
                  setLoadingRenderImages(false);
                }
              );

              // if design data is fetched get similiar designs as well
              apiActions.get_similar_designs(
                localStorage.getItem("design"),
                (res) => {
                  setLoadedSimilarDesignData(true);
                  setLoadingSimilarDesignData(false);
                  // add horizontal scroll event listener
                  if (document.getElementById("similar") && !res.is_last)
                    document
                      .getElementById("similar")
                      .addEventListener("scroll", trackScrollingSimilar);
                  setSimilarDesignDataRef(res.data);
                  setTotalSimilarDesignData(res.total_number);
                },
                () => {
                  alert("failed to fetch desgin family data");
                  setLoadingFamilyDesignData(false);
                }
              );

              // if design data is fetched, get family of designs as well
              apiActions.get_design_family(
                localStorage.getItem("design"),
                (res) => {
                  setLoadedFamilyDesignData(true);
                  setLoadingFamilyDesignData(false);
                  // add horizontal scroll event listener
                  if (document.getElementById("family") && !res.is_last)
                    document
                      .getElementById("family")
                      .addEventListener("scroll", trackScrollingFamily);
                  setFamilyDesignDataRef(res.data);
                  setTotalFamilyDesignData(res.total_number);
                },
                () => {
                  alert("failed to fetch desgin family data");
                  setLoadingFamilyDesignData(false);
                }
              );
            }
          } else {
            setLoadingRenderImages(false);
            setLoadingDesignData(false);
            setLoadingFamilyDesignData(false);
            setLoadingSimilarDesignData(false);
          }
        },
        () => {
          alert("failed to apply the filters");
          setLoadingDesignData(false);
        }
      );
    }
  }, [selectedFiltersObject]);

  useEffect(() => {
    if (localStorage.getItem("design-id") && designID != "") {
      setLoadingRenderImages(true);
      setLoadingDesignData(true);
      setLoadingFamilyDesignData(true);
      setTotalFamilyDesignData(0);
      setLoadingSimilarDesignData(true);
      setTotalSimilarDesignData(0);

      setRenderCount(renderCount + 1);

      const selectedSurfaces = JSON.parse(
        localStorage.getItem("selected-surfaces")
      );
      const selectedSpace = JSON.parse(
        localStorage.getItem("selected-furniture")
      );
      // fetch render images
      let queryFilter2 = {};
      if (selectedFiltersObject["surfaces"].length) {
        queryFilter2 = {
          design_id: localStorage.getItem("design-id"),
          surfaces: selectedFiltersObject["surfaces"],
        };
      } else {
        queryFilter2 = {
          design_id: localStorage.getItem("design-id"),
        };
      }
      apiActions.get_inspirations(
        queryFilter2,
        (res) => {
          const imagesArr1 = res.data.map((insp) => {
            let obj = {
              url: insp.rendering_image,
              name:
                "Design " +
                insp.design_id +
                " - " +
                insp.design_name +
                "-" +
                insp.finish_type,
              finish_type: insp.finish_type,
              scene_id: insp.scene_id,
            };
            return obj;
          });

          const imagesNamesArr = res.data.map(
            (insp) => "Design " + insp.design_id + " - " + insp.design_name
          );
          let copyInspiartionsObj = sortInspirations(imagesArr1);
          let inpirationsSceneIDsArr = Object.keys(copyInspiartionsObj);
          let selectedFinishForScenes = {};
          inpirationsSceneIDsArr.map((sceneID) => {
            if (copyInspiartionsObj[sceneID]["Suede"]) {
              selectedFinishForScenes[sceneID] = "Suede";
            } else if (copyInspiartionsObj[sceneID]["Hi-Gloss"]) {
              selectedFinishForScenes[sceneID] = "Hi-Gloss";
            } else if (copyInspiartionsObj[sceneID]["MR"]) {
              selectedFinishForScenes[sceneID] = "MR";
            } else if (copyInspiartionsObj[sceneID]["metallic"]) {
              selectedFinishForScenes[sceneID] = "metallic";
            }
          });

          inpirationsSceneIDsArr = retainSceneID(inpirationsSceneIDsArr);

          setInspirationsObj(copyInspiartionsObj);
          setSelectedFinishForScenes(selectedFinishForScenes);
          setInpirationsSceneIDsArr(inpirationsSceneIDsArr);
          setImagesArr(imagesArr1);
          setImgNumber(1);
          setIndex(0);
          setLoadingRenderImages(false);
          setLoadedRenderImages(true);
        },
        (error) => {
          alert("api failed to fetch the data");
          setLoadingRenderImages(false);
        }
      );

      // fetch design data
      apiActions.get_design(
        localStorage.getItem("design-id"),
        () => {
          setLoadingDesignData(false);
          setLoadedDesignData(true);

          // if design data is fetched get similiar designs as well
          apiActions.get_similar_designs(
            localStorage.getItem("design"),
            (res) => {
              setLoadedSimilarDesignData(true);
              setLoadingSimilarDesignData(false);
              // add horizontal scroll event listener
              if (document.getElementById("similar") && !res.is_last)
                document
                  .getElementById("similar")
                  .addEventListener("scroll", trackScrollingSimilar);
              setSimilarDesignDataRef(res.data);
              setTotalSimilarDesignData(res.total_number);
            },
            () => {
              alert("failed to fetch desgin family data");
              setLoadingFamilyDesignData(false);
            }
          );

          // if design data is fetched, get family of designs as well
          apiActions.get_design_family(
            localStorage.getItem("design"),
            (res) => {
              setLoadedFamilyDesignData(true);
              setLoadingFamilyDesignData(false);
              // add horizontal scroll event listener
              if (document.getElementById("family") && !res.is_last)
                document
                  .getElementById("family")
                  .addEventListener("scroll", trackScrollingFamily);
              setFamilyDesignDataRef(res.data);
              setTotalFamilyDesignData(res.total_number);
            },
            () => {
              alert("failed to fetch desgin family data");
              setLoadingFamilyDesignData(false);
            }
          );
        },
        () => {
          alert("failed to fetch desgin data");
          setLoadingDesignData(false);
        }
      );
    }
  }, [designID]);

  useEffect(() => {
    if (renderCount >= 1) {
      // fetch render images
      let queryFilter2 = {};
      if (selectedFiltersObject["surfaces"].length) {
        queryFilter2 = {
          design_id: localStorage.getItem("design-id"),
          surfaces: selectedFiltersObject["surfaces"],
        };
      } else {
        queryFilter2 = {
          design_id: localStorage.getItem("design-id"),
        };
      }
      apiActions.get_inspirations(
        queryFilter2,
        (res) => {
          const imagesArr1 = res.data.map((insp) => {
            let obj = {
              url: insp.rendering_image,
              name:
                "Design " +
                insp.design_id +
                " - " +
                insp.design_name +
                "-" +
                insp.finish_type,
              finish_type: insp.finish_type,
              scene_id: insp.scene_id,
            };
            return obj;
          });

          const imagesNamesArr = res.data.map(
            (insp) => "Design " + insp.design_id + " - " + insp.design_name
          );
          let copyInspiartionsObj = sortInspirations(imagesArr1);
          let inpirationsSceneIDsArr = Object.keys(copyInspiartionsObj);
          let selectedFinishForScenes = {};
          inpirationsSceneIDsArr.map((sceneID) => {
            if (copyInspiartionsObj[sceneID]["Suede"]) {
              selectedFinishForScenes[sceneID] = "Suede";
            } else if (copyInspiartionsObj[sceneID]["Hi-Gloss"]) {
              selectedFinishForScenes[sceneID] = "Hi-Gloss";
            } else if (copyInspiartionsObj[sceneID]["MR"]) {
              selectedFinishForScenes[sceneID] = "MR";
            } else if (copyInspiartionsObj[sceneID]["metallic"]) {
              selectedFinishForScenes[sceneID] = "metallic";
            }
          });

          inpirationsSceneIDsArr = retainSceneID(inpirationsSceneIDsArr);

          setInspirationsObj(copyInspiartionsObj);
          setSelectedFinishForScenes(selectedFinishForScenes);
          setInpirationsSceneIDsArr(inpirationsSceneIDsArr);
          // setImagesNamesArr(imagesNamesArr);
          setImagesArr(imagesArr1);
          setImgNumber(1);
          setIndex(0);
          setLoadingRenderImages(false);
          setLoadedRenderImages(true);
        },
        (error) => {
          alert("api failed to fetch the data");
          setLoadingRenderImages(false);
        }
      );
    }
  }, [loadOnlyInspirations]);

  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };

  const loader = () => {
    return (
      <div style={{ width: "100vw" }}>
        <center>
          <img
            src={Loader}
            style={{
              // position: "absolute",
              // top: "50%",
              // left: "50%",
              height: "10vh",
              width: "auto",
              // transform: "translate(-50%, -50%)",
              zIndex: 5,
            }}
          />
        </center>
      </div>
    );
  };

  const switchAvailableDesignIDHandler = (design) => {
    localStorage.setItem("design-id", design.design_id);
    localStorage.setItem("design", JSON.stringify(design));

    setWishlisted(false);
    setLiked(false);

    if (inpirationsSceneIDsArr.length) {
      setCurrentSelectedScene(inpirationsSceneIDsArr[imgNumber - 1]);
    }
    const designId = design.design_id;

    let wishlist = JSON.parse(localStorage.getItem("wishlist"));
    let likesArr = JSON.parse(localStorage.getItem("likes"));

    let wishlistIdx = wishlist.indexOf(designId);
    let likesIdx = likesArr.indexOf(designId);

    if (wishlistIdx >= 0) {
      setWishlisted(true);
    }

    if (likesIdx >= 0) {
      setLiked(true);
    }

    setDesignID(design.design_id);
    setCurrentDesign(design);
  };

  const switchDesignIDHandler = (design) => {
    localStorage.setItem("design-id", design.design_id);
    localStorage.setItem("design", JSON.stringify(design));

    setWishlisted(false);
    setLiked(false);

    if (inpirationsSceneIDsArr.length)
      setCurrentSelectedScene(inpirationsSceneIDsArr[imgNumber - 1]);

    const designId = design.design_id;

    let wishlist = JSON.parse(localStorage.getItem("wishlist"));
    let likesArr = JSON.parse(localStorage.getItem("likes"));

    let wishlistIdx = wishlist.indexOf(designId);
    let likesIdx = likesArr.indexOf(designId);

    if (wishlistIdx >= 0) {
      setWishlisted(true);
    }

    if (likesIdx >= 0) {
      setLiked(true);
    }

    setLoadingRenderImages(true);
    setLoadOnlyInspirations(!loadOnlyInspirations);

    setCurrentDesign(design);
  };

  const cleanCategory = (category) => {
    const idx = category.indexOf("_");
    let str = category;
    if (idx > -1) {
      str = category.slice(0, idx);
    }

    let uCaseStr = str.toUpperCase();
    if (uCaseStr == "PRODUCT") uCaseStr = "PRODUCTS";
    else if (uCaseStr == "DESIGN") uCaseStr = "DESIGNS";
    else if (uCaseStr == "FINISH") uCaseStr = "FINISH TYPES";
    else if (uCaseStr == "STRUCTURE") uCaseStr = "STRUCTURES";
    else if (uCaseStr == "COLOUR") uCaseStr = "COLORS";

    return uCaseStr;
  };

  const retainSceneID = (inpirationsSceneIDsArr) => {
    // if previous scene_id is available put it in the beginning
    if (currentSelectedScene != "") {
      // alert(currentSelectedScene);
      const idx = inpirationsSceneIDsArr.findIndex(
        (sceneID) => sceneID == currentSelectedScene
      );
      if (idx >= 0) {
        // alert(idx);
        let tempObj = inpirationsSceneIDsArr[idx];
        inpirationsSceneIDsArr[idx] = inpirationsSceneIDsArr[0];
        inpirationsSceneIDsArr[0] = tempObj;
      }
    }

    return inpirationsSceneIDsArr;
  };

  const sortInspirations = (imgArr) => {
    let inspirationsObj = {};
    let sortedImagesArr = [...imgArr];
    sortedImagesArr.sort((x, y) => {
      if (x.scene_id > y.scene_id) return 1;
      // when both suede and gloss are avialable for a design
      else if (x.scene_id == y.scene_id) {
        inspirationsObj[x.scene_id] = {};
        inspirationsObj[x.scene_id][x.finish_type] = x;
        inspirationsObj[y.scene_id][y.finish_type] = y;
        return -1;
      } else return -1;
    });
    // when either suede or gloss is avialable for a design
    sortedImagesArr.map((img) => {
      if (!inspirationsObj[img.scene_id]) {
        inspirationsObj[img.scene_id] = {};
        inspirationsObj[img.scene_id][img.finish_type] = img;
      }
    });
    return inspirationsObj;
  };

  return (
    <div
      style={{
        backgroundColor: `#${
          currentThemeSelection === "lite" ? "white" : "3c3599"
        }`,
        height: "auto",
        marginTop: "21vw",
      }}
      className="search-page"
    >
      <div className="filter-wrapper">
        <div
          className="scrollmenu"
          style={{
            paddingLeft: "0.5rem",
            paddingTop: "1.5rem",
            borderBottom: "0px solid #00000029",
          }}
        >
          <div className="filter-icon-div">
            <svg
              id="filter-filled-tool-symbol"
              xmlns="http://www.w3.org/2000/svg"
              width="21.82"
              height="16.791"
              viewBox="0 0 21.82 16.791"
              className="filter-icon"
            >
              <path
                id="Path_35288"
                data-name="Path 35288"
                d="M12.7,7.934a.917.917,0,0,1,.358.7v7.635c0,.46.727.693,1.158.37L17.005,14.2c.374-.342.58-.511.58-.85V8.637a.924.924,0,0,1,.358-.7L25.958,1.3c.6-.5.138-1.3-.75-1.3H5.431c-.888,0-1.352.8-.75,1.3Z"
                transform="translate(-4.41)"
                fill="#7b7b7b"
              />
            </svg>
          </div>

          {/* </div> */}

          {filterCategoriesKeys &&
            filterCategoriesKeys.map((category) => (
              <div
                // class="col-4 col-sm-4 col-md-3 col-lg-2 col-xl-1"
                onClick={() => {
                  setSelectedCatIdx(-1);
                  setSelectedFilterCategory(category);
                }}
                className="scrollmenu-div"
                style={
                  {
                    // border: "1px solid #70707038",
                    // borderRadius: "15px",
                    // height: "30px",
                  }
                }
              >
                {cleanCategory(category)}

                <ArrowDropDownIcon />
              </div>
            ))}

          {/* filter bar */}

          {/* <div
          class="col-4 col-sm-3 col-md-3 col-lg-2 col-xl-1"
          onClick={() => setSelectedFilter("color")}
        >
          Color <ArrowDropDownIcon />{" "}
        </div> */}
        </div>

        <div
          className="scrollmenu-filters"
          style={{
            paddingLeft: "0.5rem",
            paddingTop: ".5rem",
          }}
        >
          {/* filter bar */}
          {selectedFilters.length ? (
            selectedFilters.map((filter, index) => (
              <div className="filter-bar">
                {Object.values(filter)[0]}
                {Object.keys(filter)[0] != "default" && (
                  <HighlightOffIcon
                    className="remove-filter-icon"
                    onClick={() => deleteFilterAtIndex(index)}
                  />
                )}
              </div>
            ))
          ) : (
            <h6>
              <b
                style={{
                  font: "normal normal bold 14px/16px Encode Sans",
                  letterSpacing: " -0.29px",
                  color: "#585858",
                }}
              >
                Apply the above filters to sort out based on your needs
              </b>
              .
            </h6>
          )}
        </div>
      </div>
      {!loadingAvailableDesignData ? (
        <>
          <div className="selected-design">
            {loadingRenderImages ? (
              <>
                <div
                  style={{
                    height: "40vh",
                    paddingTop: "12vh",
                    position: "absolute",
                    width: "100vw",
                    zIndex: 100,
                  }}
                >
                  <center>
                    <img
                      src={Loader}
                      style={{
                        // position: "absolute",
                        // top: "50%",
                        // left: "50%",
                        height: "10vh",
                        width: "auto",
                        // transform: "translate(-50%, -50%)",
                        zIndex: 5,
                      }}
                    />
                  </center>
                </div>
                <Carousel
                  activeIndex={index}
                  onSelect={handleSelect}
                  nextIcon={
                    imgNumber < inpirationsSceneIDsArr.length ? (
                      <span
                        aria-hidden="true"
                        className="carousel-control-next-icon"
                        disabled
                        onClick={() =>
                          imgNumber == inpirationsSceneIDsArr.length
                            ? null
                            : setImgNumber(imgNumber + 1)
                        }
                        style={{ backgroundColor: "black", height: "5vh" }}
                      />
                    ) : null
                  }
                  prevIcon={
                    imgNumber > 1 ? (
                      <span
                        aria-hidden="true"
                        className="carousel-control-prev-icon"
                        disabled={imgNumber == 0 ? true : false}
                        onClick={() =>
                          imgNumber == 1 ? null : setImgNumber(imgNumber - 1)
                        }
                        style={{ backgroundColor: "black", height: "5vh" }}
                      />
                    ) : null
                  }
                  interval={null}
                  touch={false}
                  style={{ height: "30vh" }}
                >
                  {inpirationsSceneIDsArr.map((sceneID) => (
                    <Carousel.Item>
                      <img
                        className="d-block w-100"
                        src={
                          inspirationsObj[sceneID] &&
                          inspirationsObj[sceneID][
                            selectedFinishForScenes[sceneID]
                          ] &&
                          inspirationsObj[sceneID][
                            selectedFinishForScenes[sceneID]
                          ].url
                        }
                        alt="First slide"
                        style={{
                          height: "auto",
                          width: "100%",
                          minHeight: "30vh",
                        }}
                      />
                      <Carousel.Caption
                        style={{
                          position: "absolute",
                          top: "0vh",
                          paddingTop: "0.5vh",
                          paddingBottom: "0vh",
                          fontSize: "0.8rem",
                          height: "fit-content",
                          background: "rgba(0,0,0,0.4)",
                          paddingTop: "3px",
                          paddingBottom: "0px",
                          color: "white",
                          width: "100vw",
                          paddingLeft: "0vw !important",
                          right: "0%",
                          left: "0%",
                          zIndex: 1,
                          // display: "flex",
                          // alignItems: "center",
                        }}
                      >
                        <h6 style={{ fontSize: "0.8rem", lineHeight: "1" }}>
                          {inspirationsObj[sceneID] &&
                            inspirationsObj[sceneID][
                              selectedFinishForScenes[sceneID]
                            ] &&
                            inspirationsObj[sceneID][
                              selectedFinishForScenes[sceneID]
                            ].name}
                        </h6>
                      </Carousel.Caption>

                      <Carousel.Caption
                        style={{
                          position: "absolute",
                          bottom: "0%",
                          left: "0%",
                          paddingTop: "0.5vh",
                          paddingBottom: "0.5vh",
                          backgroundColor: "#000000",
                          height: "fit-content",
                          opacity: "0.8",
                          color: "white",
                          width: "15vw",
                          zIndex: 1,
                          // paddingLeft: "vw",
                          // right: "0%",
                          // left: "0%",
                          // display: "flex",
                          // alignItems: "center",
                        }}
                      >
                        {imgNumber}
                        {"/"}
                        {inpirationsSceneIDsArr.length}
                      </Carousel.Caption>
                    </Carousel.Item>
                  ))}
                </Carousel>
              </>
            ) : inpirationsSceneIDsArr.length ? (
              <Carousel
                activeIndex={index}
                onSelect={handleSelect}
                nextIcon={
                  imgNumber < inpirationsSceneIDsArr.length ? (
                    <span
                      aria-hidden="true"
                      className="carousel-control-next-icon"
                      disabled
                      onClick={() =>
                        imgNumber == inpirationsSceneIDsArr.length
                          ? null
                          : setImgNumber(imgNumber + 1)
                      }
                      style={{ backgroundColor: "black", height: "5vh" }}
                    />
                  ) : null
                }
                prevIcon={
                  imgNumber > 1 ? (
                    <span
                      aria-hidden="true"
                      className="carousel-control-prev-icon"
                      disabled={imgNumber == 0 ? true : false}
                      onClick={() =>
                        imgNumber == 1 ? null : setImgNumber(imgNumber - 1)
                      }
                      style={{ backgroundColor: "black", height: "5vh" }}
                    />
                  ) : null
                }
                interval={null}
                touch={false}
              >
                {inpirationsSceneIDsArr.map((sceneID) => (
                  <Carousel.Item>
                    <img
                      className="d-block w-100"
                      src={
                        inspirationsObj[sceneID] &&
                        inspirationsObj[sceneID][
                          selectedFinishForScenes[sceneID]
                        ] &&
                        inspirationsObj[sceneID][
                          selectedFinishForScenes[sceneID]
                        ].url
                      }
                      alt="First slide"
                      style={{
                        height: "auto",
                        width: "100%",
                        minHeight: "30vh",
                      }}
                    />
                    <Carousel.Caption
                      style={{
                        position: "absolute",
                        top: "0vh",
                        paddingTop: "0.5vh",
                        paddingBottom: "0vh",
                        fontSize: "0.8rem",
                        height: "fit-content",
                        background: "rgba(0,0,0,0.4)",
                        paddingTop: "3px",
                        paddingBottom: "0px",
                        color: "white",
                        width: "100vw",
                        paddingLeft: "0vw !important",
                        right: "0%",
                        left: "0%",
                        zIndex: 1,
                        // display: "flex",
                        // alignItems: "center",
                      }}
                    >
                      <h6 style={{ fontSize: "0.8rem", lineHeight: "1" }}>
                        {inspirationsObj[sceneID] &&
                          inspirationsObj[sceneID][
                            selectedFinishForScenes[sceneID]
                          ] &&
                          inspirationsObj[sceneID][
                            selectedFinishForScenes[sceneID]
                          ].name}
                      </h6>
                    </Carousel.Caption>

                    <Carousel.Caption
                      style={{
                        position: "absolute",
                        bottom: "0%",
                        left: "0%",
                        paddingTop: "0.5vh",
                        paddingBottom: "0.5vh",
                        backgroundColor: "#000000",
                        height: "fit-content",
                        opacity: "0.8",
                        color: "white",
                        width: "15vw",
                        zIndex: 1,
                        // paddingLeft: "vw",
                        // right: "0%",
                        // left: "0%",
                        // display: "flex",
                        // alignItems: "center",
                      }}
                    >
                      {imgNumber}
                      {"/"}
                      {inpirationsSceneIDsArr.length}
                    </Carousel.Caption>

                    {/* suede and gloss filter */}
                    <Carousel.Caption
                      style={{
                        position: "absolute",
                        bottom: "1%",
                        left: "69%",
                        paddingTop: "0.2vh",
                        paddingBottom: "0.2vh",
                        backgroundColor:
                          selectedFinishForScenes[sceneID] == "Suede"
                            ? "black"
                            : "#585858",
                        height: "fit-content",
                        opacity: "0.8",
                        color: "white",
                        width: "fit-content",
                        zIndex: 1,
                        fontSize: "12px",
                        paddingLeft: "1.5vw",
                        paddingRight: "1.5vw",
                        borderRadius: "10px",
                        display:
                          inspirationsObj[sceneID] &&
                          inspirationsObj[sceneID]["Suede"]
                            ? "unset"
                            : "none",

                        // filterInspirationsArr[0] == "Hi-Gloss" &&
                        // !remainingImagesArr.includes(img.scene_id) &&
                        // "none",

                        // paddingLeft: "vw",
                        // right: "0%",
                        // left: "0%",
                        // display: "flex",
                        // alignItems: "center",
                      }}
                    >
                      <button
                        onClick={() => {
                          if (selectedFinishForScenes[sceneID] == "Hi-Gloss") {
                            let copySelectedFinishForScenes = {
                              ...selectedFinishForScenes,
                            };
                            copySelectedFinishForScenes[sceneID] = "Suede";
                            setSelectedFinishForScenes(
                              copySelectedFinishForScenes
                            );
                          }
                        }}
                      >
                        Suede
                      </button>
                    </Carousel.Caption>

                    <Carousel.Caption
                      style={{
                        position: "absolute",
                        bottom: "1%",
                        left: "84%",
                        paddingTop: "0.2vh",
                        paddingBottom: "0.2vh",
                        backgroundColor:
                          selectedFinishForScenes[sceneID] == "Hi-Gloss"
                            ? "black"
                            : "#585858",
                        height: "fit-content",
                        opacity: "0.8",
                        color: "white",
                        width: "fit-content",
                        zIndex: 1,
                        fontSize: "12px",
                        paddingLeft: "1.5vw",
                        paddingRight: "1.5vw",
                        borderRadius: "10px",
                        display:
                          inspirationsObj[sceneID] &&
                          inspirationsObj[sceneID]["Hi-Gloss"]
                            ? "unset"
                            : "none",

                        // paddingLeft: "vw",
                        // right: "0%",
                        // left: "0%",
                        // display: "flex",
                        // alignItems: "center",
                      }}
                    >
                      <button
                        onClick={() => {
                          if (selectedFinishForScenes[sceneID] == "Suede") {
                            let copySelectedFinishForScenes = {
                              ...selectedFinishForScenes,
                            };
                            copySelectedFinishForScenes[sceneID] = "Hi-Gloss";
                            setSelectedFinishForScenes(
                              copySelectedFinishForScenes
                            );
                          }
                        }}
                      >
                        Gloss
                      </button>
                    </Carousel.Caption>
                  </Carousel.Item>
                ))}
              </Carousel>
            ) : (
              <div style={{ height: "10vh", paddingTop: "5vh" }}>
                <center>
                  <h5>
                    <b>No images to display :(</b>
                  </h5>
                </center>
              </div>
            )}
          </div>

          {inpirationsSceneIDsArr.length ? (
            <div className="design-actions-wrapper">
              <div className="design-actions row">
                <div className=" design-action col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 ">
                  <img
                    src={liked ? LikedIcon : LikeIcon}
                    className="like-icon"
                    onClick={() => {
                      const likesArr = JSON.parse(
                        localStorage.getItem("likes")
                      );
                      const designId = localStorage.getItem("design-id");
                      if (!liked) {
                        setLiked(true);
                        // push desing id to array
                        likesArr.push(designId);
                        if (!localStorage.getItem("logged_in") && !wishlisted)
                          setShowSignUpModal(true);
                      } else {
                        setLiked(false);
                        const idx = likesArr.indexOf(designId);
                        likesArr.splice(idx, 1);
                      }
                      localStorage.setItem("likes", JSON.stringify(likesArr));
                    }}
                  />
                  <span>Like</span>
                  {/* <p>Like</p> */}
                </div>
                <div className=" design-action col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                  <img
                    src={wishlisted ? WishlistedIcon : WishlistIcon}
                    className="wishlist-icon"
                    onClick={() => {
                      localStorage.getItem("logged_in")
                        ? wishlistHandler()
                        : setShowAuthModal(true);
                    }}
                  />

                  <span>Add to wishlist</span>
                </div>
                <div className=" design-action col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                  <img
                    src={ZoomInIcon}
                    className="zoom-in-icon"
                    onClick={() => setShowZoomInModal(true)}
                  />
                  <span>Zoom in</span>
                </div>
                <div className=" design-action col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                  <img
                    src={ViewDetailsIcon}
                    className="view-details-icon"
                    onClick={() => {
                      setSelectedProductName("");
                      setShowProductNamesList(false);
                      setShowViewDetailsModal(true);
                    }}
                  />
                  <span>View Full Sheet</span>
                </div>
              </div>
            </div>
          ) : null}

          <div className="designs-cat-wrapper">
            <div className="design-section">
              <div className="title">
                <b>
                  AVAILABLE DESIGNS{" "}
                  {!loadingAvailableDesignData
                    ? "(" + totalAvailableDesignData + ")"
                    : null}
                </b>
              </div>

              <div
                className="items row flex-row flex-nowrap horizontal-design-scroll"
                id="available"
                style={{
                  paddingLeft: "2vw",
                  paddingRight: "2vw",
                  marginLeft: "0",
                  marginRight: "0",
                  height: "43vw",
                }}
              >
                {loadingAvailableDesignData ? (
                  loader()
                ) : (
                  <>
                    {availableDesignData.map((design) => (
                      <div
                        className="item col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 "
                        style={{
                          marginTop: "1vh",
                          paddingLeft: "2vw",
                          paddingRight: "2vw",
                        }}
                      >
                        <div
                          style={{
                            backgroundImage: `url(${design.image_thumbnail})`,
                            height: "90px",
                            cursor: "pointer",
                            border: currentDesign
                              ? currentDesign.design_id == design.design_id
                                ? "8px solid #585858"
                                : ""
                              : "",
                          }}
                          onClick={() => switchAvailableDesignIDHandler(design)}
                        />
                        <div className="item-name">
                          <h6>
                            <b>{design.design_name + " " + design.design_id}</b>
                          </h6>
                        </div>
                      </div>
                    ))}

                    {loadingMoreAvailableDesignData && (
                      <div
                        className="item col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4 "
                        style={{
                          marginTop: "4vh",
                        }}
                      >
                        <center>
                          <img
                            src="/static/media/loader1.b1f8355b.gif"
                            style={{ height: "10vh", zIndex: 5 }}
                          />
                        </center>
                      </div>
                    )}
                  </>
                )}
              </div>
            </div>
            <div className="design-section">
              <div className="title">
                <b>
                  FAMILY OF DESIGNS{" "}
                  {!loadingFamilyDesignData
                    ? "(" + totalFamilyDesignData + ")"
                    : null}
                </b>
              </div>
              <div style={{ position: "relative", height: "43vw" }}>
                {loadingFamilyDesignData && (
                  <div className="loader-wrp">{loader()}</div>
                )}
                <div
                  className="items row flex-row flex-nowrap horizontal-design-scroll"
                  id="family"
                  style={{
                    paddingLeft: "2vw",
                    paddingRight: "2vw",
                    marginLeft: "0",
                    marginRight: "0",
                    height: "43vw",
                  }}
                >
                  {familyDesignData.map((design) => (
                    <div
                      className="item col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 "
                      style={{
                        marginTop: "1vh",
                        paddingLeft: "2vw",
                        paddingRight: "2vw",
                      }}
                    >
                      <div
                        style={{
                          backgroundImage: `url(${design.image_thumbnail})`,
                          height: "90px",
                          cursor: "pointer",
                          border: currentDesign
                            ? currentDesign.design_id == design.design_id
                              ? "8px solid #585858"
                              : ""
                            : "",
                        }}
                        onClick={() => switchDesignIDHandler(design)}
                      />
                      <div className="item-name">
                        <h6>
                          <b>{design.design_name + " " + design.design_id}</b>
                        </h6>
                      </div>
                    </div>
                  ))}

                  {loadingMoreFamilyDesignData && (
                    <div
                      className="item col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4 "
                      style={{
                        marginTop: "4vh",
                      }}
                    >
                      <center>
                        <img
                          src="/static/media/loader1.b1f8355b.gif"
                          style={{ height: "10vh", zIndex: 5 }}
                        />
                      </center>
                    </div>
                  )}
                </div>
              </div>
            </div>
            <div className="design-section">
              <div className="title">
                <b>
                  SIMILAR DESIGNS{" "}
                  {!loadingSimilarDesignData
                    ? "(" + totalSimilarDesignData + ")"
                    : null}
                </b>
              </div>
              <div style={{ position: "relative", height: "43vw" }}>
                {loadingSimilarDesignData && (
                  <div className="loader-wrp">{loader()}</div>
                )}
                <div
                  className="items row flex-row flex-nowrap horizontal-design-scroll"
                  id="similar"
                  style={{
                    paddingLeft: "2vw",
                    paddingRight: "2vw",
                    marginLeft: "0",
                    marginRight: "0",
                    // height: "43vw",
                  }}
                >
                  {similarDesignData.map((design) => (
                    <div
                      className="item col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 "
                      style={{
                        marginTop: "1vh",
                        paddingLeft: "2vw",
                        paddingRight: "2vw",
                      }}
                    >
                      <div
                        style={{
                          backgroundImage: `url(${design.image_thumbnail})`,
                          height: "90px",
                          cursor: "pointer",
                          border: currentDesign
                            ? currentDesign.design_id == design.design_id
                              ? "8px solid #585858"
                              : ""
                            : "",
                        }}
                        onClick={() => switchDesignIDHandler(design)}
                      />
                      <div className="item-name">
                        <h6>
                          <b>{design.design_name + " " + design.design_id}</b>
                        </h6>
                      </div>
                    </div>
                  ))}

                  {loadingMoreSimilarDesignData && (
                    <div
                      className="item col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4 "
                      style={{
                        marginTop: "4vh",
                      }}
                    >
                      <center>
                        <img
                          src="/static/media/loader1.b1f8355b.gif"
                          style={{ height: "10vh", zIndex: 5 }}
                        />
                      </center>
                    </div>
                  )}
                </div>
              </div>
            </div>{" "}
          </div>
        </>
      ) : (
        loader()
      )}

      {selectedFilterCategory == "product_names" && getPrimaryListProduct()}
      {selectedFilterCategory != "" &&
        selectedFilterCategory != "product_names" &&
        getList()}

      {selectedFilterCategory == "rooms" && getPrimaryListSpace()}

      {selectedFilterCategory != "" && selectedCatIdx != -1 && getList()}
      {showViewDetailsModal != "" && getViewDetailsModal()}
      {showZoomInModal != "" && getShowZoomInModal()}
      {showSignUpModal != "" && getSignInPopUp()}
      {showWishlistModal != "" && getWishlistModal()}
      {showAuthModal != "" && getAuthModal()}
    </div>
  );
};

export default withRouter(ExplorationPage);
