import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardMedia from "@material-ui/core/CardMedia";
import CancelIcon from "@material-ui/icons/Cancel";
import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import chatbot1 from "../../public/assets/chatbot1.png";
import chatbot2 from "../../public/assets/chatbot2.png";
import Collage from "../../public/assets/collage.jpg";
import Explore2 from "../../public/assets/explore-2.jpeg";
import Explore3 from "../../public/assets/explore-3.jpeg";
import Explore4 from "../../public/assets/explore-4.jpeg";
import Explore5 from "../../public/assets/explore-5.jpeg";
import Explore6 from "../../public/assets/explore-6.jpeg";
import Explore1 from "../../public/assets/explore.1.jpg";
import { apiActions } from "../../server";
import AuthPopup from "../components/auth-popup";
import { useThemeSelection } from "../hooks/theme-hook";
import "../styles/home-page.css";
import history from "../util/history-util";

const Home = () => {
  const { currentThemeSelection } = useThemeSelection();
  const [showSignUpModal, setShowSignUpModal] = useState(false);
  const [showAssistantPopupFirst, setShowAssistantPopupFirst] = useState(true);
  const [showAssistantPopupSecond, setShowAssistantPopupSecond] = useState(
    false
  );

  const [showAuthModal, setShowAuthModal] = useState(false);
  const [loggedIn, setLoggedIn] = useState(localStorage.getItem("logged_in"));

  useEffect(() => {
    if (!localStorage.getItem("wishlist"))
      localStorage.setItem("wishlist", JSON.stringify([]));

    if (!localStorage.getItem("likes"))
      localStorage.setItem("likes", JSON.stringify([]));

    if (localStorage.getItem("show-assistant-flow")) {
      if (localStorage.getItem("show-assistant-flow") == "no") {
        setShowAssistantPopupFirst(false);
      }
    }

    localStorage.removeItem("inspirations");
    localStorage.removeItem("fetchedAllInspirations");
    localStorage.removeItem("selectedFilters");
    localStorage.removeItem("selectedFiltersObj");
    localStorage.removeItem("design-id");
    localStorage.removeItem("pageNumber");

    apiActions.get_wishlist();
  }, []);

  const getSignInPopUp = () => {
    return (
      <div className="popup-overlay">
        <div className="coming-soon-modal-wrapper">
          <CancelIcon
            className="close-modal-coming-soon"
            onClick={() => setShowSignUpModal(false)}
          />
          <center>
            <div className="coming-soon-modal">
              <h6 style={{ font: "normal normal bold 18px/28px Encode Sans" }}>
                <b>Coming soon</b>
              </h6>
              <br />
              <center>
                <h5
                  style={{
                    backgroundColor: "#656565",
                    borderRadius: "40px",
                    color: "white",
                    font: "normal normal bold 15px/18px Encode Sans",
                    marginTop: "1vh",
                    paddingTop: "0.6vh",
                    paddingBottom: "0.6vh",
                    width: "20vw",
                  }}
                  onClick={() => {
                    setShowSignUpModal(false);
                  }}
                >
                  Okay
                </h5>
              </center>
            </div>
          </center>
        </div>
      </div>
    );
  };

  const assistantPopupFirst = () => {
    return (
      <div className="popup-overlay">
        <div className="assistant-center-modal-wrapper">
          <CancelIcon
            className="close-modal-assistant-center"
            style={{ zIndex: 10000 }}
            onClick={() => {
              setShowAssistantPopupFirst(false);

              localStorage.setItem("show-assistant-flow", "no");

              {
                !loggedIn && setShowAssistantPopupSecond(true);
              }
            }}
          />

          <div className="assistant-center-modal">
            <div className="content-inline">
              <div className="assistant-center-modal-text">
                <b>
                  Hello Customer! <br />
                  Welcome to Merino Laminates. <br /> I'm Dave, your virtual
                  assistant. <br />
                  Do you need me to help you explore the best designs offered by
                  us?
                </b>
                <br />
              </div>

              <div className="assistant-center-modal-image">
                <img src={chatbot1} />
              </div>
            </div>

            <div className="assistant-center-modal-options">
              <center>
                <div onClick={() => history.push("/color")}>
                  <center>VIEW LAMINATES AS PER COLORS</center>
                </div>

                <div onClick={() => history.push("/space")}>
                  <center>VIEW LAMINATES AS PER ROOMS</center>
                </div>

                <div onClick={() => history.push("/product-category")}>
                  <center>VIEW LAMINATES AS PER PRODUCT CATEGORIES</center>
                </div>

                <div onClick={() => history.push("/laminates-design")}>
                  <center>VIEW LAMINATES AS PER PREFERRED DESIGNS</center>
                </div>

                <div onClick={() => history.push("/search")}>
                  <center>SEE DESIGN INSPIRATION</center>
                </div>

                <div onClick={() => setShowAssistantPopupFirst(false)}>
                  <center>NO IT'S OK! I WILL EXPLORE ON MY OWN</center>
                </div>
              </center>
            </div>
          </div>
        </div>
      </div>
    );
  };

  const assistantPopupSecond = () => {
    return (
      <div className="popup-overlay">
        <div className="assistant-bottom-right-modal-wrapper">
          <CancelIcon
            className="close-modal-assistant-bottom-right"
            onClick={() => setShowAssistantPopupSecond(false)}
          />

          <div className="assistant-bottom-right-modal">
            <div className="content-inline">
              <div className="assistant-bottom-right-modal-text">
                <b>
                  Sign up now for a more personalized experience with Merino
                  Laminates.
                </b>
                <br />
              </div>

              <div className="assistant-bottom-right-modal-image">
                <img src={chatbot2} />
              </div>
            </div>

            <br />
            <br />
            <div className="assistant-bottom-right-modal-options">
              <div
                onClick={() => {
                  setShowAssistantPopupSecond(false);
                  setShowAuthModal(true);
                }}
              >
                <center>
                  <center>SIGN UP</center>
                </center>
              </div>

              <div onClick={() => setShowAssistantPopupSecond(false)}>
                <center>
                  <center>SKIP</center>
                </center>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  return (
    <div
      style={{
        backgroundColor: `#${
          currentThemeSelection === "lite" ? "white" : "3c3599"
        }`,
        height: "auto",
        marginTop: "13vh",
        padding: "0.8rem",
      }}
      className="home-page-categories"
    >
      <div className="explore-now">
        <center>
          <h5
            style={{
              font: "normal normal bold 17px/20px Encode Sans",
              letterSpacing: "3px",
              color: " #585858",
            }}
          >
            EXPLORE NOW
          </h5>
        </center>
        <div className="row">
          <div
            className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
            style={{ marginTop: "2vh" }}
          >
            <Card
              className="card-style"
              onClick={() => {
                history.push("/laminates-design");
                const option = "Bedroom";
              }}
            >
              <CardMedia
                image={Explore4}
                style={{
                  height: 0,
                  paddingTop: "56.25%",
                }}
              />

              <CardActions
                style={{
                  justifyContent: "center",
                  marginTop: "-6vh",
                  paddingBottom: "0px",
                  paddingLeft: "0px",
                  paddingRight: "0px",
                }}
              >
                <Button
                  style={{
                    color: "white",
                    width: "100%",
                    background: "rgba(0,0,0,0.4)",
                    paddingTop: "0px",
                    paddingBottom: "0px",
                    borderRadius: "0px",
                    fontSize: "12px",
                    font: "normal normal medium 16px/20px Encode Sans",
                  }}
                  size="big"
                >
                  <b
                    style={{
                      fontFamily: "Encode Sans",
                      fontSize: "0.8rem",
                      fontWeight: "500",
                      textTransform: "none",
                    }}
                  >
                    {" "}
                    View Laminates as per Designs
                  </b>
                </Button>
              </CardActions>
            </Card>
          </div>
          <div
            className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
            style={{ marginTop: "2vh" }}
          >
            <Card
              className="card-style"
              onClick={() => {
                const fetchedOptions = localStorage.getItem("fetched_options");
                const spaceFilterOptions = localStorage.getItem(
                  "space_fetched_options"
                );
                if (1) {
                  history.push("/space");
                } else {
                  alert("Settting up few thing, please wait!");
                }
              }}
            >
              <CardMedia
                image={Explore2}
                style={{
                  height: 0,
                  paddingTop: "56.25%",
                }}
              />

              <CardActions
                style={{
                  justifyContent: "center",
                  marginTop: "-6vh",
                  paddingBottom: "0px",
                  paddingLeft: "0px",
                  paddingRight: "0px",
                }}
              >
                <Button
                  style={{
                    color: "white",
                    width: "100%",
                    background: "rgba(0,0,0,0.4)",
                    paddingTop: "0px",
                    paddingBottom: "0px",
                    borderRadius: "0px",
                    fontSize: "12px",
                    font: "normal normal medium 16px/20px Encode Sans",
                  }}
                  size="big"
                >
                  <b
                    style={{
                      fontFamily: "Encode Sans",
                      fontSize: "0.8rem",
                      fontWeight: "500",
                      textTransform: "none",
                    }}
                  >
                    {" "}
                    View Laminates as per Rooms
                  </b>
                </Button>
              </CardActions>
            </Card>
          </div>
          <div
            className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
            style={{ marginTop: "2vh" }}
          >
            <Card
              className="card-style"
              onClick={() => {
                const fetchedOptions = localStorage.getItem("fetched_options");
                const spaceFilterOptions = localStorage.getItem(
                  "space_fetched_options"
                );
                if (1) {
                  history.push("/furniture");
                } else {
                  alert("Settting up few thing, please wait!");
                }
              }}
            >
              <CardMedia
                image={Explore5}
                style={{
                  height: 0,
                  paddingTop: "56.25%",
                }}
              />

              <CardActions
                style={{
                  justifyContent: "center",
                  marginTop: "-6vh",
                  paddingBottom: "0px",
                  paddingLeft: "0px",
                  paddingRight: "0px",
                }}
              >
                <Button
                  style={{
                    color: "white",
                    width: "100%",
                    background: "rgba(0,0,0,0.4)",
                    paddingTop: "0px",
                    paddingBottom: "0px",
                    borderRadius: "0px",
                    fontSize: "12px",
                    font: "normal normal medium 16px/20px Encode Sans",
                  }}
                  size="big"
                >
                  <b
                    style={{
                      fontFamily: "Encode Sans",
                      fontSize: "0.8rem",
                      fontWeight: "500",
                      textTransform: "none",
                      // letterSpacing: "1.2px",
                    }}
                  >
                    {" "}
                    View Laminates as per Furniture
                  </b>
                </Button>
              </CardActions>
            </Card>
          </div>
          <div
            className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
            style={{ marginTop: "2vh" }}
          >
            <Card
              className="card-style"
              onClick={() => {
                history.push("/product-category");
                const option = "Bedroom";
              }}
            >
              <CardMedia
                image={Explore3}
                style={{
                  height: 0,
                  paddingTop: "56.25%",
                }}
              />

              <CardActions
                style={{
                  justifyContent: "center",
                  marginTop: "-6vh",
                  paddingBottom: "0px",
                  paddingLeft: "0px",
                  paddingRight: "0px",
                }}
              >
                <Button
                  style={{
                    color: "white",
                    width: "100%",
                    background: "rgba(0,0,0,0.4)",
                    paddingTop: "0px",
                    paddingBottom: "0px",
                    borderRadius: "0px",
                    fontSize: "12px",
                    font: "normal normal medium 16px/20px Encode Sans",
                  }}
                  size="big"
                >
                  <b
                    style={{
                      fontFamily: "Encode Sans",
                      fontSize: "0.8rem",
                      fontWeight: "500",
                      textTransform: "none",
                    }}
                  >
                    {" "}
                    View Laminates as per Categories
                  </b>
                </Button>
              </CardActions>
            </Card>
          </div>
          <div
            className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
            style={{ marginTop: "2vh" }}
          >
            <Card
              className="card-style"
              className="card-style"
              onClick={() => {
                const fetchedOptions = localStorage.getItem("fetched_options");
                if (1) {
                  history.push("/color");
                } else {
                  alert("Settting up few thing, please wait!");
                }
              }}
            >
              <CardMedia
                image={Explore1}
                style={{
                  height: 0,
                  paddingTop: "56.25%",
                }}
              />

              <CardActions
                style={{
                  justifyContent: "center",
                  marginTop: "-6vh",
                  paddingBottom: "0px",
                  paddingLeft: "0px",
                  paddingRight: "0px",
                }}
              >
                <Button
                  style={{
                    color: "white",
                    width: "100%",
                    background: "rgba(0,0,0,0.4)",
                    paddingTop: "0px",
                    paddingBottom: "0px",
                    borderRadius: "0px",
                    fontSize: "12px",
                    font: "normal normal medium 16px/20px Encode Sans",
                  }}
                  size="big"
                >
                  <b
                    style={{
                      fontFamily: "Encode Sans",
                      fontSize: "0.8rem",
                      fontWeight: "500",
                      textTransform: "none",
                    }}
                  >
                    {" "}
                    View Laminates as per Colors
                  </b>
                </Button>
              </CardActions>
            </Card>
          </div>
        </div>
      </div>

      <br />
      <div
        className="inspirations"
        onClick={() => {
          const fetchedOptions = localStorage.getItem("fetched_options");
          if (1) {
            history.push("/search");
          } else {
            alert("Settting up inspiration page, please wait!");
          }
        }}
        style={{ cursor: "pointer" }}
      >
        <center>
          <h5
            style={{
              font: "normal normal bold 17px/20px Encode Sans",
              letterSpacing: "3px",
              color: " #585858",
            }}
          >
            INSPIRATIONS
          </h5>
        </center>
        <div className="row">
          <div
            className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
            style={{ marginTop: "2vh" }}
          >
            <Card className="card-style">
              <CardMedia
                image={Collage}
                style={{
                  height: 0,
                  paddingTop: "56.25%",
                }}
              />
            </Card>
          </div>
        </div>
      </div>

      <br />
      <div className="campaign">
        <center>
          <h5
            style={{
              font: "normal normal bold 17px/20px Encode Sans",
              letterSpacing: "3px",
              color: " #585858",
            }}
          >
            OUR LATEST CAMPAIGN
          </h5>
        </center>
        <div className="row">
          <div
            className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
            style={{ marginTop: "2vh" }}
          >
            <div className="video-responsive">
              <iframe
                width="100%"
                height="315"
                src="https://www.youtube.com/embed/Dv-lOKN_H-M?rel=0"
                title="YouTube video player"
                frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen
              ></iframe>
            </div>
          </div>
        </div>
      </div>
      <br />

      <div className="society-and-environment">
        <center>
          <h5
            style={{
              font: "normal normal bold 17px/20px Encode Sans",
              letterSpacing: "3px",
              color: " #585858",
            }}
          >
            SOCIETY AND ENVIRONMENT
          </h5>
        </center>
        <div className="row">
          <div
            className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
            style={{ marginTop: "2vh" }}
          >
            <a
              href="https://www.merinolaminates.com/en/environment-sustainability/"
              rel="noopener noreferrer"
              target="_blank"
            >
              <Card className="card-style">
                <CardMedia
                  image={Explore6}
                  style={{
                    height: 0,
                    paddingTop: "56.25%", // 16:9
                  }}
                />
              </Card>
            </a>
          </div>
        </div>
      </div>

      <br />

      {showSignUpModal != "" && getSignInPopUp()}

      {showAssistantPopupFirst && assistantPopupFirst()}
      {showAssistantPopupSecond && assistantPopupSecond()}
      {showAuthModal && (
        <AuthPopup
          hideAuthModal={() => setShowAuthModal(false)}
          setLoggedIn={setLoggedIn}
        />
      )}
      <div style={{ height: "10vh" }} />
    </div>
  );
};

export default withRouter(Home);
