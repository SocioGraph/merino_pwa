import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardMedia from "@material-ui/core/CardMedia";
import CancelIcon from "@material-ui/icons/Cancel";
import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import ArrowUp from "../../public/assets/arrow-up-squared.png";
import chatbot from "../../public/assets/chatbot.png";
import chatbot1 from "../../public/assets/chatbot1.png";
import Loader from "../../public/assets/loader1.gif";
import { apiActions } from "../../server";
import { useThemeSelection } from "../hooks/theme-hook";
import "../styles/home-page.css";
import { filterCategories } from "../util/filters";
import history from "../util/history-util";

const ColorPage = () => {
  const { currentThemeSelection } = useThemeSelection();
  const [showSignUpModal, setShowSignUpModal] = useState(false);

  const colorsArr = filterCategories.colour;

  const [colorDesignData, setColorDesignData] = useState([]);
  const [loadingColorDesignData, setLoadingColorDesignData] = useState(false);

  const [showAssistantPopupFirst, setShowAssistantPopupFirst] = useState(true);
  const [showAssistantPopupSecond, setShowAssistantPopupSecond] = useState(
    false
  );
  const [isVisibleTop, setIsVisibleTop] = useState(false);
  const [totalCount, setTotalCount] = useState(0);

  const [selectedColor, setSelectedColor] = useState("");

  const [colorOfDesignPageNo, setColorDesignDataPageNo] = useState(1);
  const [loadingMoreColorDesignData, setLoadingMoreColorDesignData] = useState(
    false
  );

  const [scollListener, setScrollListener] = useState(false);

  const scollListenerRef = React.useRef(scollListener);
  const setScrollListenerRef = (data) => {
    scollListenerRef.current = data;
    setScrollListener(data);
  };

  const selectedColorRef = React.useRef(selectedColor);
  const setSelectedColorRef = (data) => {
    selectedColorRef.current = data;
    setSelectedColor(data);
  };

  // ref hooks to use state in event handlers
  const colorDesignDataRef = React.useRef(colorDesignData);
  const setColorDesignDataRef = (data) => {
    colorDesignDataRef.current = data;
    setColorDesignData(data);
  };

  const colorOfDesignPageNoRef = React.useRef(colorOfDesignPageNo);
  const setColorDesignDataPageNoRef = (data) => {
    colorOfDesignPageNoRef.current = data;
    setColorDesignDataPageNo(data);
  };

  const loadingMoreColorDesignDataRef = React.useRef(
    loadingMoreColorDesignData
  );
  const setLoadingMoreColorDesignDataRef = (data) => {
    loadingMoreColorDesignDataRef.current = data;
    setLoadingMoreColorDesignData(data);
  };

  const toggleVisibility = () => {
    if (window.pageYOffset > 300) {
      setIsVisibleTop(true);
    } else {
      setIsVisibleTop(false);
    }
  };

  // Set the top cordinate to 0
  // make scrolling smooth
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  useEffect(() => {
    const str = window.location.pathname;
    apiActions.patch_user(
      {
        pwa_flow: str,
        _async: true,
      },

      (res) => console.log(res),
      (err) => console.log(err)
    );
    window.addEventListener("scroll", toggleVisibility);
  }, []);

  const replaceColorArr = {
    white: "#f5f7f9",
    "light-orange": "rgb(253,165,102)",
    "light-yellow": "rgb(255,243,155)",
    "light-green": "rgb(210,255,215)",
    "light-aqua": "rgb(159,255,246)",
    "light-blue": "rgb(154,208,255)",
    "light-indigo": "rgb(169,170,252)",
    "light-purple": "rgb(251,184,236)",
    pink: "rgb(255,210,210)",
    grey: "rgb(135,135,135)",
    orange: "rgb(253,106,0)",
    yellow: "rgb(237,220,94)",
    green: "rgb(11,245,37)",
    aqua: "rgb(0,234,218)",
    blue: "rgb(19,142,249)",
    indigo: "rgb(19,26,253)",
    violet: "rgb(115,9,255)",
    purple: "rgb(208,3,162)",
    red: "rgb(247,0,0)",
    black: "rgb(48,48,48)",
    brown: "rgb(102,48,13)",
    olive: "rgb(78,69,0)",
    "dark-green": "rgb(0,101,11)",
    "dark-aqua": "rgb(0,130,117)",
    "dark-blue": "rgb(1,47,86)",
    "dark-indigo": "rgb(0,4,136)",
    "dark-violet": "rgb(51,1,117)",
    champagne: "rgb(80,0,62)",
  };

  const max = (a, b) => (a > b ? a : b);

  const loadColorDesignsForPage = () => {
    let queryFilter = {};
    queryFilter._page_number = colorOfDesignPageNoRef.current + 1;
    queryFilter.colour = selectedColorRef.current;
    setLoadingMoreColorDesignData(true);

    apiActions.get_designs(
      queryFilter,
      (res) => {
        if (res.is_last) {
          setScrollListenerRef(false);
        } else {
          setScrollListenerRef(true);
          setColorDesignDataPageNoRef(queryFilter._page_number);
        }
        let updatedColorDesignData = [
          ...colorDesignDataRef.current,
          ...res.data,
        ];
        setColorDesignDataRef(updatedColorDesignData);
        setLoadingMoreColorDesignDataRef(false);
      },
      () => {
        alert("failed to fetch desgin family data");
        setLoadingColorDesignData(false);
      }
    );
  };

  const isBottom = (el) => {
    if (el.scrollHeight - el.clientHeight - el.scrollTop <= 2) return true;
    return false;
  };

  const trackScrollingColor = (e) => {
    const wrappedElement = document.getElementById("inspirations");
    if (isBottom(wrappedElement)) {
      loadColorDesignsForPage();
    }
  };

  const loadDesignsForColor = (colour) => {
    setScrollListenerRef(false);
    setSelectedColorRef(colour);
    setLoadingColorDesignData(true);
    setColorDesignDataPageNoRef(1);

    apiActions.get_designs(
      { colour },
      (res) => {
        setTotalCount(res.total_number);
        setColorDesignDataRef(res.data);
        // add scroll event listener
        if (!res.is_last) {
          setScrollListenerRef(true);
        } else {
          setScrollListenerRef(false);
        }
        setLoadingColorDesignData(false);
      },
      (error) => setLoadingColorDesignData(false)
    );
  };

  const getMetallicIcon = (color) => (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="38px"
      height="55.21px"
      viewBox="0 0 79.6 92"
      style={{ marginLeft: "0.7vw" }}
      onClick={() => {
        loadDesignsForColor(color);
        localStorage.setItem("selected-color", color);
      }}
    >
      {" "}
      <defs>
        {" "}
        <linearGradient id="grad2" x1="0%" y1="0%" x2="0%" y2="100%">
          {" "}
          <stop
            offset="0%"
            style={{ stopColor: "rgb(255, 0, 0)", stopOpacity: 1 }}
          />{" "}
          <stop
            offset="100%"
            // style="stop-color:rgb(255,255,0);stop-opacity:1"
          />{" "}
        </linearGradient>{" "}
      </defs>{" "}
      <path
        id="Path1"
        data-name="Path 1664"
        d="M39.8,0l39.8,23.1l0,44.5L40.2,92L0,68.8l0.1-46L39.8,0z"
        fill="url(#grad2)"
      />{" "}
    </svg>
  );

  const getAbstractIcon = (color) => (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="38px"
      height="55.21px"
      viewBox="0 0 79.6 92"
      style={{ marginLeft: "0.7vw" }}
      onClick={() => {
        loadDesignsForColor(color);
        localStorage.setItem("selected-color", color);
      }}
    >
      {" "}
      <defs>
        {" "}
        <pattern
          id="img1"
          patternUnits="userSpaceOnUse"
          width="100"
          height="100"
        >
          {" "}
          <image
            href="https://www.merinolaminates.com/wp-content/themes/merino/merinoweb-ui/assets/img/pattern.png"
            x="0"
            y="0"
            width="100"
            height="100"
          />{" "}
        </pattern>{" "}
      </defs>{" "}
      <path
        id="Path2"
        data-name="Path 1664"
        d="M39.8,0l39.8,23.1l0,44.5L40.2,92L0,68.8l0.1-46L39.8,0z"
        fill="url(#img1)"
      />{" "}
    </svg>
  );

  const assistantPopupSecond = () => {
    return (
      <div className="popup-overlay">
        <div className="assistant-center-options-modal-wrapper">
          <CancelIcon
            className="close-modal-assistant-center-options"
            onClick={() => setShowAssistantPopupSecond(false)}
          />

          <div className="assistant-center-options-modal">
            <div className="content-inline">
              <div className="assistant-center-options-modal-text">
                <b>You're looking designs for ?</b>
                <br />
              </div>

              <div className="assistant-center-options-modal-image">
                <img src={chatbot1} />
              </div>
            </div>

            <div className="assistant-center-options-modal-options">
              <center>
                <div
                  onClick={() => {
                    setShowAssistantPopupSecond(false);
                  }}
                >
                  <center>
                    <center>Home</center>
                  </center>
                </div>

                <div onClick={() => setShowAssistantPopupSecond(false)}>
                  <center>
                    <center>Garage</center>
                  </center>
                </div>

                <div onClick={() => setShowAssistantPopupSecond(false)}>
                  <center>
                    <center>Office</center>
                  </center>
                </div>
              </center>
            </div>
          </div>
        </div>
      </div>
    );
  };

  const selectedColorStr = localStorage.getItem("selected-color");

  return (
    <div
      style={{
        backgroundColor: `#${
          currentThemeSelection === "lite" ? "white" : "3c3599"
        }`,
        height: "85vh",
        overflowY: "scroll",
        marginTop: "16vh",
        padding: "4vw",
      }}
      id="inspirations"
      onScroll={() =>
        scollListener &&
        !loadingMoreColorDesignData &&
        !loadingColorDesignData &&
        trackScrollingColor()
      }
      className="home-page-categories"
    >
      <div className="explore-now">
        <center>
          <h5
            style={{
              letterSpacing: "1px",
              color: " #585858",
              fontWeight: "bold",
              fontSize: "19px",
            }}
          >
            COLOR BAR
          </h5>
        </center>
      </div>

      <div className="color-bar-section">
        {/* <div className="color-bar-title">
          <p>COLOR BAR</p>
        </div> */}

        <div className="color-bar-text">
          <p>
            Explore beautiful shades of diverse surface ideas from Merino and
            find out exactly why our surfaces are the best choice for renovating
            and designing your spaces.
          </p>
        </div>

        {colorsArr.map((color, idx) => (
          <style
            dangerouslySetInnerHTML={{
              __html: [
                `.color-${idx}:after {`,
                `  border-top: 10px solid  ${
                  replaceColorArr[color] ? replaceColorArr[color] : color
                };`,
                `}
              .color-${idx}:before {`,
                `  bottom: "100%";`,
                `  transform:  rotate(180deg);`,
                `  border-top: 10px solid  ${
                  replaceColorArr[color] ? replaceColorArr[color] : color
                };`,
                `}
              .color-${idx} {`,
                `  background-color: ${
                  replaceColorArr[color] ? replaceColorArr[color] : color
                };`,
                `}`,
              ].join("\n"),
            }}
          />
        ))}

        <div className="color-pallet">
          <div className="first-row">
            {colorsArr.slice(0, 7).map((color, idx) =>
              color == "metallic" ? (
                getMetallicIcon(color)
              ) : color == "abstract" ? (
                getAbstractIcon(color)
              ) : (
                <div
                  className={"color color-" + idx}
                  onClick={() => {
                    loadDesignsForColor(color);
                    localStorage.setItem("selected-color", color);
                  }}
                />
              )
            )}
          </div>
          <div className="second-row">
            {colorsArr.slice(7, 13).map((color, idx) =>
              color == "metallic" ? (
                getMetallicIcon(color)
              ) : color == "abstract" ? (
                getAbstractIcon(color)
              ) : (
                <div
                  className={"color color-" + (idx + 7)}
                  onClick={() => {
                    loadDesignsForColor(color);
                    localStorage.setItem("selected-color", color);
                  }}
                />
              )
            )}
          </div>
          <div className="third-row">
            {colorsArr.slice(13, 20).map((color, idx) =>
              color == "metallic" ? (
                getMetallicIcon(color)
              ) : color == "abstract" ? (
                getAbstractIcon(color)
              ) : (
                <div
                  className={"color color-" + (idx + 13)}
                  onClick={() => {
                    loadDesignsForColor(color);
                    localStorage.setItem("selected-color", color);
                  }}
                />
              )
            )}
          </div>
          <div className="fourth-row">
            {colorsArr.slice(20, max(colorsArr.lenght, 26)).map((color, idx) =>
              color == "metallic" ? (
                getMetallicIcon(color)
              ) : color == "abstract" ? (
                getAbstractIcon(color)
              ) : (
                <div
                  className={"color color-" + (idx + 20)}
                  onClick={() => {
                    loadDesignsForColor(color);
                    localStorage.setItem("selected-color", color);
                  }}
                />
              )
            )}
          </div>

          <div className="fifth-row">
            {colorsArr.slice(26, 29).map((color, idx) =>
              color == "metallic" ? (
                getMetallicIcon(color)
              ) : color == "abstract" ? (
                getAbstractIcon(color)
              ) : (
                <a href="#exp-finish">
                  <div
                    className={"color color-" + (idx + 26)}
                    onClick={() => {
                      loadDesignsForColor(color);
                      localStorage.setItem("selected-color", color);
                    }}
                  />
                </a>
              )
            )}
          </div>

          <div className="third-row" />
        </div>
      </div>

      <div style={{ height: "3vh" }} />
      {loadingColorDesignData ? (
        <center>
          <img
            src={Loader}
            style={{
              height: "10vh",
              width: "auto",
              // transform: "translate(-50%, -50%)",
              zIndex: 5,
            }}
          />
        </center>
      ) : (
        <div className="row" style={{ paddingTop: "2vh" }}>
          {colorDesignData.length ? (
            <div style={{ margin: "auto", letterSpacing: "1px" }}>
              <h6>
                <b>
                  {selectedColorStr[0].toUpperCase() +
                    selectedColorStr.slice(1)}
                </b>{" "}
                ({totalCount})
              </h6>
            </div>
          ) : null}

          {colorDesignData.map((insp) => (
            <div
              className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
              style={{ marginTop: "1vh" }}
            >
              <Card
                className="card-style"
                onClick={() => {
                  localStorage.setItem("design", JSON.stringify(insp));
                  localStorage.setItem("design-id", insp.design_id);
                  history.push("/color-second");
                }}
              >
                <CardMedia
                  image={insp.design_image}
                  style={{
                    height: 0,
                    paddingTop: "56.25%", // 16:9
                  }}
                />

                <CardActions
                  style={{
                    justifyContent: "center",
                    marginTop: "-6vh",
                    paddingBottom: "0px",
                    paddingLeft: "0px",
                    paddingRight: "0px",
                  }}
                >
                  <Button
                    style={{
                      color: "white",
                      width: "100%",
                      background: "rgba(0,0,0,0.4)",
                      zIndex: 1,

                      paddingTop: "0px",
                      paddingBottom: "0px",
                      borderRadius: "0px",
                      fontSize: "12px",
                      font: "normal normal medium 20px/24px Encode Sans",
                    }}
                    size="big"
                  >
                    <b
                      style={{
                        fontFamily: "Encode Sans",
                        fontSize: "14px",
                        letterSpacing: "1.2px",
                      }}
                    >
                      {" "}
                      {insp.design_name + "-" + insp.design_id}
                    </b>
                  </Button>
                </CardActions>
              </Card>
            </div>
          ))}

          <span style={{ width: "100%" }}>
            {loadingMoreColorDesignData ? (
              <center>
                <img
                  src={Loader}
                  style={{
                    height: "10vh",
                    width: "auto",
                    zIndex: 5,
                  }}
                />
              </center>
            ) : null}
          </span>
        </div>
      )}

      {/* {showAssistantPopupFirst && assistantPopupFirst()} */}
      {showAssistantPopupSecond && assistantPopupSecond()}

      <div style={{ height: "10vh" }} />
      {isVisibleTop && (
        <div
          style={{
            height: 35,
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            width: 35,
            position: "fixed",
            bottom: 80,
            right: 20,
            borderRadius: 100,
            overflow: "hidden",
            backgroundColor: "rgb(245, 245, 245)",
            zIndex: 5,
          }}
          onClick={() => {
            scrollToTop();
          }}
        >
          <img
            src={ArrowUp}
            style={{
              height: 25,
              width: 25,
              zIndex: 5,
            }}
          />
        </div>
      )}
    </div>
  );
};

export default withRouter(ColorPage);
