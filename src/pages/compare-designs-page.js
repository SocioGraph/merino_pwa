import CancelIcon from "@material-ui/icons/Cancel";
import GetAppIcon from "@material-ui/icons/GetApp";
import SettingsBackupRestoreIcon from "@material-ui/icons/SettingsBackupRestore";
import ShareIcon from "@material-ui/icons/Share";
import React, { useEffect, useState } from "react";
import SlideInUp from "react-animations/lib/slide-in-up";
import Carousel from "react-bootstrap/Carousel";
import { withRouter } from "react-router-dom";
import styled, { keyframes } from "styled-components";
import Loader from "../../public/assets/loader1.gif";
import { apiActions } from "../../server";
import { useThemeSelection } from "../hooks/theme-hook";
import "../styles/design-page.css";
import "../styles/search-page.css";
import { filterCategories } from "../util/filters";

const _ = require("lodash");
const SlideInUpAnimation = keyframes`${SlideInUp}`;

const SlideInUpDiv = styled.div`
  animation: 0.2s ${SlideInUpAnimation};
`;

const CompareDesignsPage = () => {
  const { currentThemeSelection } = useThemeSelection();
  const [selectedFilters, setSelectedFilters] = useState([]);
  const [selectedFiltersObject, setSelectedFiltersObject] = useState({
    structure: [],
    species: [],
    finish_type: [],
    design_category: [],
    product_names: [],
  });

  const [loadingRenderImages, setLoadingRenderImages] = useState(false);
  const [loadedRenderImages, setLoadedRenderImages] = useState(false);
  const [loadingDesignData, setLoadingDesignData] = useState(false);
  const [loadedDesignData, setLoadedDesignData] = useState(false);

  const [similarDesignData, setSimilarDesignData] = useState([]);
  const [loadingSimilarDesignData, setLoadingSimilarDesignData] = useState(
    false
  );
  const [loadedSimilarDesignData, setLoadedSimilarDesignData] = useState(false);

  const [familyDesignData, setFamilyDesignData] = useState([]);
  const [loadingFamilyDesignData, setLoadingFamilyDesignData] = useState(false);
  const [loadedFamilyDesignData, setLoadedFamilyDesignData] = useState(false);

  const [imagesArr, setImagesArr] = useState([]);
  const [imagesNamesArr, setImagesNamesArr] = useState([]);

  const [renderCount, setRenderCount] = useState(0);
  const [selectedAuth, setSelectedAuth] = useState("signup");
  const [loginFields, handleLoginFieldChange] = useState({
    email: "",
    password: "",
  });

  const [signupFields, handleSignupFieldChange] = useState({
    username: "",
    name: "",
    contact: "",
    email: "",
    password: "",
    repassword: "",
  });

  const [designID, setDesignID] = useState("");
  const [currentDesign, setCurrentDesign] = useState({});

  const [imgNumber, setImgNumber] = useState(1);

  const [selectedDesignIndex, setSelectedDesignIndex] = useState(0);

  useEffect(() => {
    if (localStorage.getItem("wishlist")) {
      let wishlist = JSON.parse(localStorage.getItem("wishlist"));
      let designId = designID == "" ? wishlist[0] : designID;

      if (designId) {
        setLoadingRenderImages(true);
        apiActions.get_inspirations(
          { design_id: designId },
          (res) => {
            const imagesArr = res.data.map((insp) => {
              let obj = {
                url: insp.rendering_image,
                name: "Design " + insp.design_id + " - " + insp.design_name,
              };
              return obj;
            });
            setImagesArr(imagesArr);
            setLoadingRenderImages(false);
            setLoadedRenderImages(true);
          },
          (error) => {
            alert("api failed to fetch the data");
            setLoadingRenderImages(true);
          }
        );
      }
    }
  }, [designID]);

  useEffect(() => {
    // Set list for similar designs
    if (localStorage.getItem("wishlist")) {
      let wishlist = JSON.parse(localStorage.getItem("wishlist"));
      let designs = JSON.parse(localStorage.getItem("designs"));

      let similarDesignsArr = wishlist
        .filter((id) => designs[id])
        .map((id) => designs[id]);

      setSimilarDesignData(similarDesignsArr);
    }
  }, []);

  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };

  const loader = () => {
    return (
      <div style={{ width: "100vw" }}>
        <center>
          <img
            src={Loader}
            style={{
              height: "10vh",
              width: "auto",
              zIndex: 5,
            }}
          />
        </center>
      </div>
    );
  };

  const switchDesignIDHandler = (design, idx) => {
    setDesignID(design.design_id);
    setCurrentDesign(design);
    setSelectedDesignIndex(idx);
  };

  return (
    <div
      style={{
        backgroundColor: `#${
          currentThemeSelection === "lite" ? "white" : "3c3599"
        }`,
        height: "auto",
        marginTop: "13vh",
      }}
      className="search-page"
    >
      <div className="selected-design">
        {loadingRenderImages ? (
          <div style={{ height: "30vh" }}>
            <div style={{ height: "10vh" }} />
            <center>
              <img
                src={Loader}
                style={{
                  height: "10vh",
                  width: "auto",
                  zIndex: 5,
                }}
              />
            </center>
          </div>
        ) : imagesArr.length ? (
          <Carousel
            activeIndex={index}
            onSelect={handleSelect}
            nextIcon={
              imgNumber < imagesArr.length ? (
                <span
                  aria-hidden="true"
                  className="carousel-control-next-icon"
                  disabled
                  onClick={() =>
                    imgNumber == imagesArr.length
                      ? null
                      : setImgNumber(imgNumber + 1)
                  }
                  style={{ backgroundColor: "black", height: "5vh" }}
                />
              ) : null
            }
            prevIcon={
              imgNumber > 1 ? (
                <span
                  aria-hidden="true"
                  className="carousel-control-prev-icon"
                  disabled={imgNumber == 0 ? true : false}
                  onClick={() =>
                    imgNumber == 1 ? null : setImgNumber(imgNumber - 1)
                  }
                  style={{ backgroundColor: "black", height: "5vh" }}
                />
              ) : null
            }
            interval={null}
            touch={false}
          >
            {imagesArr.map((img) => (
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src={img.url}
                  alt="First slide"
                  style={{
                    height: "auto",
                    width: "100%",
                    minHeight: "30vh",
                  }}
                />
                <Carousel.Caption
                  style={{
                    position: "absolute",
                    top: "0vh",
                    paddingTop: "0.5vh",
                    paddingBottom: "0vh",
                    zIndex: 1,
                    height: "fit-content",
                    background: "rgba(0,0,0,0.4)",
                    paddingTop: "0px",
                    paddingBottom: "0px",
                    color: "white",
                    width: "100vw",
                    paddingLeft: "0vw !important",
                    right: "0%",
                    left: "0%",
                  }}
                >
                  <h6 style={{ fontSize: "0.8rem", lineHeight: "1" }}>
                    {img.name}
                  </h6>
                </Carousel.Caption>

                <Carousel.Caption
                  style={{
                    position: "absolute",
                    bottom: "0%",
                    left: "0%",
                    paddingTop: "0.5vh",
                    paddingBottom: "0.5vh",
                    backgroundColor: "#000000",
                    height: "fit-content",
                    opacity: "0.8",
                    color: "white",
                    width: "15vw",
                    zIndex: 1,
                  }}
                >
                  {imgNumber}
                  {"/"}
                  {imagesArr.length}
                </Carousel.Caption>
              </Carousel.Item>
            ))}
          </Carousel>
        ) : (
          <div style={{ height: "10vh", paddingTop: "5vh" }}>
            <center>
              <h5>
                <b>No images to display :(</b>
              </h5>
            </center>
          </div>
        )}
      </div>

      <div className="design-section">
        <div
          className="items row flex-row flex-nowrap horizontal-design-scroll"
          style={{
            paddingLeft: "2vw",
            paddingRight: "2vw",
            marginLeft: "0",
            marginRight: "0",
          }}
        >
          {loadingSimilarDesignData ? (
            loader()
          ) : (
            <>
              {similarDesignData.map((design, idx) => (
                <div
                  className="item col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 "
                  style={{
                    marginTop: "1vh",
                    paddingLeft: "2vw",
                    paddingRight: "2vw",
                  }}
                >
                  <div
                    style={{
                      backgroundImage: `url(${design.image_thumbnail})`,
                      height: "90px",
                      cursor: "pointer",
                      border:
                        idx == selectedDesignIndex ? "8px solid #585858" : null,
                    }}
                    onClick={() => switchDesignIDHandler(design, idx)}
                  />
                  <div className="item-name">
                    <h6>
                      <b>{design.design_name + " " + design.design_id}</b>
                    </h6>
                  </div>
                </div>
              ))}
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default withRouter(CompareDesignsPage);
