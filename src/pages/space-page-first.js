import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardMedia from "@material-ui/core/CardMedia";
import CancelIcon from "@material-ui/icons/Cancel";
import GetAppIcon from "@material-ui/icons/GetApp";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import SettingsBackupRestoreIcon from "@material-ui/icons/SettingsBackupRestore";
import ShareIcon from "@material-ui/icons/Share";
import React, { useEffect, useState } from "react";
import SlideInUp from "react-animations/lib/slide-in-up";
import { withRouter } from "react-router-dom";
import styled, { keyframes } from "styled-components";
import ArrowUp from "../../public/assets/arrow-up-squared.png";
import SelectedImage from "../../public/assets/bed -cladd.jpg";
import Loader from "../../public/assets/loader1.gif";
import { apiActions } from "../../server";
import { useThemeSelection } from "../hooks/theme-hook";
import "../styles/search-page.css";
import { filterCategories } from "../util/filters";
import history from "../util/history-util";

const _ = require("lodash");
const SlideInUpAnimation = keyframes`${SlideInUp}`;

const SlideInUpDiv = styled.div`
  animation: 0.2s ${SlideInUpAnimation};
`;

const SpacePageFirst = () => {
  const { currentThemeSelection } = useThemeSelection();
  const [selectedFilters, setSelectedFilters] = useState([]);
  const [selectedFiltersObject, setSelectedFiltersObject] = useState({
    structure: [],
    species: [],
    finish_type: [],
    design_category: [],
    product_names: [],
    surfaces: [],
    colour: [],
  });
  const [selectedFilterCategory, setSelectedFilterCategory] = useState("");
  const [showViewDetailsModal, setShowViewDetailsModal] = useState(false);
  const [showZoomInModal, setShowZoomInModal] = useState(false);
  const [fullImage, setFullImage] = useState(false);
  const [showSignUpModal, setShowSignUpModal] = useState(false);
  const [selectedDesignID, setSelectedDesginID] = useState(-1);
  const [showWishlistModal, setShowWishlistModal] = useState(false);
  const [showAuthModal, setShowAuthModal] = useState(false);
  const [selectedAuth, setSelectedAuth] = useState("login");
  const [scenesArr, setScenesArr] = useState([]);
  const [pageCount, setPageCount] = useState(1);
  const [loadingScenes, setLoadingScenes] = useState(false);
  const [loadingDesigns, setLoadingDesigns] = useState(false);
  const [fetchedAllInspirations, setFetchedAllInspirations] = useState(false);
  const [loginFields, handleLoginFieldChange] = useState({
    email: "",
    password: "",
  });

  const [signupFields, handleSignupFieldChange] = useState({
    username: "",
    fname: "",
    lname: "",
    email: "",
    password: "",
    repassword: "",
  });
  const [selectedCatIdx, setSelectedCatIdx] = useState(-1);

  const [productSubCategory, setProductSubCategory] = useState("");
  const [totalScenes, setTotalScenes] = useState(0);
  const [isVisibleTop, setIsVisibleTop] = useState(false);

  const toggleVisibility = () => {
    if (window.pageYOffset > 300) {
      setIsVisibleTop(true);
    } else {
      setIsVisibleTop(false);
    }
  };

  // Set the top cordinate to 0
  // make scrolling smooth
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  useEffect(() => {
    window.addEventListener("scroll", toggleVisibility);
  }, []);

  let cachedFilterCategories = filterCategories;
  const filterCategoriesKeys = _.keys(cachedFilterCategories);

  useEffect(() => {
    // clear off the second filter in selected-space
    const selectedSpace = JSON.parse(localStorage.getItem("selected-space"));
    const updatedSelectedSpace = selectedSpace.slice(0, 1);
    localStorage.setItem(
      "selected-space",
      JSON.stringify(updatedSelectedSpace)
    );
  }, []);

  const getList = () => {
    let filterCategoryOptions = cachedFilterCategories[selectedFilterCategory];
    let name = selectedFilterCategory;
    // alert(selectedFilterCategory);
    if (selectedCatIdx != -1) {
      filterCategoryOptions = cachedFilterCategories["product_names"];
      let filteredOptions = filterCategoryOptions[selectedCatIdx];
      filterCategoryOptions = Object.values(filteredOptions);
      filterCategoryOptions = filterCategoryOptions[0];
      name = "Product > " + productSubCategory;
    }

    return (
      <div className="category-dropdown-wrapper">
        <SlideInUpDiv>
          <CancelIcon
            className="close-dropdown"
            onClick={() => {
              setSelectedFilterCategory("");
              setProductSubCategory("");
            }}
          />
          <div className="category-dropdown">
            <div
              className="category-name"
              onClick={() => {
                setSelectedCatIdx(-1);
                // setProductSubCategory("");
              }}
            >
              {cleanCategory(name)}
            </div>
            <div className="category-options">
              {filterCategoryOptions &&
                filterCategoryOptions.map((option) => (
                  <div
                    className="category-option"
                    onClick={() => {
                      if (selectedCatIdx == -1) {
                        const filterObj = {};
                        filterObj[selectedFilterCategory] = option;
                        if (
                          !selectedFilters.some((filter) =>
                            _.isEqual(filter, filterObj)
                          )
                        ) {
                          let copySelectedFilters = Object.assign(
                            [],
                            selectedFilters
                          );
                          copySelectedFilters.push(filterObj);
                          setSelectedFilters(copySelectedFilters);

                          // update filters object as well
                          let copySelectedFiltersObject = Object.assign(
                            {},
                            selectedFiltersObject
                          );
                          copySelectedFiltersObject[
                            selectedFilterCategory
                          ].push(option);
                          setSelectedFiltersObject(copySelectedFiltersObject);
                        } else {
                          alert("Filter is already selected.");
                        }
                        setSelectedFilterCategory("");
                      } else {
                        const filterObj = {};
                        filterObj[selectedFilterCategory] = option;
                        if (
                          !selectedFilters.some((filter) =>
                            _.isEqual(filter, filterObj)
                          )
                        ) {
                          let copySelectedFilters = Object.assign(
                            [],
                            selectedFilters
                          );
                          copySelectedFilters.push(filterObj);
                          setSelectedFilters(copySelectedFilters);

                          // update filters object as well
                          let copySelectedFiltersObject = Object.assign(
                            {},
                            selectedFiltersObject
                          );
                          copySelectedFiltersObject[
                            selectedFilterCategory
                          ].push(option);
                          setSelectedFiltersObject(copySelectedFiltersObject);
                        } else {
                          alert("Filter is already selected.");
                        }

                        setSelectedFilterCategory("");
                        setSelectedFilterCategory("");
                      }
                    }}
                  >
                    {option}
                  </div>
                ))}
            </div>
            <div style={{ height: "15vh" }} />
          </div>
        </SlideInUpDiv>
      </div>
    );
  };

  const getPrimaryListProduct = () => {
    const filterCategoryOptions = cachedFilterCategories["product_names"];
    const filteredOptions = filterCategoryOptions.map((obj) =>
      Object.keys(obj)
    );

    return (
      <div className="category-dropdown-wrapper">
        <SlideInUpDiv>
          <CancelIcon
            className="close-dropdown"
            onClick={() => {
              setSelectedFilterCategory("");
              setSelectedCatIdx(-1);
            }}
          />
          <div className="category-dropdown">
            <div className="category-name">
              {cleanCategory(selectedFilterCategory)}
            </div>
            <div className="category-options">
              {filteredOptions &&
                filteredOptions.map((option, idx) => (
                  <div
                    className="category-option"
                    onClick={() => {
                      setSelectedCatIdx(idx);
                      setSelectedFilterCategory("product_names");
                      setProductSubCategory(option[0]);
                    }}
                  >
                    {option}
                    <span className="arrow-sign">{" >"}</span>
                  </div>
                ))}
            </div>
            <div style={{ height: "15vh" }} />
          </div>
        </SlideInUpDiv>
      </div>
    );
  };

  const getWishlistModal = () => {
    return (
      <div className="sign-up-modal-wrapper">
        <CancelIcon
          className="close-modal-sign-up"
          onClick={() => setShowWishlistModal(false)}
        />
        <center>
          <div className="sign-up-modal">
            <h6 style={{ font: "normal normal bold 18px/28px Encode Sans" }}>
              <b>WISHLIST</b>
            </h6>
            <br />
            <center>
              <h5
                style={{
                  backgroundColor: "#656565",
                  borderRadius: "40px",
                  color: "white",
                  font: "normal normal bold 15px/18px Encode Sans",
                  marginTop: "1vh",
                  paddingTop: "0.5vh",
                  paddingBottom: "0.5vh",
                  width: "20vw",
                  mouse: "cursor",
                }}
              />
            </center>
          </div>
        </center>
      </div>
    );
  };

  const signupHandler = () => {
    const data = {
      email: signupFields.email,
      password: signupFields.password,
      firstname: signupFields.fname,
      lastname: signupFields.lname,
      username: signupFields.username,
    };

    apiActions.signup(data);
    setShowAuthModal(false);
  };

  const getAuthModal = () => {
    return (
      <div className="auth-modal-wrapper">
        <CancelIcon
          className="close-modal-auth"
          onClick={() => setShowAuthModal(false)}
        />
        <center>
          <div className="auth-modal">
            <div className="auth-modal-header">
              <div className="row">
                <div
                  className={
                    selectedAuth == "login"
                      ? "col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 selected-auth"
                      : "col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 "
                  }
                  style={{
                    paddingBottom: "1vh",
                    paddingTop: "1vh",
                    cursor: "pointer",
                  }}
                  onClick={() =>
                    selectedAuth == "signup" ? setSelectedAuth("login") : null
                  }
                >
                  Login
                </div>

                <div
                  className={
                    selectedAuth == "signup"
                      ? "col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 selected-auth"
                      : "col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 "
                  }
                  style={{
                    paddingBottom: "1vh",
                    paddingTop: "1vh",
                    cursor: "pointer",
                  }}
                  onClick={() =>
                    selectedAuth == "login" ? setSelectedAuth("signup") : null
                  }
                >
                  Signup
                </div>
              </div>
            </div>

            <div className="auth-modal-body">
              {selectedAuth == "login" ? (
                <>
                  <div class="container">
                    <input
                      type="text"
                      placeholder="Email Address"
                      value={loginFields.email}
                      onChange={(e) => {
                        const email = e.target.value;
                        handleLoginFieldChange({ email });
                      }}
                      name="uname"
                      required
                    />

                    <input
                      type="password"
                      value={loginFields.password}
                      onChange={(e) => {
                        const password = e.target.value;
                        handleLoginFieldChange({ password });
                      }}
                      placeholder="Password"
                      name="psw"
                      required
                    />

                    <button type="submit">Login</button>
                    <label>
                      <input
                        type="checkbox"
                        checked="checked"
                        name="remember"
                      />{" "}
                      Remember me
                    </label>
                  </div>
                </>
              ) : (
                <>
                  {" "}
                  <input
                    type="text"
                    placeholder="Username*"
                    name="uname"
                    required
                    value={signupFields.username}
                    onChange={(e) => {
                      const username = e.target.value;
                      handleSignupFieldChange({ ...signupFields, username });
                    }}
                  />
                  <input
                    type="text"
                    placeholder="First Name*"
                    name="uname"
                    required
                    value={signupFields.fname}
                    onChange={(e) => {
                      const fname = e.target.value;
                      handleSignupFieldChange({ ...signupFields, fname });
                    }}
                  />
                  <input
                    type="text"
                    placeholder="Last Name*"
                    name="uname"
                    required
                    value={signupFields.lname}
                    onChange={(e) => {
                      const lname = e.target.value;
                      handleSignupFieldChange({ ...signupFields, lname });
                    }}
                  />
                  <input
                    type="text"
                    placeholder="Email*"
                    name="uname"
                    required
                    value={signupFields.email}
                    onChange={(e) => {
                      const email = e.target.value;
                      handleSignupFieldChange({ ...signupFields, email });
                    }}
                  />
                  <input
                    type="password"
                    placeholder="Password*"
                    name="psw"
                    required
                    value={signupFields.password}
                    onChange={(e) => {
                      const password = e.target.value;
                      handleSignupFieldChange({ ...signupFields, password });
                    }}
                  />
                  <input
                    type="password"
                    placeholder="Confirm Password*"
                    name="psw"
                    required
                    value={signupFields.repassword}
                    onChange={(e) => {
                      const repassword = e.target.value;
                      handleSignupFieldChange({ ...signupFields, repassword });
                    }}
                  />
                  <label>
                    <input type="checkbox" checked="checked" name="remember" />{" "}
                    I agree with terms of use.
                  </label>
                  <button type="submit" onClick={() => signupHandler()}>
                    Create Account
                  </button>
                </>
              )}
            </div>
          </div>
        </center>
      </div>
    );
  };

  const getSignInPopUp = () => {
    return (
      <div className="sign-up-modal-wrapper">
        <CancelIcon
          className="close-modal-sign-up"
          onClick={() => setShowSignUpModal(false)}
        />
        <center>
          <div className="sign-up-modal">
            <h6 style={{ font: "normal normal bold 18px/28px Encode Sans" }}>
              <b>
                You’ve liked this design! Sign up now to add it to your wish
                list so that you can view your favorites later.
              </b>
            </h6>
            <br />
            <center>
              <h5
                style={{
                  backgroundColor: "#656565",
                  borderRadius: "40px",
                  color: "white",
                  font: "normal normal bold 15px/18px Encode Sans",
                  marginTop: "1vh",
                  paddingTop: "0.5vh",
                  paddingBottom: "0.5vh",
                  width: "20vw",
                }}
                onClick={() => {
                  setShowAuthModal(true);
                  setShowSignUpModal(false);
                }}
              >
                SIGN UP
              </h5>
            </center>
          </div>
        </center>
      </div>
    );
  };

  const getShowZoomInModal = () => {
    return (
      <div className="zoom-in-modal-wrapper">
        <CancelIcon
          className="close-dropdown-zoom-in"
          onClick={() => setShowZoomInModal(false)}
        />
        <div className="zoom-in-modal">
          <img
            className="d-block w-100"
            src={SelectedImage}
            alt="First slide"
            style={{ height: "75vh" }}
          />
        </div>

        <SettingsBackupRestoreIcon
          className="refresh-icon"
          onClick={() => setShowZoomInModal(false)}
        />

        <GetAppIcon
          className="download-icon"
          onClick={() => setShowZoomInModal(false)}
        />

        <ShareIcon
          className="share-icon"
          onClick={() => setShowZoomInModal(false)}
        />
      </div>
    );
  };
  const getViewDetailsModal = () => {
    return (
      <div className="view-details-dropdown-wrapper">
        <SlideInUpDiv>
          <CancelIcon
            className="close-dropdown"
            onClick={() => setShowViewDetailsModal(false)}
          />
          <div className="view-details-dropdown">
            <div className="design-image-wrapper">
              <div className="design-image">
                <center>
                  <h5>
                    <b>Desgin 10086 beach</b>
                  </h5>
                </center>

                {fullImage ? (
                  <div style={{ backgroundColor: "grey", height: "500px" }} />
                ) : (
                  <div style={{ backgroundColor: "grey", height: "234px" }} />
                )}

                <center>
                  <h5
                    style={{
                      backgroundColor: "#656565",
                      borderRadius: "40px",
                      color: "white",
                      font: "normal normal bold 15px/18px Encode Sans",
                      marginTop: "1vh",
                      paddingTop: "0.5vh",
                      paddingBottom: "0.5vh",
                      cursor: "pointer",
                    }}
                    onClick={() => setFullImage(!fullImage)}
                  >
                    {!fullImage ? <>VIEW FULL SHEET</> : <>VIEW HALF IMAGE</>}
                  </h5>
                </center>
              </div>
            </div>

            <div className="subcategory-dropdown">
              <h6>
                <b>SELECT SUB CATEGORY</b>
              </h6>

              <div className="desgin-dropdown-table">
                <div className="subcategory-title">Merinolam</div>

                <div className="subcategory-table">
                  <table class="table table-bordered">
                    <tbody>
                      <tr>
                        <td>Finishes</td>
                        <td>Suede</td>
                      </tr>
                      <tr>
                        <td>Species</td>
                        <td>Beech</td>
                      </tr>
                      <tr>
                        <td>Structure</td>
                        <td>Full Crown</td>
                      </tr>
                      <tr>
                        <td>Standard Size (mm)</td>
                        <td>1220 * 2440</td>
                      </tr>
                      <tr>
                        <td>Standard Size (ft)</td>
                        <td>4 * 8</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

            <div className="view-details-footer">
              <center>
                <h6>
                  <b>For minimum order quantity, please enquire.</b>
                </h6>
              </center>
              <br />
              <br />
              <center>
                <h5
                  style={{
                    backgroundColor: "#656565",
                    borderRadius: "40px",
                    color: "white",
                    font: "normal normal bold 15px/18px Encode Sans",
                    marginTop: "1vh",
                    paddingTop: "0.5vh",
                    paddingBottom: "0.5vh",
                  }}
                >
                  FINISHES AND SIZE AVAILABILITY +
                </h5>
              </center>
            </div>
            <div style={{ height: "10vh" }} />
          </div>
        </SlideInUpDiv>
      </div>
    );
  };

  const deleteFilterAtIndex = (index) => {
    let copySelectedFilters = Object.assign([], selectedFilters);
    let copySelectedFiltersObject = Object.assign({}, selectedFiltersObject);
    if (index >= 0 && copySelectedFilters.length) {
      const filterObj = copySelectedFilters[index];
      const filterKey = Object.keys(filterObj)[0];
      const filterValue = Object.values(filterObj)[0];
      const arr = copySelectedFiltersObject[filterKey];
      const idx = arr.indexOf(filterValue);

      copySelectedFiltersObject[filterKey].splice(idx, 1);
      copySelectedFilters.splice(index, 1);

      setSelectedFilters(copySelectedFilters);
      setSelectedFiltersObject(copySelectedFiltersObject);
    }
  };

  const getDataArray = (data) => {
    return Object.keys(data).map(function(key, index) {
      return data[key];
    });
  };

  const getInspirationsFromLocalStorage = () => {
    // get scenes from local localStorage
    const scenes = localStorage.getItem("scenes");
    let scenesJson;
    let scenesArr;
    if (scenes) {
      scenesJson = JSON.parse(scenes);
      scenesArr = getDataArray(scenesJson);
    } else scenesArr = [];
    return scenesArr;
  };

  const appendInspirationData = (data) => {
    const scenesArr1 = getInspirationsFromLocalStorage();
    const updatedscenesArr = Object.assign([], scenesArr1);
    updatedscenesArr.push(...data);
    setScenesArr(updatedscenesArr);
    localStorage.setItem("scenes", JSON.stringify(updatedscenesArr));
  };

  const loadDataForPage = (pageNumber) => {
    setLoadingScenes(true);
    const selectedSpace = JSON.parse(localStorage.getItem("selected-space"))[0];
    setSelectedFilters([{ default: selectedSpace }]);
    // get list for scenes for selected room_name
    const roomName = selectedSpace;
    apiActions.get_scenes(
      { room_name: roomName, _page_number: pageNumber },
      (res) => {
        const data = res.data;
        setTotalScenes(res.total_number);
        const dataArr = getDataArray(data);
        appendInspirationData(dataArr);
        // in case array has less than 50 entries
        if (dataArr.length < 50) {
          localStorage.setItem("fetchedAllInspirations", true);
          setFetchedAllInspirations(true);
          // remove scroll listener
        } else {
          // add scroll listener
          // alert("more data to come");
          document.addEventListener("scroll", trackScrolling);
        }

        setLoadingScenes(false);
      },
      () => {
        // alert("Failed to fetch scenes.");
        setLoadingScenes(false);
      }
    );
  };

  const isBottom = (el) => {
    if (el) return el.getBoundingClientRect().bottom <= window.innerHeight;
    return false;
  };

  const trackScrolling = () => {
    const wrappedElement = document.getElementById("scenes");
    if (isBottom(wrappedElement)) {
      // increment page no and pull & append data for that pageNumber
      if (!localStorage.getItem("fetchedAllInspirations")) {
        const pageNumber = localStorage.getItem("pageNumber");
        loadDataForPage(parseInt(pageNumber) + 1);
        localStorage.setItem("pageNumber", parseInt(pageNumber) + 1);
      }
      document.removeEventListener("scroll", trackScrolling);
    }
  };

  useEffect(() => {
    // add a listner for detecting scrolling
    document.addEventListener("scroll", trackScrolling);
    const scenes = localStorage.getItem("scenes");
    const fetchedAllInspirations = localStorage.getItem(
      "fetchedAllInspirations"
    );
    if (fetchedAllInspirations) setFetchedAllInspirations(true);
    if (0) {
      const scenesArr = getInspirationsFromLocalStorage();
      setScenesArr(scenesArr);
    } else {
      localStorage.setItem("pageNumberScenes", 1);
      localStorage.setItem("scenes", []);
      localStorage.removeItem("fetchedAllScenes");
      setScenesArr([]);
      loadDataForPage(1);
    }

    // get all the designs
    const designs = localStorage.getItem("designs");
    if (!designs) {
      setLoadingDesigns(true);
      apiActions.get_designs({}, () => setLoadingDesigns(false));
    }
  }, []);

  const [index, setIndex] = useState(0);

  const cleanCategory = (category) => {
    const idx = category.indexOf("_");
    let str = category;
    if (idx > -1) {
      str = category.slice(0, idx);
    }

    let uCaseStr = str.toUpperCase();
    if (uCaseStr == "PRODUCT") uCaseStr = "PRODUCTS";
    else if (uCaseStr == "DESIGN") uCaseStr = "DESIGNS";
    else if (uCaseStr == "FINISH") uCaseStr = "FINISH TYPES";
    else if (uCaseStr == "STRUCTURE") uCaseStr = "STRUCTURES";
    else if (uCaseStr == "COLOUR") uCaseStr = "COLORS";

    return uCaseStr;
  };

  return (
    <div
      style={{
        backgroundColor: `#${
          currentThemeSelection === "lite" ? "white" : "3c3599"
        }`,
        height: "auto",
        marginTop: "12vh",
      }}
      className="search-page"
    >
      <div
        class="scrollmenu-filters"
        style={{
          paddingLeft: "0.5rem",
          paddingTop: ".5rem",
        }}
      >
        {/* filter bar */}
        {selectedFilters.length ? (
          selectedFilters.map((filter, index) => (
            <div className="filter-bar">
              {Object.values(filter)[0]}
              {Object.keys(filter)[0] != "default" && (
                <HighlightOffIcon
                  className="remove-filter-icon"
                  onClick={() => deleteFilterAtIndex(index)}
                />
              )}
            </div>
          ))
        ) : (
          <h6>
            <b
              style={{
                font: "normal normal bold 14px/16px Encode Sans",
                letterSpacing: " -0.29px",
                color: "#585858",
              }}
            >
              Apply the above filters to sort out based on your needs
            </b>
            .
          </h6>
        )}
      </div>
      {!loadingScenes && (
        <div className="all-photos-text">All photos ({totalScenes})</div>
      )}

      <div className="row" id="inspirations">
        {!loadingScenes && !scenesArr.length && (
          <div
            style={{ textAlign: "center", width: "100vw", marginTop: "10vh" }}
          >
            <center>
              <h6>No results found</h6>
            </center>
          </div>
        )}

        {scenesArr.map((insp) => (
          <div
            className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
            style={{ marginTop: "1vh" }}
          >
            <Card
              className="card-style"
              onClick={() => {
                const spaceFlow = JSON.parse(
                  localStorage.getItem("selected-space")
                );
                spaceFlow.push(insp.scene_name);
                localStorage.setItem(
                  "selected-space",
                  JSON.stringify(spaceFlow)
                );

                localStorage.setItem(
                  "selected-surfaces",
                  JSON.stringify(insp.surfaces)
                );
                localStorage.setItem(
                  "selected-scene-id-space",
                  JSON.stringify(insp.scene_id)
                );
                history.push("space-second");
              }}
            >
              <CardMedia
                image={insp.rendering_thumbnail}
                style={{
                  height: 0,
                  paddingTop: "56.25%", // 16:9
                }}
              />

              <CardActions
                style={{
                  justifyContent: "center",
                  marginTop: "-6vh",
                  paddingBottom: "0px",
                  paddingLeft: "0px",
                  paddingRight: "0px",
                }}
              >
                <Button
                  style={{
                    color: "white",
                    width: "100%",
                    background: "rgba(0,0,0,0.4)",
                    paddingTop: "0px",
                    paddingBottom: "0px",
                    borderRadius: "0px",
                    fontSize: "12px",
                    font: "normal normal medium 20px/24px Encode Sans",
                  }}
                  size="big"
                >
                  <b
                    style={{
                      fontFamily: "Encode Sans",
                      fontSize: "0.8rem",
                      letterSpacing: "1.2px",
                    }}
                  >
                    {" "}
                    {insp.scene_name}
                  </b>
                </Button>
              </CardActions>
            </Card>
          </div>
        ))}
      </div>
      <div style={{ height: "5vh" }} />
      <div>
        <center>
          {loadingScenes || loadingDesigns ? (
            <img
              src={Loader}
              style={{
                height: "10vh",
                width: "auto",
                zIndex: 5,
              }}
            />
          ) : null}
        </center>
      </div>

      <div style={{ height: "10vh" }} />

      {selectedFilterCategory == "product_names" && getPrimaryListProduct()}
      {selectedFilterCategory != "" &&
        selectedFilterCategory != "product_names" &&
        getList()}

      {selectedFilterCategory != "" && selectedCatIdx != -1 && getList()}
      {showViewDetailsModal != "" && getViewDetailsModal()}
      {showZoomInModal != "" && getShowZoomInModal()}
      {showSignUpModal != "" && getSignInPopUp()}
      {showWishlistModal != "" && getWishlistModal()}
      {showAuthModal != "" && getAuthModal()}
      {isVisibleTop && (
        <div
          style={{
            height: 35,
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            width: 35,
            position: "fixed",
            bottom: 80,
            right: 20,
            borderRadius: 100,
            overflow: "hidden",
            backgroundColor: "rgb(245, 245, 245)",
            zIndex: 5,
          }}
          onClick={() => {
            scrollToTop();
          }}
        >
          <img
            src={ArrowUp}
            style={{
              height: 25,
              width: 25,
              zIndex: 5,
            }}
          />
        </div>
      )}
    </div>
  );
};

export default withRouter(SpacePageFirst);
