import React, { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardMedia from "@material-ui/core/CardMedia";
import { withRouter } from "react-router-dom";
import { apiActions } from "../../../server";
import { useThemeSelection } from "../../hooks/theme-hook";
import "../../styles/home-page.css";
import history from "../../util/history-util";
import ArrowUp from '../../../public/assets/arrow-up-squared.png'

const ProductCategories = (props) => {
  const { currentThemeSelection } = useThemeSelection();
  const [ProductCategoryList, setProductCategoryList] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const [isVisibleTop, setIsVisibleTop] = useState(false);

  const toggleVisibility = () => {
    if (window.pageYOffset > 300) {
      setIsVisibleTop(true);
    } else {
      setIsVisibleTop(false);
    }
  };

  // Set the top cordinate to 0
  // make scrolling smooth
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    });
  };

  useEffect(() => {
    window.addEventListener("scroll", toggleVisibility);
  }, []);

  useEffect(() => {
    const data = props.location.state;
    console.log("data===>>>", data);
    if (data) {
      setProductCategoryList(data.product_categories_list);
      setIsLoading(false);
    }
  }, [props]);

  useEffect(() => {
    // clear off the second filter in selected-space
    const selectedSpace = JSON.parse(localStorage.getItem("selected-space"));
    const updatedSelectedSpace = selectedSpace.slice(0, 1);
    console.log("test", updatedSelectedSpace);
    localStorage.setItem(
      "selected-space",
      JSON.stringify(updatedSelectedSpace)
    );
  }, []);

  return (
    <div
      style={{
        backgroundColor: `#${
          currentThemeSelection === "lite" ? "white" : "3c3599"
        }`,
        height: "auto",
        marginTop: "13vh",
        padding: "0.8rem",
      }}
      className="home-page-categories"
    >
      <div className="explore-now">
        <center>
          <h5
            className="text-uppercase"
            style={{
              letterSpacing: "1px",
              color: " #585858",
              fontWeight: "bold",
              fontSize: "19px",
            }}
          >
            {props.location.state.product_title}
          </h5>
        </center>
        {!isLoading &&
          (ProductCategoryList.length === 0 ? (
            <div
              style={{ textAlign: "center", width: "100vw", marginTop: "10vh" }}
            >
              <center>
                <h6>No results found</h6>
              </center>
            </div>
          ) : (
            <div className="row">
              {ProductCategoryList.map((item, index) => {
                return (
                  <div
                    key={index}
                    className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
                    style={{ marginTop: "2vh" }}
                  >
                    <Card
                      className="card-style"
                      className="card-style"
                      onClick={() => {
                        const spaceFlow = JSON.parse(
                          localStorage.getItem("selected-space")
                        );
                        spaceFlow.push(item.product_name);
                        localStorage.setItem(
                          "selected-space",
                          JSON.stringify(spaceFlow)
                        );
                        history.push({
                          pathname: "/product-category-list",
                          state: {
                            product_names: item.product_name,
                            product_categories: item.product_category,
                            product_name: item.product_name,
                          },
                        });
                      }}
                    >
                      <CardMedia
                        image={item.application_image}
                        style={{
                          height: 0,
                          paddingTop: "56.25%", // 16:9
                        }}
                      />
                      <CardActions
                        style={{
                          justifyContent: "center",
                          marginTop: "-6vh",
                          paddingBottom: "0px",
                          paddingLeft: "0px",
                          paddingRight: "0px",
                        }}
                      >
                        <Button
                          style={{
                            color: "white",
                            width: "100%",
                            background: "rgba(0,0,0,0.4)",
                            paddingTop: "0px",
                            paddingBottom: "0px",
                            borderRadius: "0px",
                            fontSize: "12px",
                            font: "normal normal medium 16px/20px Encode Sans",
                          }}
                          size="big"
                        >
                          <b
                            className="text-uppercase"
                            style={{
                              fontFamily: "Encode Sans",
                              fontSize: "0.8rem",
                              fontWeight: "500",
                              textTransform: "none",
                              // letterSpacing: "1.2px",
                            }}
                          >
                            {" "}
                            {item.product_name}
                          </b>
                        </Button>
                      </CardActions>
                    </Card>
                  </div>
                );
              })}
            </div>
          ))}
      </div>
      <div style={{ height: "10vh" }} />
      {isVisibleTop &&
        <div style={{
          height: 35,
          justifyContent: 'center',
          alignItems: 'center',
          display: 'flex',
          width: 35, position: 'fixed', bottom: 80, right: 20, borderRadius: 100, overflow: 'hidden', backgroundColor: 'rgb(245, 245, 245)', zIndex: 5,
        }} onClick={() => { scrollToTop() }}>
          <img
            src={ArrowUp}
            style={{
              height: 25,
              width: 25,
              zIndex: 5,
            }}
          />
        </div>
      }
    </div>
  );
};

export default withRouter(ProductCategories);
