import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardMedia from "@material-ui/core/CardMedia";
import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import ArrowUp from "../../../public/assets/arrow-up-squared.png";
import Compact from "../../../public/assets/product-category-compat.jpg";
import Laminates from "../../../public/assets/product-category-laminates.jpg";
import Panels from "../../../public/assets/product-category-panels.jpg";
import PerformanceLaminates from "../../../public/assets/product-category-performance-laminates.jpg";
import SpeciaLaminates from "../../../public/assets/product-category-specia-laminates.jpg";
import SolidSurfaces from "../../../public/assets/product-category-solid-surfaces.jpg";
import { apiActions } from "../../../server";
import { useThemeSelection } from "../../hooks/theme-hook";
import "../../styles/home-page.css";
import history from "../../util/history-util";

const ProductCategory = () => {
  const { currentThemeSelection } = useThemeSelection();
  const ProductCategoryList = [
    {
      id: 1,
      product_categories: "Laminates",
      product_thumbnail: Laminates,
    },
    {
      id: 2,
      product_categories: "Special Laminates",
      product_thumbnail: SpeciaLaminates,
    },
    {
      id: 3,
      product_categories: "Panels",
      product_thumbnail: Panels,
    },
    {
      id: 4,
      product_categories: "Performance Laminates",
      product_thumbnail: PerformanceLaminates,
    },
    {
      id: 5,
      product_categories: "Compact",
      product_thumbnail: Compact,
    },
    {
      id: 6,
      product_categories: "Solid Surfaces",
      product_thumbnail: SolidSurfaces,
    },
  ];
  const [isVisibleTop, setIsVisibleTop] = useState(false);

  const toggleVisibility = () => {
    if (window.pageYOffset > 300) {
      setIsVisibleTop(true);
    } else {
      setIsVisibleTop(false);
    }
  };

  // Set the top cordinate to 0
  // make scrolling smooth
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  useEffect(() => {
    const str = window.location.pathname;
    apiActions.patch_user(
      {
        pwa_flow: str,
        _async: true,
      },

      (res) => console.log(res),
      (err) => console.log(err)
    );

    window.addEventListener("scroll", toggleVisibility);
  }, []);

  useEffect(() => {
    localStorage.setItem("selected-space", JSON.stringify([]));
  }, []);

  useEffect(() => {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: "smooth",
    });
  }, []);

  return (
    <div
      style={{
        backgroundColor: `#${
          currentThemeSelection === "lite" ? "white" : "3c3599"
        }`,
        height: "auto",
        marginTop: "13vh",
        padding: "0.8rem",
      }}
      className="home-page-categories"
    >
      <div className="explore-now">
        <center>
          <h5
            style={{
              letterSpacing: "1px",
              color: " #585858",
              fontWeight: "bold",
              fontSize: "19px",
            }}
          >
            PRODUCTS
          </h5>
        </center>
        <div className="row">
          {ProductCategoryList.map((item, index) => {
            return (
              <div
                key={index}
                className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
                style={{ marginTop: "2vh" }}
              >
                <Card
                  className="card-style"
                  className="card-style"
                  onClick={() => {
                    localStorage.setItem(
                      "selected-product-category",
                      JSON.stringify([item.product_categories])
                    );
                    const data = { product_category: item.product_categories };
                    apiActions.get_products(data, (res) => {
                      if (res.data.length !== 0) {
                        localStorage.setItem(
                          "selected-space",
                          JSON.stringify([item.product_categories])
                        );
                        if (res.total_number === 1) {
                          const spaceFlow = JSON.parse(
                            localStorage.getItem("selected-space")
                          );
                          spaceFlow.push(res.data[0].product_name);
                          localStorage.setItem(
                            "selected-space",
                            JSON.stringify(spaceFlow)
                          );
                          history.push({
                            pathname: "/product-category-list",
                            state: {
                              product_names: res.data[0].product_name,
                              product_categories: item.product_category,
                              product_name: res.data[0].product_name,
                            },
                          });
                        } else {
                          history.push({
                            pathname: "/product-categories",
                            state: {
                              product_categories_list: res.data,
                              product_title: item.product_categories,
                            },
                          });
                        }
                      }
                    });
                  }}
                >
                  <CardMedia
                    image={item.product_thumbnail}
                    style={{
                      height: 0,
                      paddingTop: "56.25%", // 16:9
                    }}
                  />

                  <CardActions
                    style={{
                      justifyContent: "center",
                      marginTop: "-6vh",
                      paddingBottom: "0px",
                      paddingLeft: "0px",
                      paddingRight: "0px",
                    }}
                  >
                    <Button
                      style={{
                        color: "white",
                        width: "100%",
                        background: "rgba(0,0,0,0.4)",
                        paddingTop: "0px",
                        paddingBottom: "0px",
                        borderRadius: "0px",
                        fontSize: "12px",
                        font: "normal normal medium 16px/20px Encode Sans",
                      }}
                      size="big"
                    >
                      <b
                        className="text-uppercase"
                        style={{
                          fontFamily: "Encode Sans",
                          fontSize: "0.8rem",
                          fontWeight: "500",
                          textTransform: "none",
                          // letterSpacing: "1.2px",
                        }}
                      >
                        {" "}
                        {item.product_categories}
                      </b>
                    </Button>
                  </CardActions>
                </Card>
              </div>
            );
          })}
        </div>
      </div>
      <div style={{ height: "10vh" }} />
      {isVisibleTop && (
        <div
          style={{
            height: 35,
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            width: 35,
            position: "fixed",
            bottom: 80,
            right: 20,
            borderRadius: 100,
            overflow: "hidden",
            backgroundColor: "rgb(245, 245, 245)",
            zIndex: 5,
          }}
          onClick={() => {
            scrollToTop();
          }}
        >
          <img
            src={ArrowUp}
            style={{
              height: 25,
              width: 25,
              zIndex: 5,
            }}
          />
        </div>
      )}
    </div>
  );
};

export default withRouter(ProductCategory);
