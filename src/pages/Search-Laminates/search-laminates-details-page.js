import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import CancelIcon from "@material-ui/icons/Cancel";
import GetAppIcon from "@material-ui/icons/GetApp";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import SettingsBackupRestoreIcon from "@material-ui/icons/SettingsBackupRestore";
import ShareIcon from "@material-ui/icons/Share";
import React, { useEffect, useState } from "react";
import SlideInUp from "react-animations/lib/slide-in-up";
import Carousel from "react-bootstrap/Carousel";
import { withRouter } from "react-router-dom";
import { toast } from "react-toastify";
import styled, { keyframes } from "styled-components";
import ViewDetailsIcon from "../../../public/assets/Asset 1.svg";
import ZoomInIcon from "../../../public/assets/Asset 2.svg";
import LikeIcon from "../../../public/assets/Asset 4.svg";
import Loader from "../../../public/assets/loader1.gif";
import LikedIcon from "../../../public/assets/test.jpeg";
import WishlistedIcon from "../../../public/assets/test2.jpeg";
import WishlistIcon from "../../../public/assets/test3.jpeg";
import { apiActions } from "../../../server";
import { useThemeSelection } from "../../hooks/theme-hook";
import "../../styles/design-page.css";
import "../../styles/search-page.css";
import { filterCategories } from "../../util/filters";

const _ = require("lodash");
const SlideInUpAnimation = keyframes`${SlideInUp}`;

const SlideInUpDiv = styled.div`
  animation: 0.2s ${SlideInUpAnimation};
`;

const SearchLaminatesDetails = () => {
  const { currentThemeSelection } = useThemeSelection();
  const [selectedFilters, setSelectedFilters] = useState([]);
  const [selectedFiltersObject, setSelectedFiltersObject] = useState({
    structure: [],
    species: [],
    finish_type: [],
    design_category: [],
    product_names: [],
  });
  const [selectedFilterCategory, setSelectedFilterCategory] = useState("");
  const [showViewDetailsModal, setShowViewDetailsModal] = useState(false);
  const [showZoomInModal, setShowZoomInModal] = useState(false);
  const [fullImage, setFullImage] = useState(false);
  const [showSignUpModal, setShowSignUpModal] = useState(false);
  const [selectedDesignID, setSelectedDesginID] = useState(1);
  const [showWishlistModal, setShowWishlistModal] = useState(false);
  const [showAuthModal, setShowAuthModal] = useState(false);
  const [loadingRenderImages, setLoadingRenderImages] = useState(false);
  const [loadedRenderImages, setLoadedRenderImages] = useState(false);
  const [loadingDesignData, setLoadingDesignData] = useState(false);
  const [loadedDesignData, setLoadedDesignData] = useState(false);

  const [similarDesignData, setSimilarDesignData] = useState([]);
  const [loadingSimilarDesignData, setLoadingSimilarDesignData] = useState(
    false
  );
  const [loadedSimilarDesignData, setLoadedSimilarDesignData] = useState(false);

  const [familyDesignData, setFamilyDesignData] = useState([]);
  const [loadingFamilyDesignData, setLoadingFamilyDesignData] = useState(false);
  const [loadedFamilyDesignData, setLoadedFamilyDesignData] = useState(false);
  const [showDefaultFilters, setShowDefaultFilters] = useState(true);

  const [imagesArr, setImagesArr] = useState([]);
  const [imagesNamesArr, setImagesNamesArr] = useState([]);

  const [renderCount, setRenderCount] = useState(0);
  const [selectedAuth, setSelectedAuth] = useState("signup");
  const [loginFields, handleLoginFieldChange] = useState({
    email: "",
    password: "",
  });

  const [signupFields, handleSignupFieldChange] = useState({
    username: "",
    name: "",
    contact: "",
    email: "",
    password: "",
    repassword: "",
  });

  const [designID, setDesignID] = useState("");
  const [currentDesign, setCurrentDesign] = useState({});
  const [defaultDesign, setDefaultDesign] = useState({});

  const [imgNumber, setImgNumber] = useState(1);

  const [liked, setLiked] = useState(false);
  const [wishlisted, setWishlisted] = useState(false);

  const [selectedCatIdx, setSelectedCatIdx] = useState(-1);

  const [productSubCategory, setProductSubCategory] = useState("");
  let cachedFilterCategories = filterCategories;
  const filterCategoriesKeys = _.keys(cachedFilterCategories);

  const [signingUp, setSigningUp] = useState(false);
  const [loggingIn, setLogginIn] = useState(false);

  const getList = () => {
    let filterCategoryOptions = cachedFilterCategories[selectedFilterCategory];
    let name = selectedFilterCategory;
    if (selectedCatIdx != -1) {
      filterCategoryOptions = cachedFilterCategories["product_names"];
      let filteredOptions = filterCategoryOptions[selectedCatIdx];
      filterCategoryOptions = Object.values(filteredOptions);
      filterCategoryOptions = filterCategoryOptions[0];
      name = "Product > " + productSubCategory;
    }
    return (
      <div className="category-dropdown-wrapper">
        <SlideInUpDiv>
          <CancelIcon
            className="close-dropdown"
            onClick={() => {
              setSelectedFilterCategory("");
              setProductSubCategory("");
            }}
          />
          <div className="category-dropdown">
            <div
              className="category-name"
              onClick={() => {
                setSelectedCatIdx(-1);
                // setProductSubCategory("");
              }}
            >
              {cleanCategory(name)}
            </div>
            <div className="category-options">
              {filterCategoryOptions &&
                filterCategoryOptions.map((option) => (
                  <div
                    className="category-option"
                    onClick={() => {
                      if (selectedCatIdx == -1) {
                        const filterObj = {};
                        filterObj[selectedFilterCategory] = option;
                        if (
                          !selectedFilters.some((filter) =>
                            _.isEqual(filter, filterObj)
                          )
                        ) {
                          let copySelectedFilters = Object.assign(
                            [],
                            selectedFilters
                          );
                          copySelectedFilters.push(filterObj);
                          setSelectedFilters(copySelectedFilters);
                          setShowDefaultFilters(false);
                          // update filters object as well
                          let copySelectedFiltersObject = Object.assign(
                            {},
                            selectedFiltersObject
                          );
                          copySelectedFiltersObject[
                            selectedFilterCategory
                          ].push(option);
                          setSelectedFiltersObject(copySelectedFiltersObject);
                        } else {
                          alert("Filter is already selected.");
                        }
                        setSelectedFilterCategory("");
                      } else {
                        const filterObj = {};
                        filterObj[selectedFilterCategory] = option;
                        if (
                          !selectedFilters.some((filter) =>
                            _.isEqual(filter, filterObj)
                          )
                        ) {
                          let copySelectedFilters = Object.assign(
                            [],
                            selectedFilters
                          );
                          copySelectedFilters.push(filterObj);
                          setSelectedFilters(copySelectedFilters);
                          setShowDefaultFilters(false);
                          // update filters object as well
                          let copySelectedFiltersObject = Object.assign(
                            {},
                            selectedFiltersObject
                          );
                          copySelectedFiltersObject[
                            selectedFilterCategory
                          ].push(option);
                          setSelectedFiltersObject(copySelectedFiltersObject);
                        } else {
                          alert("Filter is already selected.");
                        }

                        setSelectedFilterCategory("");
                        setSelectedFilterCategory("");
                      }
                    }}
                  >
                    {option}
                  </div>
                ))}
            </div>
            <div style={{ height: "15vh" }} />
          </div>
        </SlideInUpDiv>
      </div>
    );
  };

  const getPrimaryListProduct = () => {
    const filterCategoryOptions = cachedFilterCategories["product_names"];
    const filteredOptions = filterCategoryOptions.map((obj) =>
      Object.keys(obj)
    );

    return (
      <div className="category-dropdown-wrapper">
        <SlideInUpDiv>
          <CancelIcon
            className="close-dropdown"
            onClick={() => {
              setSelectedFilterCategory("");
              setSelectedCatIdx(-1);
            }}
          />
          <div className="category-dropdown">
            <div className="category-name">
              {cleanCategory(selectedFilterCategory)}
            </div>
            <div className="category-options">
              {filteredOptions &&
                filteredOptions.map((option, idx) => (
                  <div
                    className="category-option"
                    onClick={() => {
                      setSelectedCatIdx(idx);
                      // alert(option);
                      setSelectedFilterCategory("product_names");
                      setProductSubCategory(option[0]);
                    }}
                  >
                    {option} <span className="arrow-sign">{" >"}</span>
                  </div>
                ))}
            </div>
            <div style={{ height: "15vh" }} />
          </div>
        </SlideInUpDiv>
      </div>
    );
  };

  const getWishlistModal = () => {
    return (
      <div className="sign-up-modal-wrapper">
        <CancelIcon
          className="close-modal-sign-up"
          onClick={() => setShowWishlistModal(false)}
        />
        <center>
          <div className="sign-up-modal">
            <h6 style={{ font: "normal normal bold 18px/28px Encode Sans" }}>
              <b>WISHLIST</b>
            </h6>
            <br />
            <center>
              <h5
                style={{
                  backgroundColor: "#656565",
                  borderRadius: "40px",
                  color: "white",
                  font: "normal normal bold 15px/18px Encode Sans",
                  marginTop: "1vh",
                  paddingTop: "0.5vh",
                  paddingBottom: "0.5vh",
                  width: "20vw",
                  mouse: "cursor",
                }}
              />
            </center>
          </div>
        </center>
      </div>
    );
  };

  const validSignUp = () => {
    return (
      signupFields.username != "" &&
      signupFields.email != "" &&
      signupFields.name != "" &&
      signupFields.password != "" &&
      signupFields.password == signupFields.repassword
    );
  };

  const validLogIn = () => {
    return loginFields.password != "" && loginFields.email != "";
  };
  const signupHandler = () => {
    setSigningUp(true);
    const data = {
      email: signupFields.email,
      password: signupFields.password,
      name: signupFields.name,
      phone_number: signupFields.contact,
      username: signupFields.username,
    };

    apiActions.patch_user(
      data,
      (res) => {
        setSigningUp(false);
        setSelectedAuth("login");
      },
      (e) => {
        setSigningUp(false);
        alert("Failed to register, please try again");
      }
    );
  };

  const loginHandler = () => {
    setLogginIn(true);
    apiActions.login(
      loginFields.email,
      loginFields.password,
      (res) => {
        setLogginIn(false);
        setShowAuthModal(false);
      },
      (e) => {
        setLogginIn(false);
        alert("Username or password not correct!");
      }
    );
  };

  const getAuthModal = () => {
    return (
      <div className="auth-modal-wrapper">
        <CancelIcon
          className="close-modal-auth"
          onClick={() => {
            setShowAuthModal(false);
          }}
        />
        <center>
          <div className="auth-modal">
            <div className="auth-modal-header">
              <div className="row">
                <div
                  className={
                    selectedAuth == "login"
                      ? "col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 selected-auth"
                      : "col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 "
                  }
                  style={{
                    paddingBottom: "1vh",
                    paddingTop: "1vh",
                    cursor: "pointer",
                  }}
                  onClick={() =>
                    selectedAuth == "signup" ? setSelectedAuth("login") : null
                  }
                >
                  Login
                </div>

                <div
                  className={
                    selectedAuth == "signup"
                      ? "col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 selected-auth"
                      : "col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 "
                  }
                  style={{
                    paddingBottom: "1vh",
                    paddingTop: "1vh",
                    cursor: "pointer",
                  }}
                  onClick={() =>
                    selectedAuth == "login" ? setSelectedAuth("signup") : null
                  }
                >
                  Signup
                </div>
              </div>
            </div>

            <div className="auth-modal-body">
              {selectedAuth == "login" ? (
                <>
                  <div class="container">
                    <input
                      type="text"
                      placeholder="Username"
                      value={loginFields.email}
                      onChange={(e) => {
                        const email = e.target.value;
                        handleLoginFieldChange({ ...loginFields, email });
                      }}
                      required
                    />

                    <input
                      type="password"
                      value={loginFields.password}
                      onChange={(e) => {
                        const password = e.target.value;
                        handleLoginFieldChange({ ...loginFields, password });
                      }}
                      placeholder="Password"
                      name="psw"
                      required
                    />

                    <div
                      onClick={() =>
                        !loggingIn
                          ? validLogIn()
                            ? loginHandler()
                            : alert("Please fill the form correctly")
                          : null
                      }
                      className={
                        loggingIn
                          ? "progress-btn active "
                          : validLogIn()
                          ? "progress-btn"
                          : "disabled progress-btn"
                      }
                      data-progress-style="indefinite"
                    >
                      <div className="btn"> Login</div>
                      <div class="progress" />
                    </div>

                    <label>
                      <input
                        type="checkbox"
                        checked="checked"
                        name="remember"
                      />{" "}
                      Remember me
                    </label>
                  </div>
                </>
              ) : (
                <>
                  {" "}
                  <input
                    type="text"
                    placeholder="Username*"
                    name="uname"
                    required
                    value={signupFields.username}
                    onChange={(e) => {
                      const username = e.target.value;
                      handleSignupFieldChange({ ...signupFields, username });
                    }}
                  />
                  <input
                    type="text"
                    placeholder="Name*"
                    name="uname"
                    required
                    value={signupFields.name}
                    onChange={(e) => {
                      const name = e.target.value;
                      handleSignupFieldChange({ ...signupFields, name });
                    }}
                  />
                  <input
                    type="email"
                    placeholder="Email*"
                    name="uname"
                    required
                    value={signupFields.email}
                    onChange={(e) => {
                      const email = e.target.value;
                      handleSignupFieldChange({ ...signupFields, email });
                    }}
                  />
                  <input
                    type="text"
                    placeholder="Mobile No*"
                    name="uname"
                    required
                    value={signupFields.contact}
                    onChange={(e) => {
                      const contact = e.target.value;
                      handleSignupFieldChange({ ...signupFields, contact });
                    }}
                  />
                  <input
                    type="password"
                    placeholder="Password*"
                    name="psw"
                    required
                    value={signupFields.password}
                    onChange={(e) => {
                      const password = e.target.value;
                      handleSignupFieldChange({ ...signupFields, password });
                    }}
                  />
                  <input
                    type="password"
                    placeholder="Confirm Password*"
                    name="psw"
                    required
                    value={signupFields.repassword}
                    onChange={(e) => {
                      const repassword = e.target.value;
                      handleSignupFieldChange({ ...signupFields, repassword });
                    }}
                  />
                  <label>
                    <input type="checkbox" checked="checked" name="remember" />{" "}
                    I agree with terms of use.
                  </label>
                  <div
                    onClick={() =>
                      !signingUp
                        ? validSignUp()
                          ? signupHandler()
                          : alert("Please fill the form correctly")
                        : null
                    }
                    className={
                      signingUp
                        ? "progress-btn active "
                        : validSignUp()
                        ? "progress-btn"
                        : "disabled progress-btn"
                    }
                    data-progress-style="indefinite"
                  >
                    <div className="btn"> Create Account</div>
                    <div class="progress" />
                  </div>
                </>
              )}
            </div>
          </div>
        </center>
      </div>
    );
  };

  const getSignInPopUp = () => {
    return (
      <div className="sign-up-modal-wrapper">
        <CancelIcon
          className="close-modal-sign-up"
          onClick={() => setShowSignUpModal(false)}
        />
        <center>
          <div className="sign-up-modal">
            <h6 style={{ font: "normal normal bold 18px/28px Encode Sans" }}>
              <b>
                You’ve liked this design! Sign up now to add it to your wish
                list so that you can view your favorites later.
              </b>
            </h6>
            <br />
            <center>
              <h5
                style={{
                  backgroundColor: "#656565",
                  borderRadius: "40px",
                  color: "white",
                  font: "normal normal bold 15px/18px Encode Sans",
                  marginTop: "1vh",
                  paddingTop: "0.5vh",
                  paddingBottom: "0.5vh",
                  width: "20vw",
                }}
                onClick={() => {
                  setShowAuthModal(true);
                  setShowSignUpModal(false);
                }}
              >
                SIGN UP
              </h5>
            </center>
          </div>
        </center>
      </div>
    );
  };

  const getShowZoomInModal = () => {
    return (
      <div className="zoom-in-modal-wrapper">
        <CancelIcon
          className="close-dropdown-zoom-in"
          onClick={() => setShowZoomInModal(false)}
        />
        <div className="zoom-in-modal">
          <img
            className="d-block w-100"
            src={imagesArr[imgNumber - 1].url}
            alt="First slide"
            style={{ height: "75vh" }}
          />
        </div>

        <SettingsBackupRestoreIcon
          className="refresh-icon"
          onClick={() => setShowZoomInModal(false)}
        />

        <GetAppIcon
          className="download-icon"
          onClick={() => setShowZoomInModal(false)}
        />

        <ShareIcon
          className="share-icon"
          onClick={() => setShowZoomInModal(false)}
        />
      </div>
    );
  };
  const getViewDetailsModal = () => {
    const design = JSON.parse(localStorage.getItem("design"));
    const name = "Design " + design.design_id + " " + design.design_name;
    return (
      <div className="view-details-dropdown-wrapper">
        <SlideInUpDiv>
          <CancelIcon
            className="close-dropdown"
            onClick={() => setShowViewDetailsModal(false)}
          />
          <div className="view-details-dropdown">
            <div className="design-image-wrapper">
              <div
                className="design-image"
                style={{ width: fullImage ? "100%" : "50%" }}
              >
                <center>
                  <h5
                    style={{
                      "font-size": "1rem",
                      "margin-bottom": "15px",
                    }}
                  >
                    <b>{name}</b>
                  </h5>
                </center>

                {fullImage ? (
                  <div
                    style={{
                      background: `url(${design.design_image})`,
                      height: "680px",
                      "background-repeat": "no-repeat",
                      "background-size": "contain",
                      "background-position": "center",
                    }}
                  />
                ) : (
                  <div
                    style={{
                      background: `url(${design.image_thumbnail})`,
                      height: "234px",
                    }}
                  />
                )}

                <center>
                  <h5
                    style={{
                      backgroundColor: "#656565",
                      borderRadius: "40px",
                      color: "white",
                      font: "normal normal bold 15px/18px Encode Sans",
                      marginTop: "1vh",
                      paddingTop: "0.5vh",
                      paddingBottom: "0.5vh",
                      cursor: "pointer",
                    }}
                    onClick={() => setFullImage(!fullImage)}
                  >
                    {!fullImage ? <>VIEW FULL SHEET</> : <>VIEW SMALL SHEET</>}
                  </h5>
                </center>
              </div>
            </div>

            <div className="subcategory-dropdown">
              <h6>
                <b>SELECT SUB CATEGORY</b>
              </h6>

              <div className="desgin-dropdown-table">
                <div className="subcategory-title">Merinolam</div>

                <div className="subcategory-table">
                  <table class="table table-bordered">
                    <tbody>
                      <tr>
                        <td>Finishes</td>
                        <td>Suede</td>
                      </tr>
                      <tr>
                        <td>Species</td>
                        <td>Beech</td>
                      </tr>
                      <tr>
                        <td>Structure</td>
                        <td>Full Crown</td>
                      </tr>
                      <tr>
                        <td>Standard Size (mm)</td>
                        <td>1220 * 2440</td>
                      </tr>
                      <tr>
                        <td>Standard Size (ft)</td>
                        <td>4 * 8</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

            <div className="view-details-footer">
              <center>
                <h6>
                  <b>For minimum order quantity, please enquire.</b>
                </h6>
              </center>
              <br />
              <br />
              <center>
                <h5
                  style={{
                    backgroundColor: "#656565",
                    borderRadius: "40px",
                    color: "white",
                    font: "normal normal bold 15px/18px Encode Sans",
                    marginTop: "1vh",
                    paddingTop: "0.5vh",
                    paddingBottom: "0.5vh",
                  }}
                >
                  FINISHES AND SIZE AVAILABILITY +
                </h5>
              </center>
            </div>
            <div style={{ height: "10vh" }} />
          </div>
        </SlideInUpDiv>
      </div>
    );
  };

  const deleteFilterAtIndex = (index) => {
    let copySelectedFilters = Object.assign([], selectedFilters);
    let copySelectedFiltersObject = Object.assign({}, selectedFiltersObject);
    if (index >= 0 && copySelectedFilters.length) {
      const filterObj = copySelectedFilters[index];
      const filterKey = Object.keys(filterObj)[0];
      const filterValue = Object.values(filterObj)[0];
      const arr = copySelectedFiltersObject[filterKey];
      const idx = arr.indexOf(filterValue);

      copySelectedFiltersObject[filterKey].splice(idx, 1);
      copySelectedFilters.splice(index, 1);

      setSelectedFilters(copySelectedFilters);
      setSelectedFiltersObject(copySelectedFiltersObject);
    }
  };

  const wishlistHandler = () => {
    const loggedIn = localStorage.getItem("logged_in");
    const designId = localStorage.getItem("design-id");
    if (!wishlisted) {
      setWishlisted(true);
      if (loggedIn && designId)
        apiActions.create_interaction(
          designId,
          "shortlisted",
          {},
          (res) => {
            setTimeout(() => apiActions.get_wishlist(), 5000);
            toast.success("Design added to wishlist successfully!", {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          },
          () => setWishlisted(false)
        );
      else {
        setShowAuthModal(true);
      }
    } else {
      setWishlisted(false);
      if (loggedIn && designId)
        apiActions.remove_interaction(
          designId,
          "shortlisted",
          {},
          (res) => setTimeout(() => apiActions.get_wishlist(), 5000),
          () => setWishlisted(true)
        );
      else {
        setShowAuthModal(true);
      }
    }
  };

  useEffect(() => {
    if (localStorage.getItem("design")) {
      setCurrentDesign(JSON.parse(localStorage.getItem("design")));
      setDefaultDesign(JSON.parse(localStorage.getItem("design")));
    }
    const designId = localStorage.getItem("design-id");
    if (designId) {
      // setDesignID(designId);
      let wishlist = JSON.parse(localStorage.getItem("wishlist"));
      let likesArr = JSON.parse(localStorage.getItem("likes"));

      let wishlistIdx = wishlist.indexOf(designId);
      let likesIdx = likesArr.indexOf(designId);

      if (wishlistIdx >= 0) {
        setWishlisted(true);
      }

      if (likesIdx >= 0) {
        setLiked(true);
      }

      setLoadingRenderImages(true);
      setLoadingDesignData(true);
      setLoadingFamilyDesignData(true);
      setLoadingSimilarDesignData(true);
      setRenderCount(renderCount + 1);

      // fetch render images
      apiActions.get_inspirations(
        { design_id: localStorage.getItem("design-id") },
        (res) => {
          const imagesArr = res.data.map((insp) => {
            let obj = {
              url: insp.rendering_image,
              name: "Design " + insp.design_id + " - " + insp.design_name,
            };
            return obj;
          });
          const imagesNamesArr = res.data.map(
            (insp) => "Design " + insp.design_id + " - " + insp.design_name
          );
          setImagesArr(imagesArr);
          setLoadingRenderImages(false);
          setLoadedRenderImages(true);
        },
        (error) => {
          alert("api failed to fetch the data");
          setLoadingRenderImages(true);
        }
      );

      // fetch design data
      apiActions.get_design(
        localStorage.getItem("design-id"),
        () => {
          setLoadingDesignData(false);
          setLoadedDesignData(true);

          // if design data is fetched get similiar designs as well
          apiActions.get_similar_designs(
            localStorage.getItem("design"),
            (res) => {
              setLoadedSimilarDesignData(true);
              setLoadingSimilarDesignData(false);
              setSimilarDesignData(res.data);
            },
            () => {
              alert("failed to fetch desgin family data");
              setLoadingFamilyDesignData(false);
            }
          );

          // if design data is fetched, get family of designs as well
          apiActions.get_design_family(
            localStorage.getItem("design"),
            (res) => {
              setLoadedFamilyDesignData(true);
              setLoadingFamilyDesignData(false);
              setFamilyDesignData(res.data);
            },
            () => {
              alert("failed to fetch desgin family data");
              setLoadingFamilyDesignData(false);
            }
          );
        },
        () => {
          alert("failed to fetch desgin data");
          setLoadingDesignData(false);
        }
      );
    }
  }, []);

  useEffect(() => {
    if (renderCount >= 1) {
      setLoadingRenderImages(true);
      setLoadingDesignData(true);
      setLoadingFamilyDesignData(true);
      setLoadingSimilarDesignData(true);
      apiActions.get_designs(
        selectedFiltersObject,
        (res) => {
          const design = res.data[0];
          if (design) {
            localStorage.setItem("design-id", design.design_id);
            localStorage.setItem("design", JSON.stringify(design));
            setDesignID("");
            setCurrentDesign({});
            setCurrentDesign(JSON.parse(localStorage.getItem("design")));
            setDefaultDesign(JSON.parse(localStorage.getItem("design")));

            setLoadingRenderImages(true);
            setLoadingDesignData(true);
            setLoadingFamilyDesignData(true);
            setLoadingSimilarDesignData(true);
            setRenderCount(renderCount + 1);

            // fetch render images
            apiActions.get_inspirations(
              { design_id: localStorage.getItem("design-id") },
              (res) => {
                const imagesArr = res.data.map((insp) => {
                  let obj = {
                    url: insp.rendering_image,
                    name:
                      insp.design_name +
                      "-" +
                      insp.design_id +
                      "-" +
                      insp.room_name,
                  };
                  return obj;
                });
                const imagesNamesArr = res.data.map(
                  (insp) =>
                    "Design " + insp.design_id + " - " + insp.design_name
                );
                setImagesArr(imagesArr);
                // setImagesNamesArr(imagesNamesArr);
                setLoadingRenderImages(false);
                setLoadedRenderImages(true);
              },
              (error) => {
                alert("api failed to fetch the data");
                setLoadingRenderImages(true);
              }
            );

            // fetch design data
            apiActions.get_design(
              localStorage.getItem("design-id"),
              () => {
                setLoadingDesignData(false);
                setLoadedDesignData(true);

                // if design data is fetched get similiar designs as well
                apiActions.get_similar_designs(
                  localStorage.getItem("design"),
                  (res) => {
                    setLoadedSimilarDesignData(true);
                    setLoadingSimilarDesignData(false);
                    setSimilarDesignData(res.data);
                  },
                  () => {
                    alert("failed to fetch desgin family data");
                    setLoadingFamilyDesignData(false);
                  }
                );

                // if design data is fetched, get family of designs as well
                apiActions.get_design_family(
                  localStorage.getItem("design"),
                  (res) => {
                    setLoadedFamilyDesignData(true);
                    setLoadingFamilyDesignData(false);
                    setFamilyDesignData(res.data);
                  },
                  () => {
                    alert("failed to fetch desgin family data");
                    setLoadingFamilyDesignData(false);
                  }
                );
              },
              () => {
                alert("failed to fetch desgin data");
                setLoadingDesignData(false);
              }
            );
          } else {
            setLoadingRenderImages(false);
            setLoadingDesignData(false);
            setLoadingFamilyDesignData(false);
            setLoadingSimilarDesignData(false);
          }
        },
        () => {
          alert("failed to apply the filters");
          setLoadingDesignData(false);
        }
      );
    }
  }, [selectedFiltersObject]);

  useEffect(() => {
    if (localStorage.getItem("design-id") && designID != "") {
      setLoadingRenderImages(true);
      setLoadingDesignData(true);
      setRenderCount(renderCount + 1);

      // fetch render images
      apiActions.get_inspirations(
        { design_id: localStorage.getItem("design-id") },
        (res) => {
          const imagesArr = res.data.map((insp) => {
            let obj = {
              url: insp.rendering_image,
              name:
                insp.design_name +
                "-" +
                insp.design_id +
                "-" +
                insp.room_name +
                "-" +
                insp.finish_type,
            };
            return obj;
          });

          const imagesNamesArr = res.data.map(
            (insp) => "Design " + insp.design_id + " - " + insp.design_name
          );
          setImagesArr(imagesArr);
          // setImagesNamesArr(imagesNamesArr);
          setLoadingRenderImages(false);
          setLoadedRenderImages(true);
        },
        (error) => {
          alert("api failed to fetch the data");
          setLoadingRenderImages(true);
        }
      );

      // fetch design data
      apiActions.get_design(
        localStorage.getItem("design-id"),
        () => {
          setLoadingDesignData(false);
          setLoadedDesignData(true);

          // if design data is fetched, get family of designs as well
          apiActions.get_design_family(
            localStorage.getItem("design"),
            (res) => {
              setLoadedFamilyDesignData(true);
              setLoadingFamilyDesignData(false);
              setFamilyDesignData(res.data);
            },
            () => {
              alert("failed to fetch desgin family data");
              setLoadingFamilyDesignData(false);
            }
          );
        },
        () => {
          alert("failed to fetch desgin data");
          setLoadingDesignData(false);
        }
      );
    }
  }, [designID]);

  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };

  const loader = () => {
    return (
      <div style={{ width: "100vw" }}>
        <center>
          <img
            src={Loader}
            style={{
              height: "10vh",
              width: "auto",
              zIndex: 5,
            }}
          />
        </center>
      </div>
    );
  };

  const switchDesignIDHandler = (design) => {
    localStorage.setItem("design-id", design.design_id);
    localStorage.setItem("design", JSON.stringify(design));

    setWishlisted(false);
    setLiked(false);
    setImgNumber(1);
    setIndex(0);

    const designId = design.design_id;

    let wishlist = JSON.parse(localStorage.getItem("wishlist"));
    let likesArr = JSON.parse(localStorage.getItem("likes"));

    let wishlistIdx = wishlist.indexOf(designId);
    let likesIdx = likesArr.indexOf(designId);

    if (wishlistIdx >= 0) {
      setWishlisted(true);
    }

    if (likesIdx >= 0) {
      setLiked(true);
    }

    setDesignID(design.design_id);
    setCurrentDesign(design);
  };

  const cleanCategory = (category) => {
    const idx = category.indexOf("_");
    let str = category;
    if (idx > -1) {
      str = category.slice(0, idx);
    }

    let uCaseStr = str.toUpperCase();
    if (uCaseStr == "PRODUCT") uCaseStr = "PRODUCTS";
    else if (uCaseStr == "DESIGN") uCaseStr = "DESIGNS";
    else if (uCaseStr == "FINISH") uCaseStr = "FINISH TYPES";
    else if (uCaseStr == "STRUCTURE") uCaseStr = "STRUCTURES";
    else if (uCaseStr == "COLOUR") uCaseStr = "COLORS";

    return uCaseStr;
  };

  const selectedSpaces = JSON.parse(localStorage.getItem("selected-space"));

  return (
    <div
      style={{
        backgroundColor: `#${
          currentThemeSelection === "lite" ? "white" : "3c3599"
        }`,
        height: "auto",
        marginTop: "21vw",
      }}
      className="search-page"
    >
      <div
        className="scrollmenu"
        style={{
          paddingLeft: "0.5rem",
          paddingTop: "1.5rem",
          borderBottom: "0px solid #00000029",
        }}
      >
        <div className="filter-icon-div">
          <svg
            id="filter-filled-tool-symbol"
            xmlns="http://www.w3.org/2000/svg"
            width="21.82"
            height="16.791"
            viewBox="0 0 21.82 16.791"
            className="filter-icon"
          >
            <path
              id="Path_35288"
              data-name="Path 35288"
              d="M12.7,7.934a.917.917,0,0,1,.358.7v7.635c0,.46.727.693,1.158.37L17.005,14.2c.374-.342.58-.511.58-.85V8.637a.924.924,0,0,1,.358-.7L25.958,1.3c.6-.5.138-1.3-.75-1.3H5.431c-.888,0-1.352.8-.75,1.3Z"
              transform="translate(-4.41)"
              fill="#7b7b7b"
            />
          </svg>
        </div>

        {/* </div> */}

        {filterCategoriesKeys &&
          filterCategoriesKeys.map((category) => (
            <div
              // class="col-4 col-sm-4 col-md-3 col-lg-2 col-xl-1"
              onClick={() => {
                setSelectedCatIdx(-1);
                setSelectedFilterCategory(category);
              }}
              className="scrollmenu-div"
              style={{}}
            >
              {cleanCategory(category)}

              <ArrowDropDownIcon />
            </div>
          ))}
      </div>

      <div
        className="scrollmenu-filters"
        style={{
          paddingLeft: "0.5rem",
          paddingTop: ".5rem",
          // borderBottom: "2px solid #00000029",
        }}
      >
        {showDefaultFilters &&
          selectedSpaces.map((filter, index) => (
            <div className="filter-bar">{filter}</div>
          ))}
        {/* filter bar */}
        {selectedFilters.length
          ? selectedFilters.map((filter, index) => (
              <div>
                {Object.values(filter)[0]}
                <HighlightOffIcon
                  className="remove-filter-icon"
                  onClick={() => deleteFilterAtIndex(index)}
                />
              </div>
            ))
          : !showDefaultFilters && (
              <h6>
                <b
                  style={{
                    font: "normal normal bold 14px/16px Encode Sans",
                    letterSpacing: " -0.29px",
                    color: "#585858",
                  }}
                >
                  Apply the above filters to sort out based on your needs
                </b>
                .
              </h6>
            )}
      </div>

      {!(
        // loadingRenderImages &&
        loadingSimilarDesignData
      ) ? (
        //&& loadingFamilyDesignData
        <>
          <div className="selected-design">
            {loadingRenderImages ? (
              <div style={{ height: "40vh", paddingTop: "12vh" }}>
                <center>
                  <img
                    src={Loader}
                    style={{
                      height: "10vh",
                      width: "auto",
                      zIndex: 5,
                    }}
                  />
                </center>
              </div>
            ) : imagesArr.length ? (
              <Carousel
                activeIndex={index}
                onSelect={handleSelect}
                nextIcon={
                  imgNumber < imagesArr.length ? (
                    <span
                      aria-hidden="true"
                      className="carousel-control-next-icon"
                      disabled
                      onClick={() =>
                        imgNumber == imagesArr.length
                          ? null
                          : setImgNumber(imgNumber + 1)
                      }
                      style={{ backgroundColor: "black", height: "5vh" }}
                    />
                  ) : null
                }
                prevIcon={
                  imgNumber > 1 ? (
                    <span
                      aria-hidden="true"
                      className="carousel-control-prev-icon"
                      disabled={imgNumber == 0 ? true : false}
                      onClick={() =>
                        imgNumber == 1 ? null : setImgNumber(imgNumber - 1)
                      }
                      style={{ backgroundColor: "black", height: "5vh" }}
                    />
                  ) : null
                }
                interval={null}
                touch={false}
              >
                {imagesArr.map((img) => (
                  <Carousel.Item>
                    <img
                      className="d-block w-100"
                      src={img.url}
                      alt="First slide"
                      style={{
                        height: "auto",
                        width: "100%",
                        minHeight: "30vh",
                      }}
                    />
                    <Carousel.Caption
                      style={{
                        position: "absolute",
                        top: "0vh",
                        paddingTop: "0.5vh",
                        paddingBottom: "0vh",
                        fontSize: "0.8rem",
                        height: "fit-content",
                        background: "rgba(0,0,0,0.4)",
                        paddingTop: "3px",
                        paddingBottom: "0px",
                        color: "white",
                        width: "100vw",
                        paddingLeft: "0vw !important",
                        right: "0%",
                        left: "0%",
                        zIndex: 1,
                      }}
                    >
                      <h6 style={{ fontSize: "0.8rem", lineHeight: "1" }}>
                        {img.name}
                      </h6>
                    </Carousel.Caption>

                    <Carousel.Caption
                      style={{
                        position: "absolute",
                        bottom: "0%",
                        left: "0%",
                        paddingTop: "0.5vh",
                        paddingBottom: "0.5vh",
                        backgroundColor: "#000000",
                        height: "fit-content",
                        opacity: "0.8",
                        color: "white",
                        width: "15vw",
                        zIndex: 1,
                      }}
                    >
                      {imgNumber}
                      {"/"}
                      {imagesArr.length}
                    </Carousel.Caption>
                  </Carousel.Item>
                ))}
              </Carousel>
            ) : (
              <div style={{ height: "10vh", paddingTop: "5vh" }}>
                <center>
                  <h5>
                    <b>No images to display :(</b>
                  </h5>
                </center>
              </div>
            )}
          </div>

          {!loadingRenderImages && loadedRenderImages && imagesArr.length ? (
            <div className="design-actions-wrapper">
              <div className="design-actions row">
                <div className=" design-action col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 ">
                  <img
                    src={liked ? LikedIcon : LikeIcon}
                    className="like-icon"
                    onClick={() => {
                      const likesArr = JSON.parse(
                        localStorage.getItem("likes")
                      );
                      const designId = localStorage.getItem("design-id");
                      if (!liked) {
                        setLiked(true);
                        // push desing id to array
                        likesArr.push(designId);
                        if (!localStorage.getItem("logged_in") && !wishlisted)
                          setShowSignUpModal(true);
                      } else {
                        setLiked(false);
                        const idx = likesArr.indexOf(designId);
                        likesArr.splice(idx, 1);
                      }
                      localStorage.setItem("likes", JSON.stringify(likesArr));
                    }}
                  />
                  {/* <p>Like</p> */}
                </div>
                <div className=" design-action col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                  <img
                    src={wishlisted ? WishlistedIcon : WishlistIcon}
                    className="wishlist-icon"
                    onClick={() => {
                      localStorage.getItem("logged_in")
                        ? wishlistHandler()
                        : setShowAuthModal();
                    }}
                  />
                </div>
                <div className=" design-action col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                  <img
                    src={ZoomInIcon}
                    className="zoom-in-icon"
                    onClick={() => setShowZoomInModal(true)}
                  />
                </div>
                <div className=" design-action col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                  <img
                    src={ViewDetailsIcon}
                    className="view-details-icon"
                    onClick={() => setShowViewDetailsModal(true)}
                  />
                </div>
              </div>
            </div>
          ) : null}

          <div className="design-section">
            <div className="title">
              <b>
                AVAILABLE DESIGNS{" "}
                {loadedSimilarDesignData
                  ? "(" + similarDesignData.length + ")"
                  : null}
              </b>
            </div>

            <div
              className="items row flex-row flex-nowrap horizontal-design-scroll"
              style={{
                paddingLeft: "2vw",
                paddingRight: "2vw",
                marginLeft: "0",
                marginRight: "0",
              }}
            >
              {loadingSimilarDesignData ? (
                loader()
              ) : (
                <>
                  <div
                    className="item col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 "
                    style={{
                      marginTop: "1vh",
                      paddingLeft: "2vw",
                      paddingRight: "2vw",
                    }}
                  >
                    <div
                      style={{
                        backgroundImage: `url(${defaultDesign.image_thumbnail})`,
                        height: "90px",
                        border:
                          currentDesign.design_id == defaultDesign.design_id
                            ? "8px solid #585858"
                            : "",
                      }}
                      onClick={() => switchDesignIDHandler(defaultDesign)}
                    />
                    <div className="item-name">
                      <h6>
                        <b>
                          {defaultDesign.design_name +
                            " " +
                            defaultDesign.design_id}
                        </b>
                      </h6>
                    </div>
                  </div>
                  {similarDesignData.map((design) => (
                    <div
                      className="item col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 "
                      style={{
                        marginTop: "1vh",
                        paddingLeft: "2vw",
                        paddingRight: "2vw",
                      }}
                    >
                      <div
                        style={{
                          backgroundImage: `url(${design.image_thumbnail})`,
                          height: "90px",
                          cursor: "pointer",
                          border:
                            currentDesign.design_id == design.design_id
                              ? "8px solid #585858"
                              : "",
                        }}
                        onClick={() => switchDesignIDHandler(design)}
                      />
                      <div className="item-name">
                        <h6>
                          <b>{design.design_name + " " + design.design_id}</b>
                        </h6>
                      </div>
                    </div>
                  ))}
                </>
              )}
            </div>
          </div>

          <div className="design-section">
            <div className="title">
              <b>
                FAMILY OF DESIGNS{" "}
                {loadedFamilyDesignData
                  ? "(" + familyDesignData.length + ")"
                  : null}
              </b>
            </div>
            {loadingFamilyDesignData ? (
              loader()
            ) : (
              <div
                className="items row flex-row flex-nowrap horizontal-design-scroll"
                style={{
                  paddingLeft: "2vw",
                  paddingRight: "2vw",
                  marginLeft: "0",
                  marginRight: "0",
                }}
              >
                {familyDesignData.map((design) => (
                  <div
                    className="item col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 "
                    style={{
                      marginTop: "1vh",
                      paddingLeft: "2vw",
                      paddingRight: "2vw",
                    }}
                  >
                    <div
                      style={{
                        backgroundImage: `url(${design.image_thumbnail})`,
                        height: "90px",
                        cursor: "pointer",
                      }}
                      onClick={() => switchDesignIDHandler(design)}
                    />
                    <div className="item-name">
                      <h6>
                        <b>{design.design_name + " " + design.design_id}</b>
                      </h6>
                    </div>
                  </div>
                ))}
              </div>
            )}
          </div>
        </>
      ) : (
        loader()
      )}

      {selectedFilterCategory == "product_names" && getPrimaryListProduct()}
      {selectedFilterCategory != "" &&
        selectedFilterCategory != "product_names" &&
        getList()}

      {selectedFilterCategory != "" && selectedCatIdx != -1 && getList()}
      {showViewDetailsModal != "" && getViewDetailsModal()}
      {showZoomInModal != "" && getShowZoomInModal()}
      {showSignUpModal != "" && getSignInPopUp()}
      {showWishlistModal != "" && getWishlistModal()}
      {showAuthModal != "" && getAuthModal()}
    </div>
  );
};

export default withRouter(SearchLaminatesDetails);
