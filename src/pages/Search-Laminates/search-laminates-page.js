import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardMedia from "@material-ui/core/CardMedia";
import React, { useState, useEffect, useRef } from "react";
import { withRouter } from "react-router-dom";
import Loader from "../../../public/assets/loader1.gif";
import { apiActions } from "../../../server";
import { useThemeSelection } from "../../hooks/theme-hook";
import "../../styles/home-page.css";
import history from "../../util/history-util";
import "../../styles/home-page.css";
import "./search-laminates.css";
import searchIcon from "../../../public/assets/search.png";
import ArrowUp from "../../../public/assets/arrow-up-squared.png";
import CancelIcon from "@material-ui/icons/Cancel";
import chatbot1 from "../../../public/assets/chatbot1.png";

const SearchLaminatesCategory = (props) => {
  const prevScrollY = useRef(0);
  const { currentThemeSelection } = useThemeSelection();
  const [isLoading, setIsLoading] = useState(false);
  const [searchKeyword, setSearchKeyword] = useState("");
  const [productDetailsList, setProductDetailsList] = useState([]);
  const [isPaginationLoading, setIsPaginationLoading] = useState(false);
  const [callPaginatuon, setCallPaginatuon] = useState(true);
  const [isApiCall, setIsApiCall] = useState(false);
  const [isLastPage, setIsLastPage] = useState(false);
  const [currentPage, setCrrentPage] = useState(1);
  const [isShowError, setIsShowError] = useState(false);
  const [isVisibleTop, setIsVisibleTop] = useState(false);
  const [showAssistantPopupFirst, setShowAssistantPopupFirst] = useState(false);

  const toggleVisibility = () => {
    if (window.pageYOffset > 300) {
      setIsVisibleTop(true);
    } else {
      setIsVisibleTop(false);
    }
  };

  // Set the top cordinate to 0
  // make scrolling smooth
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  useEffect(() => {
    window.addEventListener("scroll", toggleVisibility);
  }, []);

  useEffect(() => {
    localStorage.setItem("selected-space", JSON.stringify([]));
    let localStorageData = JSON.parse(
      localStorage.getItem("search-keywords-list")
    );
    let localStorageTitle = localStorage.getItem("search-keywords-title");
    setSearchKeyword(localStorageTitle);
    if (localStorageData.length !== 0) {
      setProductDetailsList(localStorageData);
    }
  }, []);

  const assistantPopupFirst = () => {
    return (
      <div className="popup-overlay">
        <div className="assistant-center-modal-wrapper">
          <CancelIcon
            className="close-modal-assistant-center"
            style={{ zIndex: 10000 }}
            onClick={() => {
              setShowAssistantPopupFirst(false);
            }}
          />

          <div className="assistant-center-modal">
            <div className="content-inline">
              <div className="assistant-center-modal-text">
                <b>
                  Hi there! <br /> Hope you are doing good. <br /> How can I
                  help you today?
                  <br />
                  Choose one from below category.
                </b>
                <br />
              </div>

              <div className="assistant-center-modal-image">
                <img src={chatbot1} />
              </div>
            </div>

            <div className="assistant-center-modal-options">
              <center>
                <div onClick={() => history.push("/color")}>
                  <center>Search based on Space category</center>
                </div>

                <div onClick={() => history.push("/space")}>
                  <center>Search based on Color category</center>
                </div>

                <div onClick={() => history.push("/product-category")}>
                  <center>Search based on Product category</center>
                </div>

                <div onClick={() => history.push("/laminates-design")}>
                  <center>Download E Catalogue</center>
                </div>

                <div onClick={() => history.push("/search")}>
                  <center>Recommended designs</center>
                </div>

                <div onClick={() => setShowAssistantPopupFirst(false)}>
                  <center>Trending designs</center>
                </div>

                <div onClick={() => setShowAssistantPopupFirst(false)}>
                  <center>Trending designs</center>
                </div>

                <div onClick={() => setShowAssistantPopupFirst(false)}>
                  <center>FAQs</center>
                </div>

                <div onClick={() => setShowAssistantPopupFirst(false)}>
                  <center>Quick links</center>
                </div>
              </center>
            </div>
          </div>
        </div>
      </div>
    );
  };

  useEffect(() => {
    var url = window.location.pathname;
    var filename = url.substring(url.lastIndexOf("/") + 1);
    if (filename === "search-laminates") {
      const handleScroll = () => {
        window.onscroll = () => {
          let currentScrollPos = window.pageYOffset;
          let maxScroll = document.body.scrollHeight - window.innerHeight;
          if (
            !isPaginationLoading &&
            isLastPage === false &&
            currentScrollPos > 0 &&
            currentScrollPos > maxScroll &&
            isApiCall
          ) {
            callPagination();
          }
        };
      };

      window.addEventListener("scroll", handleScroll);

      return () => window.removeEventListener("scroll", handleScroll);
    }
  }, [isPaginationLoading, productDetailsList, isLastPage]);

  Object.size = function(obj) {
    var size = 0,
      key;
    for (key in obj) {
      if (obj.hasOwnProperty(key)) size++;
    }
    return size;
  };

  const onSearchLaminates = (value) => {
    setSearchKeyword(value);
    setIsPaginationLoading(false);
    localStorage.setItem("search-keywords-title", value);

    function filterIt(arr, searchKey) {
      return arr.filter(function(obj) {
        return obj.design_search.includes(searchKey);
      });
    }

    let localStorageDesigns = JSON.parse(localStorage.getItem("designs"));
    const values = Object.values(localStorageDesigns);
    const data = filterIt(values, value);
    setProductDetailsList(data);
    localStorage.setItem("search-keywords-list", JSON.stringify(data));
    let localStorageData = JSON.parse(
      localStorage.getItem("search-keywords-list")
    );
    setIsShowError(false);
    if (localStorageData.length === 0) {
      const splice = value.trim();
      if (splice !== "") setIsApiCall(true);
      setIsLoading(true);
      apiActions.get_designs({ design_search: "~" + value }, (res) => {
        setIsLoading(false);
        if (res.data.length !== 0) {
          setProductDetailsList(res.data);
          localStorage.setItem(
            "search-keywords-list",
            JSON.stringify(res.data)
          );
        } else {
          setIsShowError(true);
        }
      });
    }
  };

  const onClose = () => {
    setSearchKeyword("");
    setProductDetailsList([]);
    setIsLoading(false);
    setIsApiCall(false);
    setIsPaginationLoading(false);
    localStorage.setItem("search-keywords-title", "");
    localStorage.setItem("search-keywords-list", JSON.stringify([]));
  };

  const callPagination = () => {
    var url = window.location.pathname;
    var filename = url.substring(url.lastIndexOf("/") + 1);
    if (filename === "search-laminates") {
      let page = currentPage + 1;
      setCrrentPage(page);
      setIsPaginationLoading(true);
      apiActions.get_designs(
        { design_search: "~" + searchKeyword, _page_number: page },
        (res) => {
          if (res.is_last === true) {
            setIsLastPage(res.is_last);
          }
          if (res.data.length !== 0) {
            const List = [...productDetailsList, ...res.data];
            localStorage.setItem("search-keywords-list", JSON.stringify(List));
            setProductDetailsList(List);
            setIsPaginationLoading(false);
          }
        }
      );
    }
  };

  const handleKeyDown = (e) => {
    e.preventDefault();
    const splice = searchKeyword.trim();
    if (splice !== "") setIsApiCall(true);
    setIsLoading(true);
    apiActions.get_designs({ design_search: "~" + searchKeyword }, (res) => {
      setIsLoading(false);
      if (res.data.length !== 0) {
        setProductDetailsList(res.data);
        localStorage.setItem("search-keywords-list", JSON.stringify(res.data));
      }
    });
  };

  return (
    <div
      style={{
        backgroundColor: `#${
          currentThemeSelection === "lite" ? "white" : "3c3599"
        }`,
        height: "auto",
        marginTop: "13vh",
        padding: "0.8rem",
      }}
      className="home-page-categories"
    >
      <div className="upcomingCampaignHeader">
        <div className="right">
          <div className="searchWrap search-input">
            <form
              onSubmit={(e) => {
                handleKeyDown(e);
              }}
            >
              <div
                onClick={(e) => {
                  handleKeyDown(e);
                }}
              >
                <img
                  src={searchIcon}
                  alt="Smiley face"
                  className="search-icon"
                  width="20"
                  height="20"
                />
              </div>
              <input
                type="text"
                value={searchKeyword}
                placeholder="Search based on design number"
                className="form-control"
                onChange={(e) => {
                  onSearchLaminates(e.target.value);
                }}
              />
              <div
                onClick={() => {
                  onClose();
                }}
                className="search-close-icon"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="20"
                  height="20"
                  viewBox="0 0 24 24"
                  fill="none"
                  stroke="black"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                >
                  <line x1="18" y1="6" x2="6" y2="18" />
                  <line x1="6" y1="6" x2="18" y2="18" />
                </svg>
              </div>
            </form>
          </div>
        </div>
        <div
          onClick={() => {
            onClose();
          }}
          className="search-cancel-btn"
        >
          Cancel
        </div>
      </div>
      <center>
        {isLoading && (
          <div style={{ marginTop: "50vw" }}>
            <img
              src={Loader}
              style={{
                height: "10vh",
                width: "auto",
                zIndex: 5,
              }}
            />
          </div>
        )}
      </center>
      {productDetailsList.length !== 0 && (
        <div className="all-photos-text pl-0" style={{ marginTop: 60 }}>
          All photos ({productDetailsList.length})
        </div>
      )}
      {isShowError ? (
        <div className="row justify-center" style={{ marginTop: 100 }}>
          <p>Invalid search keyword</p>
        </div>
      ) : (
        productDetailsList.length !== 0 && (
          <div className="row" style={{ marginTop: 0 }}>
            {productDetailsList.map((item, index) => {
              return (
                <div
                  key={index}
                  className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
                  style={{ marginTop: "2vh" }}
                >
                  <Card
                    className="card-style"
                    className="card-style"
                    onClick={() => {
                      localStorage.setItem("search-design-id", item.design_id);
                      localStorage.setItem(
                        "search-design",
                        JSON.stringify(item)
                      );
                      history.push({
                        pathname: "/search-page-second",
                      });
                    }}
                  >
                    <CardMedia
                      image={item.image_thumbnail}
                      style={{
                        height: 0,
                        paddingTop: "56.25%", // 16:9
                      }}
                    />

                    <CardActions
                      style={{
                        justifyContent: "center",
                        marginTop: "-6vh",
                        paddingBottom: "0px",
                        paddingLeft: "0px",
                        paddingRight: "0px",
                      }}
                    >
                      <Button
                        style={{
                          color: "white",
                          width: "100%",
                          background: "rgba(0,0,0,0.4)",
                          paddingTop: "0px",
                          paddingBottom: "0px",
                          borderRadius: "0px",
                          fontSize: "12px",
                          font: "normal normal medium 16px/20px Encode Sans",
                        }}
                        size="big"
                      >
                        <b
                          style={{
                            fontFamily: "Encode Sans",
                            fontSize: "0.8rem",
                            fontWeight: "500",
                            textTransform: "none",
                            // letterSpacing: "1.2px",
                          }}
                        >
                          {" "}
                          DESIGN {item.design_number} -{item.design_name}
                        </b>
                      </Button>
                    </CardActions>
                  </Card>
                </div>
              );
            })}
          </div>
        )
      )}
      <div style={{ height: "5vh" }} />
      <center>
        {isPaginationLoading && (
          <div style={{ marginBottom: 40 }}>
            <img
              src={Loader}
              style={{
                height: "10vh",
                width: "auto",
                zIndex: 5,
              }}
            />
          </div>
        )}
      </center>
      <div style={{ height: "5vh" }} />

      {showAssistantPopupFirst && assistantPopupFirst()}
      {isVisibleTop && (
        <div
          style={{
            height: 35,
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            width: 35,
            position: "fixed",
            bottom: 80,
            right: 20,
            borderRadius: 100,
            overflow: "hidden",
            backgroundColor: "rgb(245, 245, 245)",
            zIndex: 5,
          }}
          onClick={() => {
            scrollToTop();
          }}
        >
          <img
            src={ArrowUp}
            style={{
              height: 25,
              width: 25,
              zIndex: 5,
            }}
          />
        </div>
      )}
    </div>
  );
};

export default withRouter(SearchLaminatesCategory);
