import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardMedia from "@material-ui/core/CardMedia";
import React, { useEffect, useRef, useState } from "react";
import { withRouter } from "react-router-dom";
import ArrowUp from "../../../public/assets/arrow-up-squared.png";
import Loader from "../../../public/assets/loader1.gif";
import { apiActions } from "../../../server";
import { useThemeSelection } from "../../hooks/theme-hook";
import "../../styles/home-page.css";
import history from "../../util/history-util";

const ProductDetails = (props) => {
  const prevScrollY = useRef(0);
  const { currentThemeSelection } = useThemeSelection();
  const [isLoading, setIsLoading] = useState(true);
  const [productDetailsList, setProductDetailsList] = useState([]);
  const [isPaginationLoading, setIsPaginationLoading] = useState(false);
  const [callPaginatuon, setCallPaginatuon] = useState(true);
  const [isLastPage, setIsLastPage] = useState(false);
  const [currentPage, setCrrentPage] = useState(1);
  const [isVisibleTop, setIsVisibleTop] = useState(false);
  const [totalDesigns, setTotalDesigns] = useState(0);

  const toggleVisibility = () => {
    if (window.pageYOffset > 300) {
      setIsVisibleTop(true);
    } else {
      setIsVisibleTop(false);
    }
  };

  // Set the top cordinate to 0
  // make scrolling smooth
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  useEffect(() => {
    window.addEventListener("scroll", toggleVisibility);
  }, []);

  useEffect(() => {
    const data = {
      design_category: props.location.state.product_categories,
    };
    apiActions.get_designs(data, (res) => {
      setIsLoading(false);
      setIsLastPage(res.is_last);
      if (res.data.length !== 0) {
        setTotalDesigns(res.total_number);
        setProductDetailsList(res.data);
      }
    });
  }, []);

  useEffect(() => {
    var url = window.location.pathname;
    var filename = url.substring(url.lastIndexOf("/") + 1);
    if (filename === "laminates-design-category") {
      const handleScroll = () => {
        window.onscroll = () => {
          let currentScrollPos = window.pageYOffset;
          let maxScroll = document.body.scrollHeight - window.innerHeight;
          if (
            !isPaginationLoading &&
            isLastPage === false &&
            currentScrollPos > 0 &&
            currentScrollPos > maxScroll
          ) {
            callPagination();
          }
        };
      };

      window.addEventListener("scroll", handleScroll);

      return () => window.removeEventListener("scroll", handleScroll);
    }
  }, [isPaginationLoading, productDetailsList, isLastPage]);

  const callPagination = () => {
    var url = window.location.pathname;
    var filename = url.substring(url.lastIndexOf("/") + 1);
    if (filename === "laminates-design-category") {
      const data = {
        design_category: props.location.state.product_categories,
      };
      let page = currentPage + 1;
      setCrrentPage(page);
      setIsPaginationLoading(true);
      apiActions.get_designs({ ...data, _page_number: page }, (res) => {
        if (res.is_last === true) {
          setIsLastPage(res.is_last);
        }
        if (res.data.length !== 0) {
          const List = [...productDetailsList, ...res.data];
          setProductDetailsList(List);
          setIsPaginationLoading(false);
        }
      });
    }
  };

  return (
    <div
      style={{
        backgroundColor: `#${
          currentThemeSelection === "lite" ? "white" : "3c3599"
        }`,
        height: "auto",
        marginTop: "13vh",
        padding: "0.8rem",
      }}
      className="home-page-categories"
    >
      <div className="explore-now">
        <center>
          <h5
            className="text-uppercase"
            style={{
              letterSpacing: "1px",
              color: " #585858",
              fontWeight: "bold",
              fontSize: "19px",
            }}
          >
            {props.location.state.product_name
              ? props.location.state.product_name
              : props.location.state.product_categories}
          </h5>
        </center>
        {!isLoading && productDetailsList.length !== 0 && (
          <div className="all-photos-text pl-0" style={{ marginBottom: -10 }}>
            All photos ({totalDesigns})
          </div>
        )}
        {!isLoading &&
          (productDetailsList.length === 0 ? (
            <div
              style={{ textAlign: "center", width: "100vw", marginTop: "10vh" }}
            >
              <center>
                <h6>No results found</h6>
              </center>
            </div>
          ) : (
            <div className="row">
              {productDetailsList.map((item, index) => {
                return (
                  <div
                    key={index}
                    className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
                    style={{ marginTop: "2vh" }}
                  >
                    <Card
                      className="card-style"
                      className="card-style"
                      onClick={() => {
                        localStorage.setItem("design-id", item.design_id);
                        localStorage.setItem("design", JSON.stringify(item));
                        history.push({
                          pathname: "/laminates-design-details",
                          state: {
                            product_title: props.location.state.product_name
                              ? props.location.state.product_name
                              : props.location.state.product_categories,
                          },
                        });
                      }}
                    >
                      <CardMedia
                        image={item.image_thumbnail}
                        style={{
                          height: 0,
                          paddingTop: "56.25%", // 16:9
                        }}
                      />

                      <CardActions
                        style={{
                          justifyContent: "center",
                          marginTop: "-6vh",
                          paddingBottom: "0px",
                          paddingLeft: "0px",
                          paddingRight: "0px",
                        }}
                      >
                        <Button
                          style={{
                            color: "white",
                            width: "100%",
                            background: "rgba(0,0,0,0.4)",
                            paddingTop: "0px",
                            paddingBottom: "0px",
                            borderRadius: "0px",
                            fontSize: "12px",
                            font: "normal normal medium 16px/20px Encode Sans",
                          }}
                          size="big"
                        >
                          <b
                            style={{
                              fontFamily: "Encode Sans",
                              fontSize: "0.8rem",
                              fontWeight: "500",
                              textTransform: "none",
                              // letterSpacing: "1.2px",
                            }}
                          >
                            {" "}
                            DESIGN {item.design_number} -{item.design_name}
                          </b>
                        </Button>
                      </CardActions>
                    </Card>
                  </div>
                );
              })}
            </div>
          ))}
      </div>
      <center>
        {isPaginationLoading && (
          <img
            src={Loader}
            style={{
              height: "10vh",
              width: "auto",
              zIndex: 5,
            }}
          />
        )}
      </center>
      <div style={{ height: "10vh" }} />
      <div>
        <center>
          {isLoading && (
            <img
              src={Loader}
              style={{
                height: "10vh",
                width: "auto",
                zIndex: 5,
              }}
            />
          )}
        </center>
      </div>
      {isVisibleTop && (
        <div
          style={{
            height: 35,
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            width: 35,
            position: "fixed",
            bottom: 80,
            right: 20,
            borderRadius: 100,
            overflow: "hidden",
            backgroundColor: "rgb(245, 245, 245)",
            zIndex: 5,
          }}
          onClick={() => {
            scrollToTop();
          }}
        >
          <img
            src={ArrowUp}
            style={{
              height: 25,
              width: 25,
              zIndex: 5,
            }}
          />
        </div>
      )}
    </div>
  );
};

export default withRouter(ProductDetails);
