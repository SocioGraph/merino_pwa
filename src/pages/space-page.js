import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardMedia from "@material-ui/core/CardMedia";
import CancelIcon from "@material-ui/icons/Cancel";
import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import ArrowUp from "../../public/assets/arrow-up-squared.png";
import chatbot from "../../public/assets/chatbot.png";
import chatbot1 from "../../public/assets/chatbot1.png";
import Explore3 from "../../public/assets/living.jpg";
import Office from "../../public/assets/office.jpg";
import ClassicOffice from "../../public/assets/classic_office.jpg";
import Explore2 from "../../public/assets/space-bathroom.jpeg";
import Bedroom from "../../public/assets/space-bedroom.jpeg";
import Explore4 from "../../public/assets/space-exterior.jpeg";
import Kitchen from "../../public/assets/space-kitchen.jpeg";
import { useThemeSelection } from "../hooks/theme-hook";
import { apiActions } from "../../server";
import "../styles/home-page.css";
import history from "../util/history-util";

const SpacePage = () => {
  const { currentThemeSelection } = useThemeSelection();
  const [showSignUpModal, setShowSignUpModal] = useState(false);
  const [showAssistantPopupFirst, setShowAssistantPopupFirst] = useState(true);
  const [showAssistantPopupSecond, setShowAssistantPopupSecond] = useState(
    false
  );
  const [selectedSpace, setSelectedSpace] = useState("");
  const [isVisibleTop, setIsVisibleTop] = useState(false);

  const toggleVisibility = () => {
    if (window.pageYOffset > 300) {
      setIsVisibleTop(true);
    } else {
      setIsVisibleTop(false);
    }
  };

  // Set the top cordinate to 0
  // make scrolling smooth
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  useEffect(() => {
    const str = window.location.pathname;
    apiActions.patch_user(
      {
        pwa_flow: str,
        _async: true,
      },

      (res) => console.log(res),
      (err) => console.log(err)
    );
    window.addEventListener("scroll", toggleVisibility);
  }, []);

  useEffect(() => {
    // clear off filters in selected-space
    localStorage.setItem("selected-space", JSON.stringify([]));
  }, []);

  const assistantPopupFirst = () => {
    return (
      <div className="popup-overlay">
        <div className="assistant-bottom-right-modal-wrapper">
          <CancelIcon
            className="close-modal-assistant-bottom-right"
            onClick={() => setShowAssistantPopupFirst(false)}
          />

          <div className="assistant-bottom-right-modal">
            <div className="content-inline">
              <div className="assistant-bottom-right-modal-text">
                <b>
                  Looking designs for a particular <br />
                  surface? Tap here !
                </b>
                <br />
              </div>

              <div className="assistant-bottom-right-modal-image">
                <img
                  src={chatbot}
                  style={{
                    maxWidth: "unset !important",
                    width: "100px",
                    marginLeft: "-10vw",
                    marginTop: "-1vh",
                  }}
                />
              </div>
            </div>

            <br />
            <br />
            <div className="assistant-bottom-right-modal-options">
              <div
                onClick={() => {
                  setShowAssistantPopupFirst(false);
                  setShowAssistantPopupSecond(true);
                }}
              >
                <center>
                  <center>YES, LET'S EXPLORE</center>
                </center>
              </div>

              <div onClick={() => setShowAssistantPopupFirst(false)}>
                <center>
                  <center>NO, MAYBE LATER</center>
                </center>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  const assistantPopupSecond = () => {
    return (
      <div className="popup-overlay">
        <div className="assistant-center-options-modal-wrapper">
          <CancelIcon
            className="close-modal-assistant-center-options"
            onClick={() => setShowAssistantPopupSecond(false)}
          />

          <div className="assistant-center-options-modal">
            <div className="content-inline">
              <div className="assistant-center-options-modal-text">
                <b>Please select a surface to proceed.</b>
                <br />
              </div>

              <div className="assistant-center-options-modal-image">
                <img src={chatbot1} />
              </div>
            </div>

            <div className="assistant-center-options-modal-options">
              <center>
                <div
                  onClick={() =>
                    history.push({
                      pathname: `/explore`,
                      state: { surfaces: "bed" },
                    })
                  }
                >
                  <center
                    onClick={() =>
                      history.push({
                        pathname: `/explore`,
                        state: { surfaces: "bed" },
                      })
                    }
                  >
                    BED
                  </center>
                </div>

                <div
                  onClick={() =>
                    history.push({
                      pathname: `/explore`,
                      state: { surfaces: "wardrobe" },
                    })
                  }
                >
                  <center
                    onClick={() =>
                      history.push({
                        pathname: `/explore`,
                        state: { surfaces: "wardrobe" },
                      })
                    }
                  >
                    WARDROBE
                  </center>
                </div>

                <div
                  onClick={() =>
                    history.push({
                      pathname: `/explore`,
                      state: { surfaces: "shelf" },
                    })
                  }
                >
                  <center
                    onClick={() =>
                      history.push({
                        pathname: `/explore`,
                        state: { surfaces: "shelf" },
                      })
                    }
                  >
                    SHELF
                  </center>
                </div>

                <div
                  onClick={() =>
                    history.push({
                      pathname: `/explore`,
                      state: { surfaces: "cabinet" },
                    })
                  }
                >
                  <center
                    onClick={() =>
                      history.push({
                        pathname: `/explore`,
                        state: { surfaces: "cabinet" },
                      })
                    }
                  >
                    CABINET
                  </center>
                </div>
              </center>
            </div>
          </div>
        </div>
      </div>
    );
  };

  return (
    <div
      style={{
        backgroundColor: `#${
          currentThemeSelection === "lite" ? "white" : "3c3599"
        }`,
        height: "auto",
        marginTop: "13vh",
        padding: "0.8rem",
      }}
      className="home-page-categories"
    >
      <div className="explore-now">
        <center>
          <h5
            style={{
              letterSpacing: "1px",
              color: " #585858",
              fontWeight: "bold",
              fontSize: "19px",
            }}
          >
            SPACES
          </h5>
        </center>
        <div className="row">
          <div
            className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
            style={{ marginTop: "2vh" }}
          >
            <Card
              className="card-style"
              className="card-style"
              onClick={() => {
                localStorage.setItem(
                  "selected-space",
                  JSON.stringify(["Bedroom"])
                );
                history.push("/space-first");
              }}
            >
              <CardMedia
                image={Bedroom}
                style={{
                  height: 0,
                  paddingTop: "56.25%", // 16:9
                }}
              />

              <CardActions
                style={{
                  justifyContent: "center",
                  marginTop: "-6vh",
                  paddingBottom: "0px",
                  paddingLeft: "0px",
                  paddingRight: "0px",
                }}
              >
                <Button
                  style={{
                    color: "white",
                    width: "100%",
                    background: "rgba(0,0,0,0.4)",
                    paddingTop: "0px",
                    paddingBottom: "0px",
                    borderRadius: "0px",
                    fontSize: "12px",
                    font: "normal normal medium 16px/20px Encode Sans",
                  }}
                  size="big"
                >
                  <b
                    style={{
                      fontFamily: "Encode Sans",
                      fontSize: "0.8rem",
                      fontWeight: "500",
                      textTransform: "none",
                      // letterSpacing: "1.2px",
                    }}
                  >
                    {" "}
                    BEDROOM
                  </b>
                </Button>
              </CardActions>
            </Card>
          </div>

          <div
            className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
            style={{ marginTop: "2vh" }}
          >
            <Card
              className="card-style"
              onClick={() => {
                localStorage.setItem(
                  "selected-space",
                  JSON.stringify(["Bathroom"])
                );
                history.push("/space-first");
              }}
            >
              <CardMedia
                image={Explore2}
                style={{
                  height: 0,
                  paddingTop: "56.25%", // 16:9
                }}
              />

              <CardActions
                style={{
                  justifyContent: "center",
                  marginTop: "-6vh",
                  paddingBottom: "0px",
                  paddingLeft: "0px",
                  paddingRight: "0px",
                }}
              >
                <Button
                  style={{
                    color: "white",
                    width: "100%",
                    background: "rgba(0,0,0,0.4)",
                    paddingTop: "0px",
                    paddingBottom: "0px",
                    borderRadius: "0px",
                    fontSize: "12px",
                    font: "normal normal medium 16px/20px Encode Sans",
                  }}
                  size="big"
                >
                  <b
                    style={{
                      fontFamily: "Encode Sans",
                      fontSize: "0.8rem",
                      fontWeight: "500",
                      textTransform: "none",
                    }}
                  >
                    {" "}
                    BATHROOM
                  </b>
                </Button>
              </CardActions>
            </Card>
          </div>
          <div
            className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
            style={{ marginTop: "2vh" }}
          >
            <Card
              className="card-style"
              onClick={() => {
                localStorage.setItem(
                  "selected-space",
                  JSON.stringify(["Small Office"])
                );
                history.push("/space-first");
              }}
            >
              <CardMedia
                image={Office}
                style={{
                  height: 0,
                  paddingTop: "56.25%", // 16:9
                }}
              />

              <CardActions
                style={{
                  justifyContent: "center",
                  marginTop: "-6vh",
                  paddingBottom: "0px",
                  paddingLeft: "0px",
                  paddingRight: "0px",
                }}
              >
                <Button
                  style={{
                    color: "white",
                    width: "100%",
                    background: "rgba(0,0,0,0.4)",
                    paddingTop: "0px",
                    paddingBottom: "0px",
                    borderRadius: "0px",
                    fontSize: "12px",
                    font: "normal normal medium 16px/20px Encode Sans",
                  }}
                  size="big"
                >
                  <b
                    style={{
                      fontFamily: "Encode Sans",
                      fontSize: "0.8rem",
                      fontWeight: "500",
                      textTransform: "none",
                      // letterSpacing: "1.2px",
                    }}
                  >
                    {" "}
                    OFFICE
                  </b>
                </Button>
              </CardActions>
            </Card>
          </div>
          <div
            className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
            style={{ marginTop: "2vh" }}
          >
            <Card
              className="card-style"
              onClick={() => {
                localStorage.setItem(
                  "selected-space",
                  JSON.stringify(["Kitchen"])
                );
                history.push("/space-first");
              }}
            >
              <CardMedia
                image={Kitchen}
                style={{
                  height: 0,
                  paddingTop: "56.25%", // 16:9
                }}
              />

              <CardActions
                style={{
                  justifyContent: "center",
                  marginTop: "-6vh",
                  paddingBottom: "0px",
                  paddingLeft: "0px",
                  paddingRight: "0px",
                }}
              >
                <Button
                  style={{
                    color: "white",
                    width: "100%",
                    background: "rgba(0,0,0,0.4)",
                    paddingTop: "0px",
                    paddingBottom: "0px",
                    borderRadius: "0px",
                    fontSize: "12px",
                    font: "normal normal medium 16px/20px Encode Sans",
                  }}
                  size="big"
                >
                  <b
                    style={{
                      fontFamily: "Encode Sans",
                      fontSize: "0.8rem",
                      fontWeight: "500",
                      textTransform: "none",
                    }}
                  >
                    {" "}
                    KITCHEN
                  </b>
                </Button>
              </CardActions>
            </Card>
          </div>
          <div
            className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
            style={{ marginTop: "2vh" }}
          >
            <Card
              className="card-style"
              onClick={() => {
                localStorage.setItem(
                  "selected-space",
                  JSON.stringify(["Living Room"])
                );
                history.push("/space-first");
              }}
            >
              <CardMedia
                image={Explore3}
                style={{
                  height: 0,
                  paddingTop: "56.25%", // 16:9
                }}
              />

              <CardActions
                style={{
                  justifyContent: "center",
                  marginTop: "-6vh",
                  paddingBottom: "0px",
                  paddingLeft: "0px",
                  paddingRight: "0px",
                }}
              >
                <Button
                  style={{
                    color: "white",
                    width: "100%",
                    background: "rgba(0,0,0,0.4)",
                    paddingTop: "0px",
                    paddingBottom: "0px",
                    borderRadius: "0px",
                    fontSize: "12px",
                    font: "normal normal medium 16px/20px Encode Sans",
                  }}
                  size="big"
                >
                  <b
                    style={{
                      fontFamily: "Encode Sans",
                      fontSize: "0.8rem",
                      fontWeight: "500",
                      textTransform: "none",
                      // letterSpacing: "1.2px",
                    }}
                  >
                    {" "}
                    LIVING ROOM
                  </b>
                </Button>
              </CardActions>
            </Card>
          </div>

          <div
            className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
            style={{ marginTop: "2vh" }}
          >
            <Card
              className="card-style"
              onClick={() => {
                localStorage.setItem(
                  "selected-space",
                  JSON.stringify(["Exterior "])
                );
                history.push("/space-first");
              }}
            >
              <CardMedia
                image={Explore4}
                style={{
                  height: 0,
                  paddingTop: "56.25%", // 16:9
                }}
              />

              <CardActions
                style={{
                  justifyContent: "center",
                  marginTop: "-6vh",
                  paddingBottom: "0px",
                  paddingLeft: "0px",
                  paddingRight: "0px",
                }}
              >
                <Button
                  style={{
                    color: "white",
                    width: "100%",
                    background: "rgba(0,0,0,0.4)",
                    paddingTop: "0px",
                    paddingBottom: "0px",
                    borderRadius: "0px",
                    fontSize: "12px",
                    font: "normal normal medium 16px/20px Encode Sans",
                  }}
                  size="big"
                >
                  <b
                    style={{
                      fontFamily: "Encode Sans",
                      fontSize: "0.8rem",
                      fontWeight: "500",
                      textTransform: "none",
                    }}
                  >
                    {" "}
                    EXTERIOR
                  </b>
                </Button>
              </CardActions>
            </Card>
          </div>
          <div
            className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
            style={{ marginTop: "2vh" }}
          >
            <Card
              className="card-style"
              onClick={() => {
                localStorage.setItem(
                  "selected-space",
                  JSON.stringify(["Classic Office"])
                );
                history.push("/space-first");
              }}
            >
              <CardMedia
                image={ClassicOffice}
                style={{
                  height: 0,
                  paddingTop: "56.25%", // 16:9
                }}
              />

              <CardActions
                style={{
                  justifyContent: "center",
                  marginTop: "-6vh",
                  paddingBottom: "0px",
                  paddingLeft: "0px",
                  paddingRight: "0px",
                }}
              >
                <Button
                  style={{
                    color: "white",
                    width: "100%",
                    background: "rgba(0,0,0,0.4)",
                    paddingTop: "0px",
                    paddingBottom: "0px",
                    borderRadius: "0px",
                    fontSize: "12px",
                    font: "normal normal medium 16px/20px Encode Sans",
                  }}
                  size="big"
                >
                  <b
                    style={{
                      fontFamily: "Encode Sans",
                      fontSize: "0.8rem",
                      fontWeight: "500",
                      textTransform: "none",
                      // letterSpacing: "1.2px",
                    }}
                  >
                    {" "}
                    CLASSIC OFFICE
                  </b>
                </Button>
              </CardActions>
            </Card>
          </div>
        </div>
      </div>

      {showAssistantPopupFirst && assistantPopupFirst()}
      {showAssistantPopupSecond && assistantPopupSecond()}

      <div style={{ height: "10vh" }} />
      {isVisibleTop && (
        <div
          style={{
            height: 35,
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            width: 35,
            position: "fixed",
            bottom: 80,
            right: 20,
            borderRadius: 100,
            overflow: "hidden",
            backgroundColor: "rgb(245, 245, 245)",
            zIndex: 5,
          }}
          onClick={() => {
            scrollToTop();
          }}
        >
          <img
            src={ArrowUp}
            style={{
              height: 25,
              width: 25,
              zIndex: 5,
            }}
          />
        </div>
      )}
    </div>
  );
};

export default withRouter(SpacePage);
