import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardMedia from "@material-ui/core/CardMedia";
import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import ArrowUp from "../../public/assets/arrow-up-squared.png";
import { apiActions } from "../../server";
import { useThemeSelection } from "../hooks/theme-hook";
import "../styles/home-page.css";
import history from "../util/history-util";

const SpacePage = () => {
  const { currentThemeSelection } = useThemeSelection();
  const [showSignUpModal, setShowSignUpModal] = useState(false);
  const [furnitures, setFurnitures] = useState([]);
  const [isVisibleTop, setIsVisibleTop] = useState(false);

  const toggleVisibility = () => {
    if (window.pageYOffset > 300) {
      setIsVisibleTop(true);
    } else {
      setIsVisibleTop(false);
    }
  };

  // Set the top cordinate to 0
  // make scrolling smooth
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  useEffect(() => {
    const str = window.location.pathname;
    apiActions.patch_user(
      {
        pwa_flow: str,
        _async: true,
      },

      (res) => console.log(res),
      (err) => console.log(err)
    );
    window.addEventListener("scroll", toggleVisibility);
  }, []);

  useEffect(() => {
    const cachedFurnitures = JSON.parse(localStorage.getItem("furnitures"));
    if (!(cachedFurnitures && cachedFurnitures.length))
      apiActions.get_furnitures(
        {},
        (res) => setFurnitures(res.data),
        (err) => console.log(err)
      );
    else setFurnitures(cachedFurnitures);
  }, []);

  return (
    <div
      style={{
        backgroundColor: `#${
          currentThemeSelection === "lite" ? "white" : "3c3599"
        }`,
        height: "auto",
        marginTop: "13vh",
        padding: "0.8rem",
      }}
      className="home-page-categories"
    >
      <div className="explore-now">
        <center>
          <h5
            style={{
              letterSpacing: "1px",
              color: " #585858",
              fontWeight: "bold",
              fontSize: "19px",
            }}
          >
            FURNITURES
          </h5>
        </center>
        <div className="row">
          {furnitures.map((furniture) => (
            <div
              className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4"
              style={{ marginTop: "2vh" }}
            >
              <Card
                className="card-style"
                className="card-style"
                onClick={() => {
                  localStorage.setItem(
                    "selected-furniture",
                    JSON.stringify([furniture.furniture_name])
                  );
                  history.push("/furniture-first");
                }}
              >
                <CardMedia
                  image={furniture.furniture_image}
                  style={{
                    height: 0,
                    paddingTop: "56.25%", // 16:9
                  }}
                />

                <CardActions
                  style={{
                    justifyContent: "center",
                    marginTop: "-6vh",
                    paddingBottom: "0px",
                    paddingLeft: "0px",
                    paddingRight: "0px",
                  }}
                >
                  <Button
                    style={{
                      color: "white",
                      width: "100%",
                      background: "rgba(0,0,0,0.4)",
                      zIndex: 1,

                      paddingTop: "0px",
                      paddingBottom: "0px",
                      borderRadius: "0px",
                      fontSize: "12px",
                      font: "normal normal medium 16px/20px Encode Sans",
                    }}
                    size="big"
                  >
                    <b
                      style={{
                        fontFamily: "Encode Sans",
                        fontSize: "0.8rem",
                        fontWeight: "500",
                        textTransform: "none",
                        // letterSpacing: "1.2px",
                      }}
                    >
                      {" "}
                      {furniture.furniture_name.toUpperCase()}
                    </b>
                  </Button>
                </CardActions>
              </Card>
            </div>
          ))}
        </div>
      </div>
      <div style={{ height: "10vh" }} />
      {isVisibleTop && (
        <div
          style={{
            height: 35,
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            width: 35,
            position: "fixed",
            bottom: 80,
            right: 20,
            borderRadius: 100,
            overflow: "hidden",
            backgroundColor: "rgb(245, 245, 245)",
            zIndex: 5,
          }}
          onClick={() => {
            scrollToTop();
          }}
        >
          <img
            src={ArrowUp}
            style={{
              height: 25,
              width: 25,
              zIndex: 5,
            }}
          />
        </div>
      )}
    </div>
  );
};

export default withRouter(SpacePage);
