import React, { useState, useEffect } from "react";
import { useStore, useAction } from "easy-peasy";
import { withRouter } from "react-router-dom";
import { useThemeSelection } from "../hooks/theme-hook";
import history from "../util/history-util";

import OnboardingImage1 from "../../public/assets/onboarding 1.png";
import OnboardingImage2 from "../../public/assets/onboarding 2.png";
import OnboardingImage3 from "../../public/assets/onboarding 3.png";

import Logo from "../../public/assets/mernino_updated_logo.png";
import Toolbar from "@material-ui/core/Toolbar";

import { apiActions } from "../../server";

import "../styles/splash-screens-page.css";

const SplashScreens = () => {
  const { currentThemeSelection } = useThemeSelection();

  const [selectedScreen, setSelectedScreen] = useState(1);
  const [fakeRegistration, setFakeRegistration] = useState(false);

  const signUpHandler = async () => {
    try {
      await apiActions.signup();
      setFakeRegistration(true);
    } catch (e) {
      setFakeRegistration(false);
    }
  };

  useEffect(() => {
    if (!localStorage.getItem("user_id")) signUpHandler();
    else history.push("/home");
  }, []);

  return (
    <div className="splash-screen">
      <div className="splash-screen-header">
        <div
          position="absolute"
          style={{
            height: "fit-content",
            position: "fixed",
            height: "15vh",
            right: "0",
            left: "0",
            top: "0",
            // display: "inline-flex",
            justifyContent: "center",
            // alignItems: "center",

            margin:
              "0% auto" /* Will not center vertically and won't work in IE6/7. */,
          }}
          className={"nav-bar-splash"}
        >
          <img
            src={Logo}
            alt="fireSpot"
            style={{
              position: "absolute",
              top: "50%",
              left: "50%",
              height: "10vh",
              width: "auto",
              transform: "translate(-50%, -50%)",
              zIndex: 5,
            }}
          />
        </div>
      </div>

      <div className="splash-image-wrapper">
        <div className="splash-image">
          <center>
            {selectedScreen == 1 ? (
              <img src={OnboardingImage1} className="splash-image" />
            ) : selectedScreen == 2 ? (
              <img src={OnboardingImage2} className="splash-image" />
            ) : (
              <img src={OnboardingImage3} className="splash-image" />
            )}
          </center>
        </div>

        <div className="splash-content">
          <center>
            {selectedScreen == 1 ? (
              <p>
                Explore beautiful and largest range of surface design ideas
                based on the latest trends from Merino !
              </p>
            ) : selectedScreen == 2 ? (
              <p>
                Visualize desired designs on surface of your choice from a wide
                range of categories available!
              </p>
            ) : (
              <p>
                Make use of Merino's own Virtual Assistant to make your search
                for the best design a lot easier!
              </p>
            )}
          </center>
        </div>

        <div className="splash-button-action">
          <center>
            {selectedScreen == 1 ? (
              <button onClick={() => setSelectedScreen(2)}>
                <b>NEXT</b>
              </button>
            ) : selectedScreen == 2 ? (
              <button onClick={() => setSelectedScreen(3)}>
                <b>NEXT</b>
              </button>
            ) : (
              <button
                disabled={!fakeRegistration}
                className={!fakeRegistration ? "disabled-btn" : "active-btn"}
                onClick={() => history.push("/home")}
              >
                <b>LET'S GO!</b>
              </button>
            )}
          </center>
        </div>

        <div className="splash-timeline-container">
          <center>
            <div className="splash-timeline-wrapper">
              <div
                className={
                  selectedScreen == 1
                    ? "selected-splash-timeline"
                    : "splash-timeline"
                }
              />
              <div
                className={
                  selectedScreen == 2
                    ? "selected-splash-timeline"
                    : "splash-timeline"
                }
              />
              <div
                className={
                  selectedScreen == 3
                    ? "selected-splash-timeline"
                    : "splash-timeline"
                }
              />
            </div>

            {/* {selectedScreen == 1 ? (
              <button>
                <b>NEXT</b>
              </button>
            ) : selectedScreen == 2 ? (
              <button>
                <b>NEXT</b>
              </button>
            ) : (
              <button>
                <b>LET'S GO!</b>
              </button>
            )} */}
          </center>
        </div>
      </div>
    </div>
  );
};

export default withRouter(SplashScreens);
