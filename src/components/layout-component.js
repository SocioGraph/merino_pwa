import React, { Fragment } from "react";
import NavBar from "../components/navbar-component";
import Footer from "../components/footer-component";
import history from "../util/history-util";
export const Layout = (props) => {
  return (
    <Fragment>
      {history.location.pathname != "/" && (
        <NavBar routePath={history.location.pathname} />
      )}
      <div
        style={{ marginTop: ".5rem", fontFamily: "Encode Sans, Expanded Bold" }}
      >
        {props.children}
      </div>
      {history.location.pathname != "/" && (
        <Footer routePath={history.location.pathname} goBack={history.goBack} />
      )}
    </Fragment>
  );
};
