import AppBar from "@material-ui/core/AppBar";
import Drawer from "@material-ui/core/Drawer";
import IconButton from "@material-ui/core/IconButton";
import { withTheme } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import MenuIcon from "@material-ui/icons/Menu";
import { makeStyles } from "@material-ui/styles";
import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import Logo from "../../public/assets/mernino_updated_logo.png";
import "../styles/nav-bar.css";
import { compose } from "../util/functional-util";
import { MenuOptions } from "./menu-options-component";
import { SettingsList } from "./settings-list-component";
import chatbot2 from "../../public/assets/chatbot2.png";
import CancelIcon from "@material-ui/icons/Cancel";
import chatbot1 from "../../public/assets/chatbot1.png";
import history from "../util/history-util";

const useNavStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
  appBar: {
    position: "fixed",
    top: "0vh",
    backgroundColor: "#fff !important",
    width: "100%",
    zIndex: 2,
  },
  navIcon: {
    color: "#3c3599",
  },
  settingsButton: {
    marginRight: -18,
  },
  title: {
    fontSize: "1.125rem",
    color: "#3c3599",
    fontWeight: 800,
    flexGrow: 1,
  },
});

const NavBar = ({ routePath }) => {
  const [settingsDrawer, setSettingsDrawer] = useState(false);
  const classes = useNavStyles();
  const [showAssistantPopupFirst, setShowAssistantPopupFirst] = useState(false);

  console.log(routePath);

  const assistantPopupFirst = () => {
    return (
      <div className="popup-overlay" style={{ zIndex: 1001 }}>
        <div
          className="assistant-center-modal-wrapper"
          style={{ zIndex: 1000 }}
        >
          <CancelIcon
            className="close-modal-assistant-center"
            style={{ zIndex: 10000 }}
            onClick={() => {
              setShowAssistantPopupFirst(false);

              // {
              //   !loggedIn && setShowAssistantPopupSecond(true);
              // }
            }}
          />

          <div className="assistant-center-modal" style={{ zIndex: 100 }}>
            <div className="content-inline">
              <div className="assistant-center-modal-text">
                <b>
                  Hello Customer! <br />
                  Welcome to Merino Laminates. <br /> I'm Dave, your virtual
                  assistant. <br />
                  Do you need me to help you explore the best designs offered by
                  us?
                </b>
                <br />
              </div>

              <div className="assistant-center-modal-image">
                <img src={chatbot1} />
              </div>
            </div>

            <div className="assistant-center-modal-options">
              <center>
                <div
                  onClick={() => {
                    history.push("/color");
                    setShowAssistantPopupFirst(false);
                  }}
                >
                  <center>VIEW LAMINATES AS PER COLORS</center>
                </div>

                <div
                  onClick={() => {
                    history.push("/space");
                    setShowAssistantPopupFirst(false);
                  }}
                >
                  <center>VIEW LAMINATES AS PER ROOMS</center>
                </div>

                <div
                  onClick={() => {
                    history.push("/product-category");
                    setShowAssistantPopupFirst(false);
                  }}
                >
                  <center>VIEW LAMINATES AS PER PRODUCT CATEGORIES</center>
                </div>

                <div
                  onClick={() => {
                    history.push("/laminates-design");
                    setShowAssistantPopupFirst(false);
                  }}
                >
                  <center>VIEW LAMINATES AS PER PREFERRED DESIGNS</center>
                </div>

                <div
                  onClick={() => {
                    history.push("/search");
                    setShowAssistantPopupFirst(false);
                  }}
                >
                  <center>SEE DESIGN INSPIRATION</center>
                </div>

                <div onClick={() => setShowAssistantPopupFirst(false)}>
                  <center>NO IT'S OK! I WILL EXPLORE ON MY OWN</center>
                </div>
              </center>
            </div>
          </div>
        </div>
      </div>
    );
  };

  useEffect(() => {
    setSettingsDrawer(false);
  }, [routePath]);

  return (
    <div className={classes.root}>
      <AppBar
        position="absolute"
        style={{
          height: "fit-content",
          position: "fixed",
          // height: "13vh",
          right: "0",
          left: "0",
          top: "0",
          // display: "inline-flex",
          justifyContent: "center",
          // alignItems: "center",
          background: "#efeeee",

          margin:
            "0% auto" /* Will not center vertically and won't work in IE6/7. */,
        }}
        className={routePath != "/" ? "nav-bar" : "nav-bar-splash"}
      >
        {/* <div className="top-toolbar">
          <h5 className="eCat">E-CATALOGUE</h5>
        </div> */}

        {routePath != "/" && (
          <Toolbar
            style={{
              height: "fit-content",
              minHeight: "0vh",
              marginTop: "0vh",
              paddingTop: "0.5vh",
              paddingBottom: "0.5vh",

              // borderBottom: "2px solid #00000029",
            }}
          >
            <span className="e-cat1">
              <a
                href="https://www.merinolaminates.com/en/e-catalogue"
                target="_blank"
              >
                <img
                  src="https://www.merinolaminates.com/wp-content/themes/merino/merinoweb-ui/assets/img/ecatalogue.png"
                  class="Icon"
                  alt="icon"
                />
                E-Catalogue{" "}
              </a>
            </span>

            <span className="flg">
              <a href="https://www.merinolaminates.com/en/select-country">
                {/* <svg class="Icon">
                  <use href="https://www.merinolaminates.com/wp-content/themes/merino/merinoweb-ui/assets/img/cobold-sprite.svg#icon-earth"></use>
                </svg> */}
                <img
                  src="https://www.merinolaminates.com/wp-content/uploads/2020/01/india-flag-xs.png"
                  class="Icon Flag"
                />
              </a>
            </span>
          </Toolbar>
        )}

        <hr
          style={{
            marginTop: "0rem",
            marginBottom: "0rem",
            color: "red",
          }}
        ></hr>

        <Toolbar
          style={{
            paddingLeft: "0vw",
            paddingRight: "0vw",
            paddingTop: "1vh",
            paddingBottom: "1vh",
          }}
        >
          {routePath != "/" && (
            <span
              onClick={() => setSettingsDrawer(!settingsDrawer)}
              className="menu-btn"
            >
              <a>
                <svg
                  class="menuicon"
                  xmlns="http://www.w3.org/2000/svg"
                  width="50"
                  height="50"
                  viewBox="0 0 50 50"
                >
                  <title>Toggle Menu</title>
                  <g>
                    <line
                      class="menuicon__bar"
                      x1="13"
                      y1="16.5"
                      x2="37"
                      y2="16.5"
                    ></line>
                    <line
                      class="menuicon__bar"
                      x1="13"
                      y1="24.5"
                      x2="37"
                      y2="24.5"
                    ></line>
                    <line
                      class="menuicon__bar"
                      x1="13"
                      y1="24.5"
                      x2="37"
                      y2="24.5"
                    ></line>
                    <line
                      class="menuicon__bar"
                      x1="13"
                      y1="32.5"
                      x2="37"
                      y2="32.5"
                    ></line>
                  </g>
                </svg>
              </a>
            </span>
          )}

          <div class="MainLogoMobile">
            <a href="https://www.merinolaminates.com/" target="_blank">
              <img
                src="https://www.merinolaminates.com/wp-content/themes/merino/merinoweb-ui/assets/img/mernino_updated_logo.png"
                alt="Merino Brand"
              />
            </a>
          </div>

          <div className="chat-bot">
            <img
              src={chatbot2}
              onClick={() =>
                setShowAssistantPopupFirst(!showAssistantPopupFirst)
              }
              style={{
                // position: "absolute",
                // top: "4vh ",
                // right: "-11vw",
                // height: "10vh",
                margin: "auto",
                height: "52px",
                // transform: "translate(-50%, -50%)",
                zIndex: 5,
              }}
            />

            <p
              style={{
                fontSize: "9px !important",
                marginBottom: "0rem",
                zIndex: 5,
                // fontSize: "12px",
                color: "#656565",
              }}
            >
              <b>Assistant</b>
            </p>
          </div>
        </Toolbar>
      </AppBar>
      <Drawer
        style={{
          position: "relative",
          zIndex: 1,
          height: "90vh",
          backgroundColor: "#f3f3f3",
        }}
        onClose={() => setSettingsDrawer(false)}
        open={settingsDrawer}
        anchor="left"
        className="drw-wrap"
      >
        <div
          tabIndex={0}
          role="button"
          className="drw-wrap-1"
          style={{
            outline: "none",
            marginTop: "28vw",
            backgroundColor: "#f3f3f3",
          }}
        >
          <MenuOptions
            className="drw-wrap-2"
            closeSettingsDrawer={() => setSettingsDrawer(false)}
          />

          <SettingsList />
        </div>
      </Drawer>

      {showAssistantPopupFirst && assistantPopupFirst()}
    </div>
  );
};

export default compose(withTheme(), withRouter)(NavBar);
