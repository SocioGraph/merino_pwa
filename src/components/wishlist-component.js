import Button from "@material-ui/core/Button";
import CancelIcon from "@material-ui/icons/Cancel";
import React, { useEffect, useState } from "react";
import { apiActions } from "../../server";
import "../styles/footer.css";
import history from "../util/history-util";

const Wishlist = (props) => {
  let wishlist = JSON.parse(localStorage.getItem("wishlist"));
  let designs = JSON.parse(localStorage.getItem("designs"));

  const [showWishlistModal, setShowWishlistModal] = useState(false);

  const [removedDesigns, setRemovedDesigns] = useState([]);

  const [loadingWishlist, setLoadingWishlist] = useState(false);

  const [loggedIn, setLoggedIn] = useState(localStorage.getItem("logged_in"));

  useEffect(() => {
    let cachedWishlist = JSON.parse(localStorage.getItem("wishlist"));
    if (!cachedWishlist.length) {
      setLoadingWishlist(true);
      apiActions.get_wishlist(
        {},
        () => {
          setLoadingWishlist(false);
          wishlist = JSON.parse(localStorage.getItem("wishlist"));
          designs = JSON.parse(localStorage.getItem("designs"));
        },
        (err) => {
          setLoadingWishlist(false);
        }
      );
    }
  }, []);

  return (
    <div className="wishlist-modal-wrapper">
      <CancelIcon
        className="close-modal-wishlist"
        onClick={() => props.setShowWishlistModal(false)}
      />
      <center>
        <div className="wishlist-modal-wrapper">
          <div className="wishlist-modal">
            <div className="wishlist-modal-header">
              <h6 style={{ font: "normal normal bold 18px/28px Encode Sans" }}>
                <b>
                  My Wishlist{" "}
                  {loggedIn ? (
                    <span>({wishlist.length - removedDesigns.length})</span>
                  ) : null}
                </b>
              </h6>
            </div>

            <div className="wishlist-items-wrapper">
              {loggedIn ? (
                designs ? (
                  !loadingWishlist ? (
                    wishlist.map((id) =>
                      designs[id] && removedDesigns.indexOf(id) == -1 ? (
                        <div className="wishlist-item row">
                          <div
                            className="wishlist-item-image col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 "
                            style={{
                              background: `url(${designs[id].image_thumbnail})`,
                            }}
                          />
                          <div className="wishlist-item-name col-9 col-sm-9 col-md-9 col-lg-9 col-xl-9 ">
                            <b>
                              {id} | {designs[id].design_name}
                            </b>
                          </div>
                          <CancelIcon
                            className="remove-wishlist-item"
                            onClick={() => {
                              let copyRemovedDesigns = Object.assign(
                                [],
                                removedDesigns
                              );
                              copyRemovedDesigns.push(id);
                              setRemovedDesigns(copyRemovedDesigns);
                              apiActions.remove_interaction(
                                id,
                                "shortlisted",
                                {},
                                (res) =>
                                  setTimeout(
                                    () => apiActions.get_wishlist(),
                                    4000
                                  ),
                                () =>
                                  console.log("failed to remove from wishlist")
                              );
                            }}
                          />
                        </div>
                      ) : null
                    )
                  ) : (
                    <div style={{ fontSize: "13px" }}>Loading...</div>
                  )
                ) : null
              ) : (
                <h6 style={{ marginTop: "4vh" }}>
                  You aren't logged in. Please login to see your wishlist.
                </h6>
              )}
            </div>

            {wishlist.length > 1 && (
              <div className="wishlist-modal-footer">
                <center>
                  <Button
                    className="compare-btn"
                    onClick={() => {
                      props.setShowWishlistModal(false);
                      history.push("/compare");
                    }}
                  >
                    COMPARE
                  </Button>
                </center>
              </div>
            )}
          </div>
        </div>
      </center>
    </div>
  );
};

export default Wishlist;
