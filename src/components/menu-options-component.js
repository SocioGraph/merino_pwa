import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { makeStyles } from "@material-ui/styles";
import React, { useState } from "react";
import { useThemeSelection } from "../hooks/theme-hook";
import history from "../util/history-util";
import AuthPopup from "./auth-popup";
import CancelIcon from "@material-ui/icons/Cancel";

const useSettingsStyles = makeStyles({
  root: {
    height: "auto",
    marginTop: "3rem",
    width: 250,
  },
  listItem: {
    borderBottom: "2px solid #00000029",
  },
});

export const MenuOptions = (props) => {
  const { currentThemeSelection, handleThemeToggle } = useThemeSelection();
  const { root, listItem } = useSettingsStyles();
  const [showAuthModal, setShowAuthModal] = useState(false);

  const [loggedIn, setLoggedIn] = useState(localStorage.getItem("logged_in"));
  const [logoutPopup, setLogoutPopup] = useState(false);

  const getLogoutPopup = () => {
    return (
      <div className="popup-overlay">
        <div className="coming-soon-modal-wrapper">
          <CancelIcon
            className="close-modal-coming-soon"
            onClick={() => null}
          />
          <center>
            <div className="coming-soon-modal">
              <h6 style={{ font: "normal normal bold 18px/28px Encode Sans" }}>
                Are you sure you want to logout?
              </h6>

              <center style={{ display: "inline-flex" }}>
                <h5
                  style={{
                    backgroundColor: "#656565",
                    borderRadius: "40px",
                    color: "white",
                    font: "normal normal bold 15px/18px Encode Sans",
                    marginTop: "1vh",
                    paddingTop: "0.6vh",
                    paddingBottom: "0.6vh",
                    width: "20vw",
                  }}
                  onClick={() => {
                    setLogoutPopup(false);
                  }}
                >
                  No
                </h5>

                <h5
                  style={{
                    backgroundColor: "#656565",
                    borderRadius: "40px",
                    color: "white",
                    font: "normal normal bold 15px/18px Encode Sans",
                    marginTop: "1vh",
                    paddingTop: "0.6vh",
                    paddingBottom: "0.6vh",
                    width: "20vw",
                    marginLeft: "1vw",
                  }}
                  onClick={() => {
                    setLoggedIn(false);
                    localStorage.removeItem("logged_in");
                    localStorage.setItem("wishlist", JSON.stringify([]));
                    setLogoutPopup(false);
                  }}
                >
                  Yes
                </h5>
              </center>
            </div>
          </center>
        </div>
      </div>
    );
  };

  return (
    <div
      className="drawer-wrapper"
      style={{
        backgroundColor: `#${
          currentThemeSelection === "lite" ? "f3f3f3" : "3c3599"
        }`,
        fontFamily: "Encode Sans, Expanded Bold",
      }}
    >
      <List
        className="drawer"
        // subheader={
        //   <ListSubheader
        //     style={{
        //       color: `#${currentThemeSelection === "lite" ? "5b5b5b" : "fff"}`,
        //       // fontWeight: 700,
        //     }}
        //   >
        //     <h5 style={{ fontFamily: "Encode Sans, Expanded Bold" }}>MENU</h5>
        //   </ListSubheader>
        // }
        style={{
          color: `#${currentThemeSelection === "lite" ? "5b5b5b" : "fff"}`,
          fontWeight: 700,
          transform: "unset !important",
        }}
      >
        {/* <ListItem className={listItem}>
          <h5>MENU</h5>
        </ListItem> */}

        <ListItem className={listItem} onClick={() => history.push("/home")}>
          <h6
            style={{
              color: history.location.pathname == "/home" ? "red" : "#44444e",
            }}
            onClick={() => props.closeSettingsDrawer()}
          >
            HOME
          </h6>
        </ListItem>
        <ListItem className={listItem}>
          <h6 onClick={() => props.closeSettingsDrawer()}>
            <a
              rel="noopener noreferrer"
              href="https://www.merinolaminates.com/en/about-us/"
              target="_blank"
              style={{ color: "#44444e" }}
            >
              ABOUT
            </a>
          </h6>
        </ListItem>
        <ListItem className={listItem}>
          <a
            rel="noopener noreferrer"
            href="https://www.merinolaminates.com/en/environment-sustainability"
            target="_blank"
            style={{ color: "#44444e" }}
          >
            <h6 onClick={() => props.closeSettingsDrawer()}>
              SOCIETY & ENVIRONMENT
            </h6>
          </a>
        </ListItem>
        <ListItem className={listItem}>
          <a
            rel="noopener noreferrer"
            href="https://visualizer.merinolaminates.com"
            target="_blank"
            style={{ color: "#44444e" }}
          >
            <h6 onClick={() => props.closeSettingsDrawer()}>VISUALIZER</h6>
          </a>
        </ListItem>
        <ListItem className={listItem}>
          <a
            rel="noopener noreferrer"
            href="https://www.merinolaminates.com/en/careers"
            target="_blank"
            style={{ color: "#44444e" }}
          >
            <h6 onClick={() => props.closeSettingsDrawer()}>MORE</h6>
          </a>
        </ListItem>

        {!loggedIn ? (
          <ListItem className={listItem} onClick={() => setShowAuthModal(true)}>
            <h6>LOGIN</h6>
          </ListItem>
        ) : (
          <ListItem
            className={listItem}
            onClick={() => {
              // setLoggedIn(false);
              setLogoutPopup(true);
              // localStorage.removeItem("logged_in");
            }}
            style={{ color: "#44444e" }}
          >
            <h6>LOGOUT</h6>
          </ListItem>
        )}
      </List>
      {showAuthModal && (
        <AuthPopup
          hideAuthModal={() => setShowAuthModal(false)}
          setLoggedIn={setLoggedIn}
          closeSettingsDrawer={() => props.closeSettingsDrawer()}
        />
      )}

      {logoutPopup && getLogoutPopup()}
    </div>
  );
};
