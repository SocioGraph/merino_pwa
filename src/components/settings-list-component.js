import List from "@material-ui/core/List";
import { makeStyles } from "@material-ui/styles";
import React from "react";
import { useThemeSelection } from "../hooks/theme-hook";

const useSettingsStyles = makeStyles({
  root: {
    height: "auto",
    // marginTop: "3.5rem",  
    width: 250,
  },
});

export const SettingsList = () => {
  const { currentThemeSelection, handleThemeToggle } = useThemeSelection();
  const { root } = useSettingsStyles();

  return (
    <div
      className={root}
      style={{
        backgroundColor: `#${
          currentThemeSelection === "lite" ? "white" : "3c3599"
        }`,
        fontFamily: "Encode Sans, Expanded Bold",
      }}
    >
      <List
        style={{
          color: `#${currentThemeSelection === "lite" ? "5b5b5b" : "fff"}`,
          fontWeight: 700,
        }}
      ></List>
    </div>
  );
};
