const assistantFlowInsirations = {
  totalQuestions: 5,
  data: [
    {
      type: "component",
      data: {
        text: "Your favourite color?",
        component: "color-component",
      },
    },
    {
      type: "options",
      data: {
        text: "Type of finish you prefer?",
        options: ["suede", "hi-gloss"],
      },
    },
    {
      type: "options",
      data: {
        text: "Type of design you want to explore?",
        options: [
          "woodgrains",
          "solids",
          "patterns",
          "solids",
          "infusio",
          "metallic",
        ],
      },
    },
    {
      type: "options",
      data: {
        text: "Surface you are looking for?",
        options: ["bed", "wardrobe", "shelf", "cabinet", "infusio", "metallic"],
      },
    },
    {
      type: "options",
      data: {
        text: "Awesome! We have some amazing designs for you. Lets dig in ...",
        options: ["Let's explore"],
      },
    },
  ],
};

export default assistantFlowInsirations;
