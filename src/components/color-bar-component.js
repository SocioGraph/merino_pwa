import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import { useThemeSelection } from "../hooks/theme-hook";
import "../styles/home-page.css";
import { apiActions } from "../../server";
import Loader from "../../public/assets/loader1.gif";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import history from "../util/history-util";
import chatbot from "../../public/assets/chatbot.png";
import CancelIcon from "@material-ui/icons/Cancel";
import chatbot1 from "../../public/assets/chatbot1.png";
import { filterCategories, spaceFilterCategories } from "../util/filters";

const ColorComponent = (props) => {
  const { currentThemeSelection } = useThemeSelection();
  const [showSignUpModal, setShowSignUpModal] = useState(false);

  const colorsArr = filterCategories.colour;

  const [designs, setDesigns] = useState([]);
  const [loadingDesigns, setLoadingDesigns] = useState(false);

  const [showAssistantPopupFirst, setShowAssistantPopupFirst] = useState(true);
  const [showAssistantPopupSecond, setShowAssistantPopupSecond] = useState(
    false
  );

  const [selectedColor, setSelectedColor] = useState({ idx: -1, color: "" });
  const [selectedColorIdx, setSelectedColorIdx] = useState(-1);

  // const replaceColorArr = {
  //   "light-orange": ,
  // }

  const replaceColorArr = {
    white: "#f5f7f9",
    "light-orange": "rgb(253,165,102)",
    "light-yellow": "rgb(255,243,155)",
    "light-green": "rgb(210,255,215)",
    "light-aqua": "rgb(159,255,246)",
    "light-blue": "rgb(154,208,255)",
    "light-indigo": "rgb(169,170,252)",
    "light-purple": "rgb(251,184,236)",
    pink: "rgb(255,210,210)",
    grey: "rgb(135,135,135)",
    orange: "rgb(253,106,0)",
    yellow: "rgb(237,220,94)",
    green: "rgb(11,245,37)",
    aqua: "rgb(0,234,218)",
    blue: "rgb(19,142,249)",
    indigo: "rgb(19,26,253)",
    violet: "rgb(115,9,255)",
    purple: "rgb(208,3,162)",
    red: "rgb(247,0,0)",
    black: "rgb(48,48,48)",
    brown: "rgb(102,48,13)",
    olive: "rgb(78,69,0)",
    "dark-green": "rgb(0,101,11)",
    "dark-aqua": "rgb(0,130,117)",
    "dark-blue": "rgb(1,47,86)",
    "dark-indigo": "rgb(0,4,136)",
    "dark-violet": "rgb(51,1,117)",
    champagne: "rgb(80,0,62)",
  };

  const max = (a, b) => (a > b ? a : b);
  useEffect(() => {}, [selectedColor]);

  const loadDesignsForColor = (colour) => {
    setLoadingDesigns(true);
    apiActions.get_designs(
      { colour },
      (res) => {
        setDesigns(res.data);
        setLoadingDesigns(false);
      },
      (error) => setLoadingDesigns(false)
    );
  };

  const getMetallicIcon = (color) => (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="38px"
      height="55.21px"
      viewBox="0 0 79.6 92"
      style={{ marginLeft: "0.7vw" }}
      onClick={() => {
        setSelectedColor({ idx: 1, color });
        props.setColorSelected(color);
      }}
    >
      {" "}
      <defs>
        {" "}
        <linearGradient id="grad2" x1="0%" y1="0%" x2="0%" y2="100%">
          {" "}
          <stop
            offset="0%"
            style={{ stopColor: "rgb(255, 0, 0)", stopOpacity: 1 }}
          />{" "}
          <stop
            offset="100%"
            // style="stop-color:rgb(255,255,0);stop-opacity:1"
          />{" "}
        </linearGradient>{" "}
      </defs>{" "}
      <path
        id="Path1"
        data-name="Path 1664"
        d="M39.8,0l39.8,23.1l0,44.5L40.2,92L0,68.8l0.1-46L39.8,0z"
        fill="url(#grad2)"
      />{" "}
    </svg>
  );

  console.log("selectedColor", selectedColor);

  const getAbstractIcon = (color) => (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="38px"
      height="55.21px"
      viewBox="0 0 79.6 92"
      style={{ marginLeft: "0.7vw" }}
      onClick={() => {
        setSelectedColor({ idx: 1, color });
        props.setColorSelected(color);
      }}
    >
      {" "}
      <defs>
        {" "}
        <pattern
          id="img1"
          patternUnits="userSpaceOnUse"
          width="100"
          height="100"
        >
          {" "}
          <image
            href="https://www.merinolaminates.com/wp-content/themes/merino/merinoweb-ui/assets/img/pattern.png"
            x="0"
            y="0"
            width="100"
            height="100"
          />{" "}
        </pattern>{" "}
      </defs>{" "}
      <path
        id="Path2"
        data-name="Path 1664"
        d="M39.8,0l39.8,23.1l0,44.5L40.2,92L0,68.8l0.1-46L39.8,0z"
        fill="url(#img1)"
      />{" "}
    </svg>
  );

  return (
    <div
      style={{
        backgroundColor: `#${
          currentThemeSelection === "lite" ? "white" : "3c3599"
        }`,
        height: "auto",

        padding: "0.4rem",
      }}
      className="home-page-categories"
    >
      <div className="color-bar-section">
        {colorsArr.map((color, idx) => (
          <style
            dangerouslySetInnerHTML={{
              __html: [
                `.color-${idx}:after {`,
                `  border-top: 10px solid  ${
                  replaceColorArr[color] ? replaceColorArr[color] : color
                };`,
                `  left: 0;`,
                `}
              .color-${idx}:before {`,
                `  bottom: "100%";`,
                `  transform:  rotate(180deg);`,
                `  border-top: 10px solid  ${
                  replaceColorArr[color] ? replaceColorArr[color] : color
                };`,
                `  left: 0;`,
                `}
              .color-${idx} {`,
                `  background-color: ${
                  replaceColorArr[color] ? replaceColorArr[color] : color
                };`,
                `}`,
              ].join("\n"),
            }}
          />
        ))}

        <div className="color-pallet">
          <div className="first-row">
            {colorsArr.slice(0, 7).map((color, idx) =>
              color == "metallic" ? (
                getMetallicIcon(color)
              ) : color == "abstract" ? (
                getAbstractIcon(color)
              ) : (
                <a href="#exp-finish">
                  <div
                    className={"color color-" + idx}
                    onClick={() => {
                      selectedColor.idx == -1 &&
                        setSelectedColor({ idx, color });
                      props.setColorSelected(color);
                    }}
                  />
                </a>
              )
            )}
          </div>
          <div className="second-row">
            {colorsArr.slice(7, 13).map((color, idx) =>
              color == "metallic" ? (
                getMetallicIcon(color)
              ) : color == "abstract" ? (
                getAbstractIcon(color)
              ) : (
                <a href="#exp-finish">
                  <div
                    className={"color color-" + (idx + 7)}
                    onClick={() => {
                      selectedColor.idx == -1 &&
                        setSelectedColor({ idx: idx + 7, color });
                      props.setColorSelected(color);
                    }}
                  />
                </a>
              )
            )}
          </div>
          <div className="third-row">
            {colorsArr.slice(13, 20).map((color, idx) =>
              color == "metallic" ? (
                getMetallicIcon(color)
              ) : color == "abstract" ? (
                getAbstractIcon(color)
              ) : (
                <a href="#exp-finish">
                  <div
                    className={"color color-" + (idx + 13)}
                    onClick={() => {
                      selectedColor.idx == -1 &&
                        setSelectedColor({ idx: idx + 13, color });
                      props.setColorSelected(color);
                    }}
                  />
                </a>
              )
            )}
          </div>
          <div className="fourth-row">
            {colorsArr.slice(20, 26).map((color, idx) =>
              color == "metallic" ? (
                getMetallicIcon(color)
              ) : color == "abstract" ? (
                getAbstractIcon(color)
              ) : (
                <a href="#exp-finish">
                  <div
                    className={"color color-" + (idx + 20)}
                    onClick={() => {
                      selectedColor.idx == -1 &&
                        setSelectedColor({ idx: idx + 20, color });
                      props.setColorSelected(color);
                    }}
                  />
                </a>
              )
            )}
          </div>

          <div className="fifth-row">
            {colorsArr.slice(26, 29).map((color, idx) =>
              color == "metallic" ? (
                getMetallicIcon(color)
              ) : color == "abstract" ? (
                getAbstractIcon(color)
              ) : (
                <a href="#exp-finish">
                  <div
                    className={"color color-" + (idx + 26)}
                    onClick={() => {
                      selectedColor.idx == -1 &&
                        setSelectedColor({ idx: idx + 26, color });
                      props.setColorSelected(color);
                    }}
                  />
                </a>
              )
            )}
          </div>

          <div className="third-row" />
        </div>

        <br />

        {selectedColor.idx != -1 && (
          <div className="selected-color">
            {selectedColor.color == "metallic" ? (
              getMetallicIcon(selectedColor.color)
            ) : selectedColor.color == "abstract" ? (
              getAbstractIcon(selectedColor.color)
            ) : (
              <div className={"color color-" + selectedColor.idx} />
            )}
          </div>
        )}
      </div>
    </div>
  );
};

export default withRouter(ColorComponent);
