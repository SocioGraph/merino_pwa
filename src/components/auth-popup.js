import React, { useState, useEffect } from "react";
import CancelIcon from "@material-ui/icons/Cancel";
import { apiActions } from "../../server";

const AuthPopup = ({ hideAuthModal, setLoggedIn, closeSettingsDrawer }) => {
  const [selectedAuth, setSelectedAuth] = useState("signup");
  const [loginFields, handleLoginFieldChange] = useState({
    email: "",
    password: "",
  });

  const [signupFields, handleSignupFieldChange] = useState({
    username: "",
    fname: "",
    lname: "",
    email: "",
    password: "",
    repassword: "",
  });

  const [signingUp, setSigningUp] = useState(false);
  const [loggingIn, setLogginIn] = useState(false);

  const [agreeSignupForm, setagreeSignupForm] = useState(false);

  const [showLoginPassword, setShowLoginPassword] = useState(false);
  const [showSignupPassword, setShowSignupPassword] = useState(false);
  const [showSignupCPassword, setShowSignupCPassword] = useState(false);

  const validSignUp = () => {
    return (
      signupFields.username != "" &&
      signupFields.email != "" &&
      signupFields.fname != "" &&
      signupFields.password != "" &&
      signupFields.password == signupFields.repassword &&
      agreeSignupForm
    );
  };

  const validLogIn = () => {
    return loginFields.password != "" && loginFields.email != "";
  };
  const signupHandler = () => {
    if (
      signupFields.username == "" ||
      signupFields.email == "" ||
      signupFields.fname == "" ||
      signupFields.password == "" ||
      signupFields.repassword == ""
    ) {
      alert("Please fill all required fields!");
      return;
    }

    if (signupFields.password != signupFields.repassword) {
      alert("Passwords do not match!");
      return;
    }

    if (!agreeSignupForm) {
      alert("Please agree to terms and conditions!");
      return;
    }

    setSigningUp(true);
    const data = {
      email: signupFields.email,
      password: signupFields.password,
      name: signupFields.fname + " " + signupFields.lname,
      username: signupFields.username,
    };

    apiActions.patch_user(
      data,
      (res) => {
        setSigningUp(false);
        if (res.signed_up) setSelectedAuth("login");
      },
      (e) => {
        setSigningUp(false);
        alert(e);
      }
    );
  };

  const loginHandler = () => {
    setLogginIn(true);
    apiActions.login(
      loginFields.email,
      loginFields.password,
      (res) => {
        setLogginIn(false);
        setLoggedIn(true);
        if (closeSettingsDrawer) {
          closeSettingsDrawer();
        }
        hideAuthModal();

        // get wishlist for the user
        apiActions.get_wishlist(
          {},
          () => {},
          () => {}
        );
      },
      (e) => {
        console.log(e);
        setLogginIn(false);
        alert("Username or password not correct!");
      }
    );
  };

  return (
    <div className="popup-overlay">
      <div className="auth-modal-wrapper">
        <CancelIcon
          className="close-modal-auth"
          onClick={() => {
            hideAuthModal();
          }}
        />
        <center>
          <div className="auth-modal">
            <div className="auth-modal-header">
              <div className="row">
                <div
                  className={
                    selectedAuth == "login"
                      ? "col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 selected-auth"
                      : "col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 "
                  }
                  style={{
                    paddingBottom: "1vh",
                    paddingTop: "1vh",
                    cursor: "pointer",
                  }}
                  onClick={() =>
                    selectedAuth == "signup" ? setSelectedAuth("login") : null
                  }
                >
                  Login
                </div>

                <div
                  className={
                    selectedAuth == "signup"
                      ? "col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 selected-auth"
                      : "col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 "
                  }
                  style={{
                    paddingBottom: "1vh",
                    paddingTop: "1vh",
                    cursor: "pointer",
                  }}
                  onClick={() =>
                    selectedAuth == "login" ? setSelectedAuth("signup") : null
                  }
                >
                  Signup
                </div>
              </div>
            </div>

            <div className="auth-modal-body">
              {selectedAuth == "login" ? (
                <>
                  <div class="container">
                    <input
                      type="text"
                      placeholder="Username"
                      value={loginFields.email}
                      onChange={(e) => {
                        const email = e.target.value.trim().toLowerCase();
                        handleLoginFieldChange({ ...loginFields, email });
                      }}
                      // fname="uname"
                      required
                    />

                    <input
                      type={showLoginPassword ? "text" : "password"}
                      value={loginFields.password}
                      onChange={(e) => {
                        const password = e.target.value.trim();
                        handleLoginFieldChange({ ...loginFields, password });
                      }}
                      placeholder="Password"
                      fname="psw"
                      required
                    />

                    {showLoginPassword ? (
                      <i
                        class="far fa-eye-slash"
                        id="togglePassword"
                        style={{ marginLeft: "-22px" }}
                        onClick={() => setShowLoginPassword(false)}
                      ></i>
                    ) : (
                      <i
                        class="far fa-eye"
                        id="togglePassword"
                        style={{ marginLeft: "-22px" }}
                        onClick={() => setShowLoginPassword(true)}
                      ></i>
                    )}

                    <div
                      onClick={() =>
                        !loggingIn
                          ? validLogIn()
                            ? loginHandler()
                            : alert("Please fill all required fields!")
                          : null
                      }
                      className={
                        loggingIn
                          ? "progress-btn active "
                          : validLogIn()
                          ? "progress-btn"
                          : "disabled progress-btn"
                      }
                      data-progress-style="indefinite"
                    >
                      <div className="btn"> Login</div>
                      <div class="progress" />
                    </div>

                    <label style={{ fontWeight: 500, fontSize: "14px" }}>
                      <input type="checkbox" fname="remember" /> Remember me
                    </label>
                  </div>
                </>
              ) : (
                <>
                  {" "}
                  <input
                    type="text"
                    placeholder="Username*"
                    fname="uname"
                    required
                    value={signupFields.username}
                    onChange={(e) => {
                      const username = e.target.value.trim().toLowerCase();
                      handleSignupFieldChange({ ...signupFields, username });
                    }}
                  />
                  <input
                    type="text"
                    placeholder="First Name*"
                    fname="uname"
                    required
                    value={signupFields.fname}
                    onChange={(e) => {
                      const fname = e.target.value.trim();
                      handleSignupFieldChange({ ...signupFields, fname });
                    }}
                  />
                  <input
                    type="text"
                    placeholder="Last Name"
                    fname="uname"
                    required
                    value={signupFields.lname}
                    onChange={(e) => {
                      const lname = e.target.value.trim();
                      handleSignupFieldChange({ ...signupFields, lname });
                    }}
                  />
                  <input
                    type="email"
                    placeholder="Email*"
                    fname="uname"
                    required
                    value={signupFields.email}
                    onChange={(e) => {
                      const email = e.target.value.trim().toLowerCase();
                      handleSignupFieldChange({ ...signupFields, email });
                    }}
                  />
                  <input
                    type={showSignupPassword ? "text" : "password"}
                    placeholder="Password*"
                    fname="psw"
                    required
                    value={signupFields.password}
                    onChange={(e) => {
                      const password = e.target.value.trim();
                      handleSignupFieldChange({ ...signupFields, password });
                    }}
                  />
                  {showSignupPassword ? (
                    <i
                      class="far fa-eye-slash"
                      id="togglePassword"
                      style={{ marginLeft: "-25px" }}
                      onClick={() => setShowSignupPassword(false)}
                    ></i>
                  ) : (
                    <i
                      class="far fa-eye"
                      id="togglePassword"
                      style={{ marginLeft: "-25px" }}
                      onClick={() => setShowSignupPassword(true)}
                    ></i>
                  )}
                  <input
                    type={showSignupCPassword ? "text" : "password"}
                    placeholder="Confirm Password*"
                    fname="psw"
                    required
                    value={signupFields.repassword}
                    onChange={(e) => {
                      const repassword = e.target.value.trim();
                      handleSignupFieldChange({ ...signupFields, repassword });
                    }}
                  />
                  {showSignupCPassword ? (
                    <i
                      class="far fa-eye-slash"
                      id="togglePassword"
                      style={{ marginLeft: "-25px" }}
                      onClick={() => setShowSignupCPassword(false)}
                    ></i>
                  ) : (
                    <i
                      class="far fa-eye"
                      id="togglePassword"
                      style={{ marginLeft: "-25px" }}
                      onClick={() => setShowSignupCPassword(true)}
                    ></i>
                  )}
                  <label style={{ fontWeight: 500, fontSize: "14px" }}>
                    <input
                      type="checkbox"
                      checked={agreeSignupForm}
                      fname="remember"
                      // onChange={(e) => {
                      //   alert(e.target.value.trim().toLowerCase());
                      //   // handleSignupFieldChange({ ...signupFields, repassword });
                      // }}

                      onClick={() => setagreeSignupForm(!agreeSignupForm)}
                    />{" "}
                    I agree with terms of use.
                  </label>
                  <div
                    onClick={() => (!signingUp ? signupHandler() : null)}
                    className={
                      signingUp
                        ? "progress-btn active "
                        : validSignUp()
                        ? "progress-btn"
                        : "disabled progress-btn"
                    }
                    data-progress-style="indefinite"
                  >
                    <div className="btn"> Create Account</div>
                    <div class="progress" />
                  </div>
                  {/* <button
                          type="submit"
                         
                        >
                         
                        </button> */}
                </>
              )}
            </div>
          </div>
        </center>
      </div>
    </div>
  );
};

export default AuthPopup;
