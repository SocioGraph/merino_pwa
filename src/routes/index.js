import React from "react";
import { Route, Router, Switch, Redirect } from "react-router";
import { Layout } from "../components/layout-component";
import PrivateRoute from "./PrivateRoute";
import history from "../util/history-util";
import Home from "../pages/home-page";
import SplashScreens from "../pages/splash-screens-page";
import SearchPage from "../pages/search-page";
import DesignPage from "../pages/design-page";
import CompareDesignsPage from "../pages/compare-designs-page";
import SpacePage from "../pages/space-page";
import SpacePageFirst from "../pages/space-page-first";
import SpacePageSecond from "../pages/space-page-second";

import FurniturePage from "../pages/furniture-page";
import FurniturePageFirst from "../pages/furniture-page-first";
import FurniturePageSecond from "../pages/furniture-page-second";

import ExplorationPage from "../pages/exploration-page";

import ColorPage from "../pages/color-page";
import ColorPageSecond from "../pages/color-page-second";

// Product Category
import ProductCategory from "../pages/Product-Category/product-category-page";
import ProductCategories from "../pages/Product-Category/product-categories-page";
import ProductCategoryList from "../pages/Product-Category/product-category-list-page";
import ProductDetails from "../pages/Product-Category/product-details-page";

// Laminates Design
import LaminatesDesign from "../pages/Laminates-Design/laminates-design-page";
import LaminatesDesignCategory from "../pages/Laminates-Design/laminates-design-category-page";
import LaminatesDesignDetails from "../pages/Laminates-Design/laminates-design-details-page";

// Search Laminates
import SearchLaminates from "../pages/Search-Laminates/search-laminates-page";
import SearchLaminatesDetails from "../pages/Search-Laminates/search-laminates-details-page";
import SearchPageSecond from "../pages/Search-Laminates/search-page-second";

export const Routes = () => (
  <Router history={history}>
    <Layout>
      <Switch>
        <Route exact path="/" component={SplashScreens} />
        <PrivateRoute exact path="/home" component={Home} />
        <PrivateRoute exact path="/search" component={SearchPage} />
        <PrivateRoute exact path="/design" component={DesignPage} />
        <PrivateRoute exact path="/space" component={SpacePage} />
        <PrivateRoute exact path="/compare" component={CompareDesignsPage} />
        <PrivateRoute exact path="/space-first" component={SpacePageFirst} />
        <PrivateRoute exact path="/space-second" component={SpacePageSecond} />
        <PrivateRoute path="/explore" component={ExplorationPage} />

        <PrivateRoute exact path="/furniture" component={FurniturePage} />
        <PrivateRoute
          exact
          path="/furniture-first"
          component={FurniturePageFirst}
        />
        <PrivateRoute
          exact
          path="/furniture-second"
          component={FurniturePageSecond}
        />

        <PrivateRoute exact path="/color" component={ColorPage} />
        <PrivateRoute exact path="/color-second" component={ColorPageSecond} />

        <PrivateRoute exact path="/explore" component={ExplorationPage} />

        <PrivateRoute
          exact
          path="/product-category"
          component={ProductCategory}
        />
        <PrivateRoute
          exact
          path="/product-categories"
          component={ProductCategories}
        />
        <PrivateRoute
          exact
          path="/product-category-list"
          component={ProductCategoryList}
        />
        <PrivateRoute
          exact
          path="/product-details"
          component={ProductDetails}
        />

        <PrivateRoute
          exact
          path="/laminates-design"
          component={LaminatesDesign}
        />
        <PrivateRoute
          exact
          path="/laminates-design-category"
          component={LaminatesDesignCategory}
        />
        <PrivateRoute
          exact
          path="/laminates-design-details"
          component={LaminatesDesignDetails}
        />

        <PrivateRoute
          exact
          path="/search-laminates"
          component={SearchLaminates}
        />
        <PrivateRoute
          exact
          path="/search-laminates-details"
          component={SearchLaminatesDetails}
        />
        <PrivateRoute
          exact
          path="/search-page-second"
          component={SearchPageSecond}
        />

        <Route exact path="/*" component={Home} />
      </Switch>
    </Layout>
  </Router>
);
