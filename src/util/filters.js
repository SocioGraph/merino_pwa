export const filterCategories = {
  product_names: [
    { Laminates: ["Merinolam"] },
    {
      "Special Laminates": [
        "Imagino",
        "Infusio",
        "Metalam",
        "Finguard",
        "Unicolour",
        "Laminature",
        "Tuff Gloss MR+",
      ],
    },
    { "Performance Laminates": ["FR+ (Fire Retardant)"] },
    {
      Compact: ["Armour (External Wall Cladding)"],
    },
    {
      Panels: ["Matt Meister", "Gloss Meister"],
    },
    { "Solid Surfaces": ["Merino Hanex"] },
  ],
  colour: [
    "white",
    "light-orange",
    "light-yellow",
    "light-green",
    "light-aqua",
    "light-blue",
    "light-indigo",

    "pink",
    "grey",
    "orange",
    "yellow",
    "green",
    "aqua",

    "blue",
    "indigo",
    "violet",
    "purple",
    "red",
    "metallic",
    "black",

    "brown",
    "olive",
    "dark-green",
    "dark-aqua",
    "dark-blue",
    "dark-indigo",

    "dark-violet",
    "champagne",
    "abstract",
  ],
  design_category: [
    "Woodgrains",
    "Solids",
    "Stones",
    "Patterns",
    "Metallic",
    "Infusio",
  ],
  structure: [
    "NA",
    "Hue",
    "Stone",
    "stone",
    "Fabric",
    "Floral",
    "Cambric",
    "Gumtree",
    "Rainbow",
    "Sawcuts",
    "Sparkle",
    "Concrete",
    "Metallic",
    "Geometric",
    "Uni metal",
    "Woodburst",
    "Book Match",
    "Full Crown",
    "Half Crown",
    "High Gloss",
    "Monochrome",
    "Charred Wood",
    "Vertical Lines",
    "Horizontal lines",
  ],
  species: [
    "NA",
    "Elm",
    "Oak",
    "Red",
    "Blue",
    "Grey",
    "Pine",
    "Pink",
    "Teak",
    "Wood",
    "Beech",
    "Black",
    "Brown",
    "Ebony",
    "Green",
    "Maple",
    "Slate",
    "Stone",
    "Straw",
    "Wenge",
    "White",
    "stone",
    "Acacia",
    "Balsam",
    "Bamboo",
    "Cherry",
    "Exotic",
    "Fabric",
    "Floral",
    "Linosa",
    "Marble",
    "Orange",
    "Walnut",
    "Yellow",
    "Ashwood",
    "Granite",
    "Koawood",
    "Sapelle",
    "Abstract",
    "Chestnut",
    "Macassar",
    "Mahagony",
    "Metallic",
    "Pearwood",
    "Terrazzo",
    "Bookmatch",
    "Wood Bark",
    "Recon wood",
    "Dusk Series",
    "Oxide Metal",
  ],
  finish_type: ["MR", "Suede", "Hi-Gloss", "Metallic"],
};

export const spaceFilterCategories = {
  Bedroom: ["Bed", "Wall Cladding", "Wardrobe"],
  Bathroom: [
    "Vanity Counter",
    "Shower Enclosure",
    "Bathroom Wall Cladding",
    "Batten Cladding",
  ],
  "Small Office": [
    "Wall Cladding Office",
    "Office Table Top",
    "Batten Ceiling",
    "Entrance Batten Cladding",
  ],
  Kitchen: ["Wall Cladding", "Shelf", "Cabinet"],
  "Living Room": [
    "Dining Ceiling Cladding",
    "Dining Wall Cladding",
    "Tv Wall Cladding",
    "Coffee Table Top",
  ],
  "Exterior ": [
    "Front Facade",
    "Vertical Cladding",
    "Wooden Log",
    "Exterior Perforation",
  ],
  "Classic Office": [
    "Cafe Table Top",
    "Cafe Wall Cladding",
    "Meeting Area Centre Table",
    "Meeting Area Wall Cladding",
  ],
};

const spaceFurnitureCategories = {
  Bedroom: {
    "Internal Cladding": {
      bed_wall_cladding: 1,
    },
    "Wardrobe Fascia Panel": {
      bed_wardrobe: 1,
    },
  },
  Kitchen: {
    "Wall Cladding/Internal Cladding": {
      wall_cladding: 1,
    },
    "Overhead Shelves, Overhead Cabinets, Under Counter Cabinets, Island Unit": {
      cabinet_shelf: 1,
    },
  },
  "Classic Office": {
    "Table Top, Wall Cladding": {
      cafe_table_top_cafe_wall_cladding: 1,
    },
    "Center Table, Wall Cladding": {
      round_centre_table_seating_wall_cladding: 1,
    },
  },
  "Living Room": {
    "Wall Cladding, False Ceiling": {
      dining_wall_cladding_dining_ceiling_cladding: 1,
    },
    "Coffee Table, Wall Cladding": {
      coffee_table_top_tv_wall_cladding: 1,
    },
  },
  Bathroom: {
    "Shower Enclosure, Vanity Counter": {
      shower_enclosure_vanity_counter: 1,
    },
    "Wall cladding, Batten Cladding": {
      bathroom_wall_cladding_batten_cladding: 1,
    },
    "Vanity Counter, Wall Cladding": {
      vanity_counter_bathroom_wall_cladding: 1,
    },
  },
  "Small Office": {
    "Workstation Table, Wall Cladding": {
      office_table_top_wall_cladding_office: 1,
    },
    "False Ceiling, Wall Cladding": {
      batten_ceiling_entrance_batten_cladding: 1,
    },
  },
  "Exterior ": {
    "Facade Cladding": {
      front_facade_vertical_cladding: 1,
    },
    "Laser-cut Cladding, External Cladding": {
      exterior_perforation_wooden_log: 1,
    },
  },
};

let spaceFurnitureCategoriesObj = {};

for (const property in spaceFurnitureCategories) {
  const obj = spaceFurnitureCategories[property];
  spaceFurnitureCategoriesObj[property] = {};
  for (const p in obj) {
    const sceneIDObj = obj[p];
    const keys = Object.keys(sceneIDObj);
    const sceneMap = {};
    sceneMap[p] = keys[0];
    spaceFurnitureCategoriesObj[property][p] = keys[0];
  }
}

export const cachedSpaceFurnitureCategoriesObj = spaceFurnitureCategoriesObj;
