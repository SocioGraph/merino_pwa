import { makeStyles } from "@material-ui/styles";

export const useBtnStyles = makeStyles({
  btnGroup: {
    justifyContent: "center",
    marginTop: "0.5rem",
    display: "flex",
  },
});
