import $ from "jquery";

// export default { signup };
export const apiActions = {
  signup,
  get_inspirations,
  get_design,
  get_designs,
  get_design_family,
  get_similar_designs,
  get_options,
  patch_user,
  login,
  create_interaction,
  get_wishlist,
  remove_interaction,
  get_scenes,
  get_products,
  get_furnitures,
  get_assistant_flow_inspirations,
};

var DAVE_SETTINGS = {
  BASE_URL: "https://dashboard.iamdave.ai",
  SIGNUP_API_KEY: "bWVyaW5vIGxhbWluYXRlczE1ODM0NzQ3NDYgMTE_",
  ENTERPRISE_ID: "merino_laminates",
  USER_MODEL: "customer",
  USER_ID: "customer_id",
  INTERACTION_MODEL: "design_view",
  PRODUCT_MODEL: "design",
  CATEGORY_MODEL: "product",
  PRODUCT_ID: "design_id",
  PRODUCT: {},
  DEFAULT_USER_DATA: {
    customer_type: ["PWA"],
    validated: true,
  },
};

function signup(data, callbackFunc, errorFunc) {
  var signupurl =
    DAVE_SETTINGS.BASE_URL + "/customer-signup/" + DAVE_SETTINGS.USER_MODEL;
  //Password string generator
  var randomstring = Math.random()
    .toString(36)
    .slice(-8);
  data = data || {};
  if (!data.email) {
    data.email = "ananth+" + randomstring + "@iamdave.ai";
  }
  for (let k in DAVE_SETTINGS.DEFAULT_USER_DATA) {
    if (!data[k]) {
      data[k] = DAVE_SETTINGS.DEFAULT_USER_DATA[k];
    }
  }

  $.ajax({
    url: signupurl,
    method: "POST",
    dataType: "json",
    contentType: "json",
    withCredentials: true,
    headers: {
      "Content-Type": "application/json",
      "X-I2CE-ENTERPRISE-ID": DAVE_SETTINGS.ENTERPRISE_ID,
      "X-I2CE-SIGNUP-API-KEY": DAVE_SETTINGS.SIGNUP_API_KEY,
    },
    data: JSON.stringify(data),
    success: function(data) {
      let HEADERS = {
        "Content-Type": "application/json",
        "X-I2CE-ENTERPRISE-ID": DAVE_SETTINGS.ENTERPRISE_ID,
        "X-I2CE-USER-ID": data[DAVE_SETTINGS.USER_ID],
        "X-I2CE-API-KEY": data.api_key,
      };
      setCookie("authentication", JSON.stringify(HEADERS), 24);
      setCookie("user_id", data[DAVE_SETTINGS.USER_ID]);
      setCookie("wishlist", []);
      setCookie("likes", []);
      if (callbackFunc) {
        callbackFunc(data);
      }
      let data1 = {
        location: "{agent_info.ip}",
        browser: "{agent_info.browser}",
        os: "{agent_info.os}",
        device_type: "{agent_info.device}",
        _async: true,
      };
      let params = get_url_params();
      for (let k in ["utm_source", "utm_medium", "utm_campaign", "utm_term"]) {
        if (params[k]) {
          data1[k] = params[k];
        }
      }
      patch_user(data1, function(data2) {
        set_international(data2);
      });
    },
    error: function(r, e) {
      console.log(e);
      if (errorFunc) {
        errorFunc(r, e);
      }
    },
  });
}

// Login function
function login(user_name, password, callbackFunc, errorFunc) {
  let params = {};
  params["user_id"] = user_name;
  params["password"] = password;
  params["roles"] = "customer";
  params["attrs"] = ["username", "email", "customer_id"];
  params["enterprise_id"] = DAVE_SETTINGS.ENTERPRISE_ID;
  ajaxRequestWithData(
    "/dave/oauth",
    "POST",
    JSON.stringify(params),
    function(data) {
      let HEADERS = {
        "Content-Type": "application/json",
        "X-I2CE-ENTERPRISE-ID": DAVE_SETTINGS.ENTERPRISE_ID,
        "X-I2CE-USER-ID": data.user_id,
        "X-I2CE-API-KEY": data.api_key,
      };
      setCookie("authentication", JSON.stringify(HEADERS), 24);
      setCookie("user_id", data["user_id"]);
      setCookie("logged_in", true);
      set_international(data);
      callbackFunc(data);
      // get_wishlist({}.callbackFunc, errorFunc);
    },
    errorFunc,
    () => console.log("user unauthorized!")
  );
}

// You can get the column names of any model with the following API
// ajaxRequestWithData("/attributes/<model_name>/name", "GET",
//
// When the customer signs up, add the params, name, username, password, email, and phone_number attributes, and update the customer
function patch_user(params, callbackFunc, errorFunc) {
  // e.g. patch_user({"pincode": <pincode>, "name": <name of user>, "company_name": <company_name>}, function(data) {console.log(data)});
  // In the response you will get the warehouse_id
  let user_id = getCookie("user_id");

  check_user(params, (res) => {
    if (res) {
      errorFunc("User already exists");
    } else {
      //  params = params || {}
      if (params.name && params.email && params.phone_number) {
        params["signed_up"] = true;
        setCookie("logged_in", true);
      }
      ajaxRequestWithData(
        "/object/" + DAVE_SETTINGS.USER_MODEL + "/" + user_id,
        "PATCH",
        JSON.stringify(params),
        function(data) {
          for (let k in ["name", "email", "phone_number", "customer_id"]) {
            if (data[k]) {
              setCookie("customer_" + k, data[k]);
            }
          }
          if (callbackFunc) {
            callbackFunc(data);
          }
        },
        (e) => {
          console.log("e=>", e);
          errorFunc(e.responseText);
        }
      );
    }
  });
}

// params can include
// products (in case product is selected in the filter)
// finish_type (based on which finish type is selected)
// rooms ( in case room is selected )
// surfaces ( in case surface is selected )
function create_interaction(
  design_id,
  stage,
  params,
  callbackFunc,
  errorFunc,
  repeats
) {
  do_interaction(
    design_id,
    stage,
    "create",
    params,
    callbackFunc,
    errorFunc,
    repeats
  );
}

function remove_interaction(
  design_id,
  stage,
  params,
  callbackFunc,
  errorFunc,
  repeats
) {
  do_interaction(
    design_id,
    stage,
    "delete",
    params,
    callbackFunc,
    errorFunc,
    repeats
  );
}

function do_interaction(
  design_id,
  stage,
  action,
  params,
  callbackFunc,
  errorFunc,
  repeats
) {
  if (
    !signup_guard(do_interaction, [
      design_id,
      stage,
      ,
      params,
      callbackFunc,
      errorFunc,
      repeats || 0,
    ])
  ) {
    return;
  }
  let STAGES = {
    details: "details",
    liked: "like",
    shortlisted: "shortlist",
    downloaded: "download",
    shared: "share",
    viewed: "view",
  };
  let user_id = getCookie("user_id");
  stage = stage || "shortlisted";
  if (!params) params = {};
  //  params = params || {}
  params[DAVE_SETTINGS.USER_ID] = user_id;
  params[DAVE_SETTINGS.PRODUCT_ID] = design_id;
  params["viewed"] = 1;
  params["_async"] = true;
  if (getCookie("room_id")) {
    params["rooms"] = getCookie("room_id");
  }
  if (getCookie("product_id")) {
    params["products"] = getCookie("product_id");
  }
  if (getCookie("object_id")) {
    params["surfaces"] = getCookie("object_id");
  }
  if (
    ["details", "liked", "shortlisted", "downloaded", "shared"].indexOf(stage)
  ) {
    params[stage] = action == "delete" ? false : true;
  }
  ajaxRequestWithData(
    "/object/" + DAVE_SETTINGS.INTERACTION_MODEL,
    "POST",
    JSON.stringify(params),
    function(data) {
      if (stage == "shortlisted" || stage == "liked") {
        let wishlist = getCookie(stage == "shortlisted" ? "wishlist" : "likes");
        let i = wishlist.indexOf(design_id);
        if (action == "delete" && i >= 0) {
          wishlist.splice(i, 1);
        } else if (i < 0) {
          wishlist.push(design_id);
        }
        setCookie(stage == "shortlisted" ? "wishlist" : "likes", wishlist);
      }
      if (callbackFunc) {
        callbackFunc(data);
      }
    },
    errorFunc
  );
  if (STAGES[stage]) {
    let p = {};
    p[STAGES[stage]] = 1;
    iupdate_user(p);
    iupdate_design(design_id, stage);
  }
}
function get_wishlist(params, callbackFunc, errorFunc, repeats) {
  if (
    !signup_guard(get_wishlist, [params, callbackFunc, errorFunc, repeats || 0])
  ) {
    return;
  }
  let user_id = getCookie("user_id");
  if (!params) params = {};
  //  params = params || {}
  params[DAVE_SETTINGS.USER_ID] = user_id;
  params["shortlisted"] = true;
  let designs = getCookie("designs");
  ajaxRequestWithData(
    "/objects/" + DAVE_SETTINGS.INTERACTION_MODEL,
    "GET",
    params,
    function(data) {
      let wishlist = [];
      for (let k in data.data) {
        let d = data.data[k];
        if (!designs[d.design_id]) {
          ajaxRequest("/object/design/" + d.design_id, "GET", function(data) {
            let designs = getCookie("designs") || {};
            designs[data.design_id] = data;
            setCookie("designs", designs);
          });
        }
        wishlist.push(d.design_id);
      }
      setCookie("wishlist", wishlist);
      if (callbackFunc) {
        callbackFunc(data);
      }
    },
    errorFunc
  );
}

//
// params like
//  {
//      "product_names": <product_id>
//      "product_categories": <product_category>,
//      "rooms": <room_id>,
//      "surfaces": <surface_id>,
//      "design_category": <design_category>,
//      "species": <species>,
//      "structure": <structure>,
//      "finish_type": <finish_type>,
//      "design_id": <design_id>,
//  }
function get_inspirations(params, callbackFunc, errorFunc, repeats) {
  if (
    !signup_guard(get_inspirations, [
      params,
      callbackFunc,
      errorFunc,
      repeats || 0,
    ])
  ) {
    return;
  }
  //  params = params || {}
  if (!params["_page_number"]) {
    params["_page_number"] = 1;
  }
  params["rendering_image~"] = null;
  if (getCookie("is_international")) {
    params["available_in_international"] = true;
  } else {
    params["available_in_india"] = true;
  }
  ajaxRequestWithData(
    "/objects/rendering",
    "GET",
    params,
    function(data) {
      let inspirations = getCookie("inspirations") || {};
      let designs = getCookie("designs") || {};
      let sp = getCookie("applied_filters", {}).product_names;
      let spli = [];
      for (let k in data.data) {
        let d = data.data[k];
        if (
          sp &&
          (sp.indexOf("Gloss Meister") >= 0 || sp.indexOf("Metalam") >= 0) &&
          d.finish_type == "Suede"
        ) {
          spli.push(k);
        } else if (
          sp &&
          sp.indexOf("Matt Meister") >= 0 &&
          (d.finish_type == "Hi-Gloss" ||
            d.finish_type == "MR" ||
            d.finish_type == "Metallic")
        ) {
          spli.push(k);
        }
        inspirations[d.rendering_id] = d;
        preloadImages([d.rendering_image]);
        get_design(d.design_id);
      }
      for (let k = spli.length - 1; k >= 0; k--) {
        data.data.splice(spli[k], 1);
      }
      // setCookie("inspirations", inspirations);
      if (callbackFunc) {
        callbackFunc(data);
      }
    },
    errorFunc
  );
}
// option can be:
// product_name. product_category, design_category, species, structure, finish_type
// params are the values of the other options selected
function get_options(option, params, callbackFunc, errorFunc, repeats) {
  if (
    !signup_guard(get_options, [
      option,
      params,
      callbackFunc,
      errorFunc,
      repeats || 0,
    ])
  ) {
    return;
  }

  params = params || {};
  if (!params["_page_number"]) {
    params["_page_number"] = 1;
  }
  if (getCookie("is_international")) {
    params["available_in_international"] = true;
  } else {
    params["available_in_india"] = true;
  }
  params["show_in_pwa"] = true;

  ajaxRequestWithData(
    "/unique/product_design/" + option, //      "finish_type": <finish_type>,

    "GET",
    params,
    callbackFunc,
    errorFunc
  );
}

//
// params like
//  {
//      "product_names": <product_id>
//      "product_categories": <product_category>,
//      "rooms": <room_id>,
//      "surfaces": <surface_id>,
//      "design_category": <design_category>,
//      "species": <species>,
//      "structure": <structure>,
//  }
function get_designs(params, callbackFunc, errorFunc, repeats) {
  if (
    !signup_guard(get_designs, [params, callbackFunc, errorFunc, repeats || 0])
  ) {
    return;
  }
  if (!params) {
    params = {};
  }
  //  params = params || {}

  if (params["product_names"]) {
    if (typeof params.product_names == "string") {
      params["product_names"] = toQueryType(params["product_names"]);
    } else {
      for (let k in params["product_names"] || []) {
        params["product_names"][k] = toQueryType(params["product_names"][k]);
      }
    }
  }
  let cd = getCookie("design-id");
  let designs = getCookie("designs") || {};
  let nd = designs[cd];
  let do_insert = false;
  if (nd) {
    do_insert = true;
    for (let k in params) {
      if (k.startsWith("_")) {
        continue;
      }
      if (params[k] == undefined || params[k] == null) {
        continue;
      }
      if (Array.isArray(params[k]) && Array.isArray(nd[k])) {
        if (params[k].length <= 0) {
          continue;
        }
        let match = false;
        for (let k1 in params[k]) {
          if (
            nd[k]
              .map(function(e) {
                return e.toLowerCase();
              })
              .indexOf(params[k][k1].toLowerCase()) >= 0
          ) {
            match = true;
            break;
          }
        }
        if (!match) {
          do_insert = false;
          break;
        }
        continue;
      }
      if (Array.isArray(params[k])) {
        if (params[k].length <= 0) {
          continue;
        }
        if (
          params[k]
            .map(function(e) {
              return e.toLowerCase();
            })
            .indexOf(nd[k].toLowerCase()) < 0
        ) {
          do_insert = false;
          break;
        }
        continue;
      }
      if (Array.isArray(nd[k])) {
        if (
          nd[k]
            .map(function(e) {
              return e.toLowerCase();
            })
            .indexOf(params[k].toLowerCase()) < 0
        ) {
          do_insert = false;
          break;
        }
        continue;
      }
      if (params[k].toLowerCase() != nd[k].toLowerCase()) {
        do_insert = false;
        break;
      }
    }
  }
  setCookie("applied_filters", params);
  if (!params["_page_number"]) {
    params["_page_number"] = 1;
    clearCookie("inserted-design");
  }
  if (getCookie("is_international")) {
    params["available_in_international"] = true;
  } else {
    params["available_in_india"] = true;
  }
  params["show_in_pwa"] = true;
  params["_quick_view"] = "pwa";
  params["_page_size"] = 20;
  params["_sort_by"] = "view";
  params["_sort_reverse"] = true;

  ajaxRequestWithData(
    "/objects/design",
    "GET",
    params,
    function(data) {
      let got_index = -1;
      let id = getCookie("inserted-design") || cd;
      for (let k in data.data) {
        let d = data.data[k];
        if (id && d.design_id == id) {
          got_index = parseInt(k);
        }
        if (
          d.products.indexOf("merino_hanex") >= 0 &&
          !d.design_name.startsWith("Hanex")
        ) {
          d.design_name = "Hanex - " + d.design_name;
        }
        designs[d.design_id] = d;
        preloadImages([d.design_thumbnail]);
      }
      if (got_index >= 0) {
        let gd = data.data.splice(got_index, 1)[0];
        if (params["_page_number"] == 1) {
          data.data.unshift(gd);
        }
      } else if (do_insert && nd && params["_page_number"] == 1) {
        data.data.unshift(nd);
        setCookie("inserted-design", cd);
      }
      if (!getCookie("design-id") && data.data.length > 0) {
        setCookie("design-id", data.data[0].design_id);
      }
      setCookie("designs", designs);
      if (callbackFunc) {
        callbackFunc(data);
      }
    },
    errorFunc
  );
}
function get_furnitures(params, callbackFunc, errorFunc, repeats) {
  if (
    !signup_guard(get_furnitures, [
      params,
      callbackFunc,
      errorFunc,
      repeats || 0,
    ])
  ) {
    return;
  }
  if (!params) {
    params = {};
  }
  //  params = params || {}
  if (!params["_page_number"]) {
    params["_page_number"] = 1;
  }
  params["furniture_image~"] = null;
  ajaxRequestWithData(
    "/objects/furniture",
    "GET",
    params,
    function(data) {
      let furnitures = getCookie("furnitures") || [];
      for (let k in data.data) {
        let d = data.data[k];
        furnitures[k] = d;
        preloadImages([d.furniture_image]);
      }
      setCookie("furnitures", furnitures);
      if (callbackFunc) {
        callbackFunc(data);
      }
    },
    errorFunc
  );
}
//
// params like
//  {
//      "room_name": room_name,
//      "furnitures": furniture_name
//  }
function get_scenes(params, callbackFunc, errorFunc, repeats) {
  if (
    !signup_guard(get_scenes, [params, callbackFunc, errorFunc, repeats || 0])
  ) {
    return;
  }
  if (!params) {
    params = {};
  }
  //  params = params || {}
  if (!params["_page_number"]) {
    params["_page_number"] = 1;
  }
  ajaxRequestWithData(
    "/objects/scene",
    "GET",
    params,
    function(data) {
      let scenes = getCookie("scenes") || {};
      for (let k in data.data) {
        let d = data.data[k];
        scenes[d.scene_id] = d;
        preloadImages([d.rendering_thumbnail]);
      }
      // setCookie("scenes", scenes);
      if (callbackFunc) {
        callbackFunc(data);
      }
    },
    errorFunc
  );
}
//params are like
// {
//      "product_category": <product_category>
// }
function get_products(params, callbackFunc, errorFunc, repeats) {
  if (
    !signup_guard(get_products, [params, callbackFunc, errorFunc, repeats || 0])
  ) {
    return;
  }
  if (!params) {
    params = {};
  }
  //  params = params || {}
  if (!params["_page_number"]) {
    params["_page_number"] = 1;
  }
  if (getCookie("is_international")) {
    params["available_in_international"] = true;
  } else {
    params["available_in_india"] = true;
  }
  params["show_in_pwa"] = true;

  ajaxRequestWithData(
    "/objects/product",
    "GET",
    params,
    function(data) {
      let products = getCookie("products") || {};
      for (let k in data.data) {
        let d = data.data[k];
        if (!products[d.product_category]) {
          products[d.product_category] = [];
        }
        if (
          products[d.product_category].filter(function(v) {
            v["product_id"] == d["product_id"];
          }).length <= 0
        ) {
          products[d.product_category].push(d);
          preloadImages([d.application_image]);
          products[d.product_category] = to_product_sequence(
            products[d.product_category]
          );
        }
      }
      setCookie("products", products);
      if (callbackFunc) {
        callbackFunc(data);
      }
    },
    errorFunc
  );
}

function get_design(design_id, callbackFunc, errorFunc, repeats) {
  if (
    !signup_guard(get_design, [
      design_id,
      callbackFunc,
      errorFunc,
      repeats || 0,
    ])
  )
    return;

  let params = params || {};
  let designs = getCookie("designs") || {};

  if (designs[design_id]) {
    if (callbackFunc) {
      callbackFunc(designs[design_id]);
    }
  } else {
    ajaxRequestWithData(
      "/object/design/" + design_id,
      "GET",
      params,
      function(data) {
        designs[data.design_id] = data;
        preloadImages([data.design_thumbnail, data.design_image]);
        setCookie("design", data);
        setCookie("designs", designs);
        if (callbackFunc) {
          callbackFunc(data);
        }
      },
      errorFunc
    );
  }
}
// When a specific design is selected, send the design data to this function to get family of designs
function get_design_family(design_data, callbackFunc, errorFunc, repeats) {
  if (
    !signup_guard(get_design_family, [
      design_data,
      callbackFunc,
      errorFunc,
      repeats || 0,
    ])
  ) {
    return;
  }
  design_data = JSON.parse(design_data);
  let params = {
    design_category: design_data["design_category"],
    species: design_data["species"],
    "design_id~": design_data["design_id"],
    _page_number: design_data["pageNumber"],
  };

  if (!params["_page_number"]) {
    params["_page_number"] = 1;
  }

  params["_page_size"] = 20;

  if (getCookie("is_international")) {
    params["available_in_international"] = true;
  } else {
    params["available_in_india"] = true;
  }
  params["show_in_pwa"] = true;
  params["_quick_view"] = "pwa";
  params["_page_size"] = 20;
  params["_sort_by"] = "view";
  params["_sort_reverse"] = true;
  ajaxRequestWithData(
    "/objects/design",
    "GET",
    params,
    callbackFunc,
    errorFunc
  );
}

// When a specific design is selected, send the design data to this function to get family of designs
function get_similar_designs(design_data, callbackFunc, errorFunc, repeats) {
  if (
    !signup_guard(get_similar_designs, [
      design_data,
      callbackFunc,
      errorFunc,
      repeats || 0,
    ])
  ) {
    return;
  }
  if (design_data) {
    design_data = JSON.parse(design_data);
    let params = {
      design_category: design_data["design_category"],
      structure: design_data["structure"],
      "design_id~": design_data["design_id"],
      _page_number: design_data["pageNumber"],
    };

    if (!params["_page_number"]) {
      params["_page_number"] = 1;
    }
    params["_page_size"] = 20;

    if (getCookie("is_international")) {
      params["available_in_international"] = true;
    } else {
      params["available_in_india"] = true;
    }
    params["show_in_pwa"] = true;
    params["_quick_view"] = "pwa";
    params["_page_size"] = 20;
    params["_sort_by"] = "view";
    params["_sort_reverse"] = true;
    ajaxRequestWithData(
      "/objects/design",
      "GET",
      params,
      callbackFunc,
      errorFunc
    );
  }
}

// params can be {'phone_number': <given phone number>}  or {'email': <given email>}
// callbackFunc is called with 'true' if user with given value exists
function check_user(params, callbackFunc, errorFunc) {
  // e.g. patch_user({"pincode": <pincode>, "name": <name of user>, "company_name": <company_name>}, function(data) {console.log(data)});
  // In the response you will get the warehouse_id
  //  params = params || {}
  let user_id = getCookie("user_id");

  let updatedParams = {};
  updatedParams.email = params.email;
  updatedParams.username = params.username;
  updatedParams.customer_id = params.username;
  updatedParams.condition_type = "OR";

  ajaxRequestWithData(
    "/objects/" + DAVE_SETTINGS.USER_MODEL,
    "GET",
    updatedParams,
    function(data) {
      if (data.total_number) {
        callbackFunc(true);
      } else {
        callbackFunc(false);
      }
      // if (callbackFunc) {
      //   let str = data.email;
      //   str.replace("@", "%40");

      //   if (
      //     updatedParams.email == data.email ||
      //     updatedParams.username == data.username
      //   ) {
      //     callbackFunc(true);
      //   } else {
      //     callbackFunc(false);
      //   }
      // }
    },
    errorFunc
  );
}
// --------------------------------
// Below this are all internal functions
function iupdate_user(params, callbackFunc, errorFunc) {
  // Used to update session duration and session number
  let user_id = getCookie("user_id");
  //  params = params || {}
  params["_async"] = true;
  ajaxRequestWithData(
    "/iupdate/" + DAVE_SETTINGS.USER_MODEL + "/" + user_id,
    "PATCH",
    JSON.stringify(params),
    callbackFunc,
    errorFunc
  );
}
function iupdate_design(design_id, stage, callbackFunc, errorFunc) {
  // Used to update design statistics
  let params = {};
  params["_async"] = true;
  params[stage] = 1;
  ajaxRequestWithData(
    "/iupdate/" + DAVE_SETTINGS.PRODUCT_MODEL + "/" + design_id,
    "PATCH",
    JSON.stringify(params),
    callbackFunc,
    errorFunc
  );
}

function getCookie(key, _default) {
  let result = window.localStorage.getItem(key);
  return get_value(result, _default);
}

function get_value(value, _default) {
  if (_default === undefined) {
    _default = null;
  }
  if (value === null || value === undefined) {
    return _default;
  }
  try {
    return JSON.parse(value);
  } catch (err) {
    return value;
  }
  return value;
}

function setCookie(key, value, hoursExpire) {
  // console.log(key, value, hoursExpire);
  if (hoursExpire === undefined) {
    hoursExpire = 24;
  }
  if (value === undefined) {
    return;
  }
  if (typeof value == "object") {
    value = JSON.stringify(value);
  }
  if (hoursExpire < 0 || value === null) {
    value = window.localStorage.getItem(key);
    window.localStorage.removeItem(key);
    return value;
  } else {
    window.localStorage.setItem(key, value);
  }
  return value;
}

function clearCookie(key) {
  setCookie(key, null, -24);
}

function Trim(strValue) {
  return strValue.replace(/^\s+|\s+$/g, "");
}

function toTitleCase(str) {
  return str.replace(/_/g, " ").replace(/(?:^|\s)\w/g, function(match) {
    return match.toUpperCase();
  });
}

function generate_random_string(string_length) {
  let random_string = "";
  let random_ascii;
  for (let i = 0; i < string_length; i++) {
    random_ascii = Math.floor(Math.random() * 25 + 97);
    random_string += String.fromCharCode(random_ascii);
  }
  return random_string;
}

function toIdType(str, to_lower) {
  let r = (str || "")
    .replace(/\+/g, "")
    .replace(/\(/g, "")
    .replace(/\)/g, "")
    .replace(/\s/g, "_");
  if (to_lower) {
    return r.toLowerCase();
  }
  return r;
}
function toUrlType(str) {
  return (str || "").replace(/\+/g, "%2B");
}
function toQueryType(str) {
  str = str || "";
  if (str.includes("+")) {
    str = "~" + str.slice(0, str.indexOf("+"));
  }
  if (str.includes(",")) {
    return $.map(str.split(","), function(v) {
      return v.trim();
    });
  }
  return str;
}

function clearCookies() {
  setCookie("authentication", "", -24);
  setCookie("logged_in", "", -24);
  setCookie("applied", "", -24);
  setCookie("shortlisted", "", -24);
  setCookie("shortlisted_per_surface", "", -24);
  setCookie("room_id", "", -24);
  setCookie("product_id", "", -24);
  setCookie("object_id", "", -24);
  setCookie("room_type", "", -24);
  setCookie("viewed", "", -24);
  setCookie("customer_name", "", -24);
  setCookie("customer_email", "", -24);
  setCookie("customer_phone_number", "", -24);
  setCookie("customer_password", "", -24);
  setCookie("customer_city", "", -24);
  setCookie("customer_customer_id", "", -24);
  setCookie("customer_token", "", -24);
}

function get_url_params(qd) {
  qd = qd || {};
  if (location.search)
    location.search
      .substr(1)
      .split("&")
      .forEach(function(item) {
        var s = item.split("="),
          k = s[0],
          v = s[1] && decodeURIComponent(s[1]); //  null-coalescing / short-circuit
        //(k in qd) ? qd[k].push(v) : qd[k] = [v]
        (qd[k] = qd[k] || []).push(v); // null-coalescing / short-circuit
      });
  return qd;
}

function to_sequence(data, sequence, indexor) {
  indexor =
    indexor ||
    function(a, s) {
      return s.indexOf(a);
    };
  let op = [];
  for (let s in sequence) {
    let i = indexor(sequence[s], data);
    if (i >= 0) {
      op.push(data[i]);
    }
  }
  return op;
}

let to_product_sequence = function(data) {
  return to_sequence(
    data,
    [
      "Merinolam",
      "Tuff Gloss MR+",
      "Unicolour",
      "Unicolor",
      "Finguard",
      "Synchro",
      "Laminature",
      "Infusio",
      "Imagino",
      "Metalam",
      "Writeons",
      "AB+",
      "AB+ (Antibacterial Surfaces)",
      "AB+  (Antibacterial Surfaces)",
      "FR+",
      "FR+ (Fire Retardant)",
      "Chem+",
      "ESD+",
      "Standard Compact",
      "Armour EWC Superclad",
      "Armour EWC Film",
      "Armour Pro Coated",
      "Armour Pro Film",
      "Armour EWC (Superclad)",
      "Armour EWC (Film)",
      "Armour EWC - Superclad",
      "Armour EWC - Film",
      "Armour Pro (Coated)",
      "Armour Pro (Film)",
      "Armour Pro - Coated",
      "Armour Pro - Film",
      "Shaurya (Internal Wall Cladding)",
      "Matt Meister",
      "Gloss Meister",
      "Ply Meister",
      "Post Laminated Panel",
      "Merinova Pre Lam",
      "Stoven",
      "Impreza",
      "Impreza (High Performance Ultra-Thin Compact Surfaces)",
      "Impreza unicolor core tops",
      "Merino Hanex",
      "Harmony",
    ],
    function(s, d) {
      return d.findIndex(function(d1) {
        return d1["product_name"] == s;
      });
    }
  );
};

function ajaxRequestSync(
  URL,
  METHOD,
  callbackFunc,
  errorFunc,
  async,
  unauthorized,
  HEADERS
) {
  HEADERS = HEADERS || getCookie("authentication");
  if (!unauthorized) {
    unauthorized = function() {
      signup(
        {},
        ajaxRequestSync(
          URL,
          METHOD,
          callbackFunc,
          errorFunc,
          async,
          function() {
            console.log("Failed after signup");
            alert("Could not signup! Please contact customer support!");
          },
          HEADERS
        )
      );
    };
  }
  $.ajax({
    url: DAVE_SETTINGS.BASE_URL + URL,
    method: (METHOD || "GET").toUpperCase(),
    dataType: "json",
    contentType: "json",
    async: async || false,
    headers: HEADERS,
    statusCode: {
      401: unauthorized,
      404: unauthorized,
    },
  })
    .done(function(data) {
      let result = data;
      if (callbackFunc) {
        callbackFunc(data);
      }
    })
    .fail(function(err) {
      if (errorFunc) {
        errorFunc(err);
      }
    });
}

function ajaxRequest(
  URL,
  METHOD,
  callbackFunc,
  errorFunc,
  unauthorized,
  HEADERS
) {
  return ajaxRequestSync(
    URL,
    METHOD,
    callbackFunc,
    errorFunc,
    true,
    unauthorized,
    HEADERS
  );
}

function ajaxRequestWithData(
  URL,
  METHOD,
  DATA,
  callbackFunc,
  errorFunc,
  unauthorized,
  HEADERS
) {
  HEADERS = HEADERS || getCookie("authentication");
  var defaultData = "";
  if (DATA) {
    defaultData = DATA;
  }
  if (!unauthorized) {
    unauthorized = function() {
      debugger;
      signup(
        {},
        ajaxRequestWithData(
          URL,
          METHOD,
          DATA,
          callbackFunc,
          errorFunc,
          function() {
            console.log("Failed after signup");
            alert("Could not signup! Please contact customer support!");
          },
          HEADERS
        )
      );
    };
  }
  $.ajax({
    url: DAVE_SETTINGS.BASE_URL + URL,
    method: (METHOD || "GET").toUpperCase(),
    dataType: "json",
    contentType: "application/json",
    headers: HEADERS,
    data: defaultData,
    statusCode: {
      401: unauthorized,
      404: unauthorized,
    },
  })
    .done(function(data) {
      if (callbackFunc) {
        callbackFunc(data);
      }
    })
    .fail(function(err) {
      if (errorFunc) {
        errorFunc(err);
      }
    });
}
function signup_guard(func, args) {
  if (!getCookie("authentication")) {
    console.warn(
      "Authentication headers are not set, but trying to run function " +
        func.name
    );
    let repeats = args.slice(-1);
    if (repeats < 3) {
      setTimeout(function() {
        args.push(args.pop() + 1);
        func.apply(args);
      }, 1000 * repeats);
    } else {
      console.error(
        "Stopping trying to run function" +
          func.name +
          " after " +
          repeats +
          "attempts"
      );
    }
    return false;
  }
  return true;
}

let code_to_region = {
  "169": "India",
  "10483": "Rest of the World",
  "10487": "South Africa",
  "10489": "Canada",
  "10491": "Chile",
  "10493": "Egypt",
  "10495": "Germany",
  "10497": "Indonesia",
  "10499": "Italy",
  "10501": "Malaysia",
  "10503": "Mexico",
  "10505": "Netherlands",
  "10507": "Panama",
  "10509": "Peru",
  "10511": "Philippines",
  "10513": "Puerto Rico",
  "10515": "Saudi Arabia",
  "10517": "Taiwan",
  "10519": "Thailand",
  "10521": "United Arab Emirates",
  "10523": "United States",
  "10525": "Vietnam",
  "11379": "Nepal",
};
let region_to_code = {};
for (const k in code_to_region) {
  region_to_code[code_to_region[k]] = k;
}

function set_international(data, skip_flag) {
  data = data || {};
  let params = get_url_params();
  if (params.merino_country_id) {
    data["region"] = code_to_region[params.merino_country_id];
    delete params["country"];
  }
  if (params.merino_country_reference) {
    data["region"] = code_to_region[params.merino_country_reference];
    delete params["country"];
  }
  if (params.merino_country) {
    data["region"] = params.merino_country;
    delete params["country"];
  }
  data = data || {};
  if (!data.region) {
    data.region = getCookie("region") || "India";
  }
  let region = data.region;
  let is_international;
  if (!params["country"]) {
    is_international =
      ["INDIA", "NEPAL"].indexOf(data.region.toUpperCase()) >= 0 ? false : true;
  } else if (
    ["INDIA", "NEPAL"].indexOf(params["country"][0].toUpperCase()) >= 0
  ) {
    is_international = false;
    region = params["country"][0];
  } else {
    is_international = true;
  }
  setCookie("region", region);
  setCookie("is_international", is_international);
}

function set_redirects() {
  $("a[href^='https://www.merinolaminates.com']").each(function() {
    let nh = replace_website_url($(this).attr("href"));
    $(this).attr("href", nh);
  });
  $("a[href^='" + website_path + "']").each(function() {
    let nh = replace_website_url($(this).attr("href"));
    $(this).attr("href", nh);
  });
}

function preloadImages(array) {
  if (!preloadImages.list) {
    preloadImages.list = [];
  }
  var list = preloadImages.list;
  for (var i = 0; i < array.length; i++) {
    var img = new Image();
    img.onload = function() {
      var index = list.indexOf(this);
      if (index !== -1) {
        // remove image from the array once it's loaded
        // for memory consumption reasons
        list.splice(index, 1);
      }
    };
    list.push(img);
    img.src = array[i];
  }
}

function sort_by_list(list1, list2) {}

$(document).ready(function() {
  if ($("#dave-settings").length) {
    let ds = $("#dave-settings");
    for (let s in DAVE_SETTINGS) {
      if (ds.attr(s)) {
        DAVE_SETTINGS[s] = get_value(
          ds.attr("data-" + s.toLowerCase()) || ds.attr("data-" + s)
        );
      }
    }
  }
});

function get_assistant_flow_inspirations(params, callbackFunc, errorFunc) {
  const getResponsesLocalStorage = () => {
    return localStorage.getItem("assistantInspirations")
      ? JSON.parse(localStorage.getItem("assistantInspirations"))
      : [];
  };
  const saveResponsesLocalStorage = (responsesArr) => {
    localStorage.setItem("assistantInspirations", JSON.stringify(responsesArr));
  };

  const data = {
    totalQuestions: 5,
    data: [
      {
        type: "component",
        id: "color",
        data: {
          text: "Your favourite color?",
          component: "color-bar-component",
        },
      },
      {
        type: "options",
        id: "finish",
        data: {
          text: "Type of finish you prefer?",
          options: ["suede", "hi-gloss"],
          onClick: function(finish) {
            const responsesArr = getResponsesLocalStorage();
            responsesArr.finish = finish;
            saveResponsesLocalStorage(responsesArr);
          },
        },
      },
      {
        type: "options",
        id: "design",
        data: {
          text: "Type of design you want to explore?",
          options: [
            "woodgrains",
            "solids",
            "patterns",
            "solids",
            "infusio",
            "metallic",
          ],
          onClick: function(design) {
            const responsesArr = getResponsesLocalStorage();
            responsesArr.design = design;
            saveResponsesLocalStorage(responsesArr);
          },
        },
      },
      {
        type: "options",
        id: "surface",
        data: {
          text: "Surface you are looking for?",
          options: [
            "bed",
            "wardrobe",
            "shelf",
            "cabinet",
            "infusio",
            "metallic",
          ],
          onClick: function(surface) {
            const responsesArr = getResponsesLocalStorage();
            responsesArr.surface = surface;
            saveResponsesLocalStorage(responsesArr);
          },
        },
      },
      {
        type: "options",
        data: {
          text:
            "Awesome! We have some amazing designs for you. Lets dig in ...",
          options: ["Let's explore"],
        },
      },
    ],
  };

  callbackFunc(data);
}
